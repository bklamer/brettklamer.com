---
title: rcrypt
author: Brett Klamer
description: Symmetric file encryption in R using GPG.
---

# rcrypt

rcrypt provides easy symmetric file encryption for R based on [GPG](https://www.gnupg.org/). You must have GPG installed before using rcrypt. Most symmetric GPG encryption options are supported. See the R help documentation for more details. The code can be seen here <https://bitbucket.org/bklamer/rcrypt>.

## Install

GPG must be installed if you wish to use rcrypt. Windows users should install GPG from the program available at <https://www.gpg4win.org/>. Mac OS users should install GPG from the program available at <https://gpgtools.org/>. You can check if GPG is installed on your computer by running the following from a command line: `gpg --version`.

To install the development version of rcrypt from Bitbucket:

~~~ r
devtools::install_bitbucket("bklamer/rcrypt")
~~~

## Examples

~~~ r
# Encryption requires only the input file
encrypt("file.csv")
# You can designate the output location
encrypt("file.csv", output = "path/to/file.csv.gpg")
# Not suggested, but passphrases can also be supplied
encrypt("file.csv", passphrase = "your-passphrase")
~~~

Default encryption settings:
    
- AES256 encryption
- zlib compression
- Salt and hash the passphrase using
    - SHA512
    - 65,011,712 iterations

~~~ r
# Decryption requires only the input file
decrypt("file.csv.gpg")
# You can designate the output location
decrypt("file.csv.gpg", output = "path/to/file.csv")
# Not suggested, but passphrases can also be supplied
decrypt("file.csv.gpg", passphrase = "your-passphrase")
~~~

## Warnings

For increased security, GPG normally uses a pop-up box to enter the passphrase. If you wish to bypass this, enter the passphrase through the `encrypt` or `decrypt` function as mentioned above. If bypassed, the passphrase may be saved in the script as cleartext, saved in the terminal history in cleartext, and/or available in the list of processes in cleartext.

## License

Copyright: Brett Klamer - 2015 - MIT (http://opensource.org/licenses/MIT)
