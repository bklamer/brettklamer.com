---
title: bkmisc
author: Brett Klamer
description: Brett Klamer's miscellaneous R functions.
---

# bkmisc

A personal utility package.

## Install

See the code at <https://bitbucket.org/bklamer/bkmisc>.

``` r
devtools::install_bitbucket("bklamer/bkmisc")
```

## License

Copyright: Brett Klamer - 2018 - MIT (http://opensource.org/licenses/MIT)
