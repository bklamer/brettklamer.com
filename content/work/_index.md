---
title: Work
author: Brett Klamer
description: Work created by Brett Klamer.
outputs: html
---

# Work

- R
    - Packages
        - [bkdat](/work/bkdat/) - Base R functions that provide a grammar of data manipulation.
        - [bkstat](/work/bkstat/) - Useful statistical modeling functions.
        - [bkmisc](/work/bkmisc/) - Personal utility package.
        - [depower](https://bitbucket.org/bklamer/depower) - Power analysis for differential expression studies.
        - [rankdifferencetest](/work/rankdifferencetest/) - Kornbrot's Rank Difference Test. 
        - [bsm](/work/bsm/) - Bayesian Survival Modeling. 
        - [rcrypt](/work/rcrypt/) - Symmetric file encryption using GPG.
        - [statprograms](/work/statprograms/) - A data package with information on US statistics programs.
        - [emptypkg](https://bitbucket.org/bklamer/emptypkg) - A convenient starting point for creating R packages.
    - Other
        - [Package install script](https://bitbucket.org/bklamer/r-package-install)
        - [Project directory template](https://bitbucket.org/bklamer/r-project-template)
- Writing
    - [bsm](https://bitbucket.org/bklamer/bsm/raw/master/inst/doc/bsm.pdf) - A vignette for the bsm R package.
    - [Introduction to Statistics: Lecture Notes](https://bitbucket.org/bklamer/intro) - Notes I compiled as a TA.
    - [Diversions](/diversions/) - Information I've found useful and maybe you will too.
- LaTeX
    - [Résumé](/diversions/non-statistical/latex-resume-template/) - LaTeX templates for résumés.
    - [Reproducible Homework](/diversions/statistical/reproducible-homework/) - A knitr based LaTeX template for statistics homework.
    - [Slides](https://bitbucket.org/bklamer/slides-metropolis) - A knitr compatible beamer template based on metropolis (mtheme).
    - [Thesis](https://bitbucket.org/bklamer/thesis-uw) - A knitr based LaTeX template for theses.
- Web
    - [brettklamer.com](https://bitbucket.org/bklamer/brettklamer.com) - My personal site.
