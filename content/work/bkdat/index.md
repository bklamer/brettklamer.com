---
title: bkdat
author: Brett Klamer
description: Base R functions that provide a grammar of data manipulation similar to the functions found in the tidyverse.
---

bkdat
===

Base R functions that provide a grammar of data manipulation
similar to the functions found in the tidyverse. Think of it as the
namespace of dplyr, but the internal code of plyr.

Installation
------------

See the code at <https://bitbucket.org/bklamer/bkdat>.

``` r
devtools::install_bitbucket("bklamer/bkdat")
```

Examples
--------

### arrange() - ordering rows of a data frame

``` r
library(bkdat)
arrange(.data = mtcars, cyl)
#>     mpg cyl  disp  hp drat    wt  qsec vs am gear carb
#> 1  22.8   4 108.0  93 3.85 2.320 18.61  1  1    4    1
#> 2  24.4   4 146.7  62 3.69 3.190 20.00  1  0    4    2
#> 3  22.8   4 140.8  95 3.92 3.150 22.90  1  0    4    2
#> 4  32.4   4  78.7  66 4.08 2.200 19.47  1  1    4    1
#> 5  30.4   4  75.7  52 4.93 1.615 18.52  1  1    4    2
#> 6  33.9   4  71.1  65 4.22 1.835 19.90  1  1    4    1
#> 7  21.5   4 120.1  97 3.70 2.465 20.01  1  0    3    1
#> 8  27.3   4  79.0  66 4.08 1.935 18.90  1  1    4    1
#> 9  26.0   4 120.3  91 4.43 2.140 16.70  0  1    5    2
#> 10 30.4   4  95.1 113 3.77 1.513 16.90  1  1    5    2
#> 11 21.4   4 121.0 109 4.11 2.780 18.60  1  1    4    2
#> 12 21.0   6 160.0 110 3.90 2.620 16.46  0  1    4    4
#> 13 21.0   6 160.0 110 3.90 2.875 17.02  0  1    4    4
#> 14 21.4   6 258.0 110 3.08 3.215 19.44  1  0    3    1
#> 15 18.1   6 225.0 105 2.76 3.460 20.22  1  0    3    1
#> 16 19.2   6 167.6 123 3.92 3.440 18.30  1  0    4    4
#> 17 17.8   6 167.6 123 3.92 3.440 18.90  1  0    4    4
#> 18 19.7   6 145.0 175 3.62 2.770 15.50  0  1    5    6
#> 19 18.7   8 360.0 175 3.15 3.440 17.02  0  0    3    2
#> 20 14.3   8 360.0 245 3.21 3.570 15.84  0  0    3    4
#> 21 16.4   8 275.8 180 3.07 4.070 17.40  0  0    3    3
#> 22 17.3   8 275.8 180 3.07 3.730 17.60  0  0    3    3
#> 23 15.2   8 275.8 180 3.07 3.780 18.00  0  0    3    3
#> 24 10.4   8 472.0 205 2.93 5.250 17.98  0  0    3    4
#> 25 10.4   8 460.0 215 3.00 5.424 17.82  0  0    3    4
#> 26 14.7   8 440.0 230 3.23 5.345 17.42  0  0    3    4
#> 27 15.5   8 318.0 150 2.76 3.520 16.87  0  0    3    2
#> 28 15.2   8 304.0 150 3.15 3.435 17.30  0  0    3    2
#> 29 13.3   8 350.0 245 3.73 3.840 15.41  0  0    3    4
#> 30 19.2   8 400.0 175 3.08 3.845 17.05  0  0    3    2
#> 31 15.8   8 351.0 264 4.22 3.170 14.50  0  1    5    4
#> 32 15.0   8 301.0 335 3.54 3.570 14.60  0  1    5    8
```

### filter() - subsetting rows of a data frame

``` r
filter(.data = mtcars, cyl == 4)
#>                 mpg cyl  disp  hp drat    wt  qsec vs am gear carb
#> Datsun 710     22.8   4 108.0  93 3.85 2.320 18.61  1  1    4    1
#> Merc 240D      24.4   4 146.7  62 3.69 3.190 20.00  1  0    4    2
#> Merc 230       22.8   4 140.8  95 3.92 3.150 22.90  1  0    4    2
#> Fiat 128       32.4   4  78.7  66 4.08 2.200 19.47  1  1    4    1
#> Honda Civic    30.4   4  75.7  52 4.93 1.615 18.52  1  1    4    2
#> Toyota Corolla 33.9   4  71.1  65 4.22 1.835 19.90  1  1    4    1
#> Toyota Corona  21.5   4 120.1  97 3.70 2.465 20.01  1  0    3    1
#> Fiat X1-9      27.3   4  79.0  66 4.08 1.935 18.90  1  1    4    1
#> Porsche 914-2  26.0   4 120.3  91 4.43 2.140 16.70  0  1    5    2
#> Lotus Europa   30.4   4  95.1 113 3.77 1.513 16.90  1  1    5    2
#> Volvo 142E     21.4   4 121.0 109 4.11 2.780 18.60  1  1    4    2
```

### select() - subsetting columns of a data frame

``` r
select(data = mtcars, x = c("mpg", "cyl"))
#>                      mpg cyl
#> Mazda RX4           21.0   6
#> Mazda RX4 Wag       21.0   6
#> Datsun 710          22.8   4
#> Hornet 4 Drive      21.4   6
#> Hornet Sportabout   18.7   8
#> Valiant             18.1   6
#> Duster 360          14.3   8
#> Merc 240D           24.4   4
#> Merc 230            22.8   4
#> Merc 280            19.2   6
#> Merc 280C           17.8   6
#> Merc 450SE          16.4   8
#> Merc 450SL          17.3   8
#> Merc 450SLC         15.2   8
#> Cadillac Fleetwood  10.4   8
#> Lincoln Continental 10.4   8
#> Chrysler Imperial   14.7   8
#> Fiat 128            32.4   4
#> Honda Civic         30.4   4
#> Toyota Corolla      33.9   4
#> Toyota Corona       21.5   4
#> Dodge Challenger    15.5   8
#> AMC Javelin         15.2   8
#> Camaro Z28          13.3   8
#> Pontiac Firebird    19.2   8
#> Fiat X1-9           27.3   4
#> Porsche 914-2       26.0   4
#> Lotus Europa        30.4   4
#> Ford Pantera L      15.8   8
#> Ferrari Dino        19.7   6
#> Maserati Bora       15.0   8
#> Volvo 142E          21.4   4
```

### mutate() - adding new variables to a data frame

``` r
mutate(.data = mtcars, wt_pounds = wt*1000)
#>                      mpg cyl  disp  hp drat    wt  qsec vs am gear carb wt_pounds
#> Mazda RX4           21.0   6 160.0 110 3.90 2.620 16.46  0  1    4    4      2620
#> Mazda RX4 Wag       21.0   6 160.0 110 3.90 2.875 17.02  0  1    4    4      2875
#> Datsun 710          22.8   4 108.0  93 3.85 2.320 18.61  1  1    4    1      2320
#> Hornet 4 Drive      21.4   6 258.0 110 3.08 3.215 19.44  1  0    3    1      3215
#> Hornet Sportabout   18.7   8 360.0 175 3.15 3.440 17.02  0  0    3    2      3440
#> Valiant             18.1   6 225.0 105 2.76 3.460 20.22  1  0    3    1      3460
#> Duster 360          14.3   8 360.0 245 3.21 3.570 15.84  0  0    3    4      3570
#> Merc 240D           24.4   4 146.7  62 3.69 3.190 20.00  1  0    4    2      3190
#> Merc 230            22.8   4 140.8  95 3.92 3.150 22.90  1  0    4    2      3150
#> Merc 280            19.2   6 167.6 123 3.92 3.440 18.30  1  0    4    4      3440
#> Merc 280C           17.8   6 167.6 123 3.92 3.440 18.90  1  0    4    4      3440
#> Merc 450SE          16.4   8 275.8 180 3.07 4.070 17.40  0  0    3    3      4070
#> Merc 450SL          17.3   8 275.8 180 3.07 3.730 17.60  0  0    3    3      3730
#> Merc 450SLC         15.2   8 275.8 180 3.07 3.780 18.00  0  0    3    3      3780
#> Cadillac Fleetwood  10.4   8 472.0 205 2.93 5.250 17.98  0  0    3    4      5250
#> Lincoln Continental 10.4   8 460.0 215 3.00 5.424 17.82  0  0    3    4      5424
#> Chrysler Imperial   14.7   8 440.0 230 3.23 5.345 17.42  0  0    3    4      5345
#> Fiat 128            32.4   4  78.7  66 4.08 2.200 19.47  1  1    4    1      2200
#> Honda Civic         30.4   4  75.7  52 4.93 1.615 18.52  1  1    4    2      1615
#> Toyota Corolla      33.9   4  71.1  65 4.22 1.835 19.90  1  1    4    1      1835
#> Toyota Corona       21.5   4 120.1  97 3.70 2.465 20.01  1  0    3    1      2465
#> Dodge Challenger    15.5   8 318.0 150 2.76 3.520 16.87  0  0    3    2      3520
#> AMC Javelin         15.2   8 304.0 150 3.15 3.435 17.30  0  0    3    2      3435
#> Camaro Z28          13.3   8 350.0 245 3.73 3.840 15.41  0  0    3    4      3840
#> Pontiac Firebird    19.2   8 400.0 175 3.08 3.845 17.05  0  0    3    2      3845
#> Fiat X1-9           27.3   4  79.0  66 4.08 1.935 18.90  1  1    4    1      1935
#> Porsche 914-2       26.0   4 120.3  91 4.43 2.140 16.70  0  1    5    2      2140
#> Lotus Europa        30.4   4  95.1 113 3.77 1.513 16.90  1  1    5    2      1513
#> Ford Pantera L      15.8   8 351.0 264 4.22 3.170 14.50  0  1    5    4      3170
#> Ferrari Dino        19.7   6 145.0 175 3.62 2.770 15.50  0  1    5    6      2770
#> Maserati Bora       15.0   8 301.0 335 3.54 3.570 14.60  0  1    5    8      3570
#> Volvo 142E          21.4   4 121.0 109 4.11 2.780 18.60  1  1    4    2      2780
```

### summarise() - grouped summaries of a data frame

``` r
mtcars |>
  group_by(group_cols = "cyl") |>
  summarise(mean_hp = mean(hp))
#>   cyl   mean_hp
#> 1   4  82.63636
#> 2   6 122.28571
#> 3   8 209.21429
```

Functions
---------

The following functions are implemented:

| Tidyverse function                | bkdat function       |
|:----------------------------------|:---------------------|
| tibble::add\_column()             | add\_column()        |
| tibble::add\_row()                | add\_row()           |
| dplyr::arrange()                  | arrange()            |
| NA                                | as\_df()             |
| dplyr::bind\_cols()               | bind\_cols()         |
| dplyr::bind\_rows()               | bind\_fill\_rows()   |
| dplyr::bind\_rows()               | bind\_rows()         |
| dplyr::desc()                     | desc()               |
| dplyr::do()                       | do()                 |
| dplyr::slice() & dplyr::filter()  | filter()             |
| tidyr::gather()                   | gather()             |
| dplyr::group\_by()                | group\_by()          |
| rlang::is\_atomic()               | is\_atomic()         |
| NA                                | is\_date()           |
| rlang::is\_integer()              | is\_integer()        |
| rlang::is\_string()               | is\_string()         |
| dplyr::full\_join()               | join\_full()         |
| dplyr::inner\_join()              | join\_inner()        |
| dplyr::left\_join()               | join\_left()         |
| dplyr::right\_join()              | join\_right()        |
| purrr::map()                      | map()                |
| purrr::map\_chr()                 | map\_chr()           |
| purrr::map\_dbl()                 | map\_dbl()           |
| purrr::map\_df()                  | map\_df()            |
| purrr::map\_int()                 | map\_int()           |
| dplyr::mutate()                   | mutate()             |
| dplyr::nest\_by()                 | nest\_by()           |
| dplyr::pull()                     | pull()               |
| dplyr::rename()                   | rename()             |
| tibble::remove\_rownames()        | reset\_row\_names()  |
| tibble::rownames\_to\_column()    | ronames\_to\_column()|
| dplyr::select()                   | select()             |
| tidyr::separate()                 | separate()           |
| tidyr::spread()                   | spread()             |
| dplyr::summarise()                | summarise()          |
| tidyr::unite()                    | unite()              |

Changes were made to unify the naming scheme and remove base R naming
conflicts:

-   The dplyr join functions are renamed to `join_*()` instead of
    `*_join()`.
-   `bkdat::filter()` provides the same functionality as the combination
    of `dplyr::filter()` and `dplyr::slice()`.
