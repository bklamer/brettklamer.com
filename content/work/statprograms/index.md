---
title: statprograms
author: Brett Klamer
description: Datasets on US stat programs.
---

# statprograms

The `statprograms` package contains a small collection of data on graduate statistics programs from the United States. It's available on CRAN at <https://cran.r-project.org/package=statprograms>.

## statprograms

The `statprograms` dataframe contains various information from the majority of graduate statistics programs in the United States.

## degreesawarded

The `degreesawarded` dataframe contains the number of degrees awarded per year. Originally retrieved by Steve Pierson from the National Center for Education Statistics.

## Contributing

Find an error? Missing a statistics program? Please contact me at `code@brettklamer.com` or submit an issue/pull request at <https://github.com/bklamer/statprograms>.
