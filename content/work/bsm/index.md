---
title: bsm
author: Brett Klamer
description: An R package for Bayesian survival modeling.
---

# bsm

**Note: The package is currently out of date, and will be rewritten with a stan only backend in the future.**

The bsm package is designed to fit Bayesian Cox survival models with right-censored or interval-censored data, random effects, and time-dependent covariates. Users can specify prior distributions and use LPML, DIC, and WAIC for model selection. Backend MCMC sampling is done through either of the [Stan](http://mc-stan.org/) or [JAGS](http://mcmc-jags.sourceforge.net/) programming languages. The guiding principle of bsm is user-friendliness and providing a syntax similar to the eminent survival package. 

For more information, please see the [vignette](https://bitbucket.org/bklamer/bsm/raw/master/inst/doc/bsm.pdf). The code can be seen here <https://bitbucket.org/bklamer/bsm>.

## Install

To install the development version from Bitbucket:

~~~ r
devtools::install_bitbucket("bklamer/bsm")
~~~

## Examples

~~~ r
# Time-dependent covariates
heart_mod <- bsm(
  Surv(start, stop, event) ~ transplant * (age + surgery), 
  data = heart, 
  id = "id"
)
# Time-independent covariate
leuk_mod <- bsm(Surv(time, event) ~ treatment, data = leukemia)
~~~

## License

Copyright: Brett Klamer - 2015 - MIT (http://opensource.org/licenses/MIT)
