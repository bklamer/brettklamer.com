---
title: bkstat
author: Brett Klamer
description: Brett Klamer's statistical modeling R functions.
---

# bkstat

A personal R package with useful statistical modeling functions.

## Install

See the code at <https://bitbucket.org/bklamer/bkstat>.

``` r
devtools::install_bitbucket("bklamer/bkstat")
```

## License

Copyright: Brett Klamer - 2018 - MIT (http://opensource.org/licenses/MIT)
