---
title: The Complete Installation Guide for Xubuntu 18.04
author: Brett Klamer
description: How to install Xubuntu and other software.
diversions: Non-Statistical
tags:
date: 2018-07-30
lastmod: 2018-11-11
slug:
---

# The Complete Installation Guide for Xubuntu 18.04

This is a guide for installing Xubuntu 18.04 - the hard way. The partitioning scheme is laid out as

~~~
+-----------++-------------------------------------------------------+
|           || Logical vol1 20GB         | Logical vol2 20GB+        |
|           || /dev/mapper/system-root   | /dev/mapper/system-home   |
| Boot      ||_ _ _ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _ _ _ _ _ _|
| partition || dm-crypt LUKS LVM partition                           | 
| 500MB     || /dev/mapper/sdb2_crypt                                |
| /dev/sda1 ||_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
|           || dm-crypt LUKS encrypted partition                     |
|           || /dev/sda2                                             |
+-----------++-------------------------------------------------------+
~~~

## Warnings

1. You may need to change some options for your install
2. Test in a virtual machine before use. Real hardware will still be different.
3. If grub gets installed on the USB installation device, simply reboot with the installation USB, login, and run `sudo dpkg-reconfigure grub-pc`. This will show a text interface where you can choose the installation disk(s).

## Sections

- [Installation](#install)
- [Post install](#post-install)
- [Software](#software)

## <a name="install"></a>Install Xubuntu 18.04 with dm-crypt LUKS encryption for root and home partitions

1. Download minimal Ubuntu 18.04 at <https://help.ubuntu.com/community/Installation/MinimalCD>
1. Disable secure boot and uefi from the bios.
    - If that doesn't work, you could also try a uefi install by extracting the `EFI` directory from the `mini.iso` image and placing it in the top level directory of the usb. You would then need to create a 200MB EFI partition in the partitioning step.
1. Create a bootable USB
    1. Use the amazing bootiso utility from <https://github.com/jsamr/bootiso>.
3. Reboot computer from USB
4. Installer boot menu
    1. Advanced options
    2. Expert install
5. Installer main menu
    1. Choose language
        1. Language: English
        1. Country: United States
        1. Country for default settings: United States
        1. Additional Locales: none (continue)
    2. Configure the keyboard
        1. No, don't detect keyboard layout
        1. Country of origin: English (US)
        1. Keyboard layout: English (US)
    2. Detect network hardware
    2. Configure the network
        1. Primary network interface: wlan0 (wireless) or eth0 (wired)
        1. Wireless network: choose ssid
        1. Wireless network type: wpa/wpa2 psk
        1. wpa/wpa2 passphrase: password
        1. Yes, autoconfigure networking
        1. Continue with 3 seconds wait time
        1. Hostname: ubuntu
        1. Domain name: none (continue)
    2. Choose a mirror of the Ubuntu archive
        1. Protocol for file downloads: http
        1. Ubuntu archive for mirror country: United States
        1. Ubuntu archive mirror: us.archive.ubuntu.com
        1. HTTP proxy: none (continue)
    2. Download installer components
        1. Installer components to load: none (continue)
    2. Detect virtual driver disks
    2. Set up users and passwords
        1. Yes, enable shadow passwords
        1. No, do not allow login as root
        1. Name for new user: name
        1. Username for account: name
        1. Password for new user: pass
        1. No, do not encrypt home directory (this is eCryptfs-based)
    2. Configure the clock
        1. Yes, set the clock using NTP
        1. NTP server: ntp.ubuntu.com
        1. Yes, time zone is correct
    2. Detect disks
    2. Partition disks
        1. Manual
        1. Select the disk for partitioning (Only if safe to wipe current partition table! Otherwise select free space or already created partitions.)
            1. Yes, create new empty partition table
            1. Choose gpt partition table type
        1. Select free space – to create /boot partition
            1. Create a new partition
            1. Create partition size of 500 MB in size
                1. Beginning of partition
                1. Name: boot
                1. Use as: EXT4 file system
                1. Mount point: /boot
                1. Mount options: relatime
                1. Done setting up the partitioning
        1. Select free space – to create encrypted partition
            1. Create a new partition
            1. Partition size needs to be enough for both / (root) ~20GB and /home ~20GB-4TB (sizes dependent upon individual use cases)
                1. Beginning of partition
                1. Name: crypt
                1. Use as: Physical volume for encryption
                1. Done setting up the partitioning
        1. Configure the encrypted volumes
            1. Yes, write changes to disk
            1. Finish
            1. Enter passphrase for the encrypted partition
        1. Select the partition below 'Encrypted volume (sdX2_crypt)'
            1. Use as: physical volume for LVM
            1. Done setting up the partitioning
        1. Configure the Logical Volume Manager
            1. Yes, write changes to disk
            1. Create volume group
                1. Name: system
                1. Select encrypted volume partition: /dev/mapper/sdX2_crypt
            1. Create logical volume (for root)
                1. Select system
                1. Name: root
                1. Volume size around 20GB
            1. Create logical volume (for /home)
                1. Select system
                1. Name: home
                1. Volume size: the rest of the volume or whatever you desire
            1. Finish
        1. Create partitions for / (root) and /home
            1. Select LVM partition for / (root)
                1. Use as: btrfs file system (backup on a different filesystem. Use ext4 or xfs for stability)
                1. Mount point: / (root)
                1. Mount options: relatime
                1. Done setting up the partitioning
            1. Select LVM partition for /home
                1. Use as: btrfs file system (backup on a different filesystem. Use ext4 or xfs for stability)
                1. Mount point: /home
                1. Mount options: relatime
                1. Done setting up the partitioning
		1. Finish partitioning and write to disk
            1. No, ignore swap partition warning
            1. Yes, write changes to disk
            1. Enter maximum size for swap file (512MB?)
    2. Install Base System
        1. Kernel to install: Linux-signed-generic
        1. Drivers to include in the initrd: targeted
	2. Configure package manager
        1. Yes, use restricted software
        1. Yes, use universe software
        1. Yes, use multiverse software
        1. Yes, use backported software
        1. Yes, use software from the partner repository
        1. No, do not enable source repositories in APT
        1. Select all services to use
    2. Select and install software
        1. Install security updates manually
        1. Select Xubuntu desktop
    2. Install GRUB boot loader
        1. Yes, Install GRUB boot loader to the MBR
        1. Choose internal ssd
        1. No, Don't install grub to removable media path
    2. Finish the installation
        1. Yes, system clock is set to UTC
        1. Remove install media and continue for reboot

## <a name="post-install"></a>Post install suggestions

1. Update the Linux kernel.
    1. If you want a specific kernel
        1. Download the following from <http://kernel.ubuntu.com/~kernel-ppa/mainline/>
            1. `linux-headers-4.*-generic_*_amd64.deb`
            1. `linux-headers-4.*_all.deb`
            1. `linux-image-unsigned-4.*-generic_*_amd64.deb`
            1. `linux-modules-4.*-generic_*_amd64.deb`
        1. Open terminal in download location and run
            1. `sudo dpkg -i linux-headers*.deb`
            1. `sudo dpkg -i linux-modules*.deb`
            1. `sudo dpkg -i linux-image*.deb`
            1. `sudo update-grub`
        1. Restart computer
        1. Check kernel being used with
            1. `uname -a`
        1. Remove old kernels if `/boot` gets full
    1. If you want the LTS updated Kernel. Reference <https://wiki.ubuntu.com/Kernel/LTSEnablementStack>.
1. Enable the firewall.
    
    ~~~bash
    # https://help.ubuntu.com/community/UFW
    # https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw/
    sudo ufw enable
    sudo ufw default allow outgoing
    sudo ufw default deny incoming
    ~~~

1. Disable hyperthreading (if option not available in the bios).
    1. <https://askubuntu.com/questions/942728/disable-hyper-threading-in-ubuntu>
    1. <https://aws.amazon.com/blogs/compute/disabling-intel-hyper-threading-technology-on-amazon-linux/>
    1. <https://unix.stackexchange.com/questions/416137/how-can-i-run-a-script-on-startup-on-ubuntu-server-17-10>
2. Update btrfs.
    1. Check version with
        1. `btrfs --version`
    1. Download from <https://launchpad.net/ubuntu/+source/btrfs-tools>
    1. Install using software center
    1. Check disk io/r/w transactions
        1. `sudo iotop -oPa`
3. If there is a separate partition or disk that needs to be mounted and unencrypted at boot. Reference <http://ubuntuforums.org/showthread.php?t=837416>.
    1. Check UUID of partitioning
        1. `sudo blkid`
    1. Check block size of / (root) partition for nice key size (likely 4096)
        1. `sudo blockdev --getbsz /dev/mapper/system-root`
    1. Create random keyfile in /root
        1. `sudo dd if=/dev/urandom of=/root/keyfile bs=4096 count=1`
            - the bs= value should be the block size we just found
    1. Make keyfile read only to root
        1. `sudo chmod 0400 /root/keyfile`
    1. Add keyfile to LUKS partition of /dev/sdX#_crypt
        1. `sudo cryptsetup luksAddKey /dev/sdX#_crypt /root/keyfile`
            - Enter existing password for /dev/sdX#_crypt
    1. Create mapper
        1. `sudo mousepad /etc/crypttab`
        1. add '/root/keyfile' to replace 'none' for /dev/sdX#_crypt
            - example: `sdX#_crypt UUID=XXX /root/keyfile luks`
    1. Mount the drive (if needed)
        1. `sudo mousepad /etc/fstab`
            - example: `/dev/mapper/sdX#_crypt /<mount point> btrfs relatime 0 2`
    1. Update settings in initramfs images
        1. `sudo update-initramfs -u -k all`
4. TRIM for SSDs.
    1. Reference <http://blog.neutrino.es/2013/howto-properly-activate-trim-for-your-ssd-on-linux-fstrim-lvm-and-dmcrypt/>.
    1. Enable Trim on dm-crypt
        1. Open `/etc/crypttab`
            1. `sudo mousepad /etc/crypttab`
            1. If needed, add 'discard' to the options for `sdX#_crypt`.
    2. Make sure LVM has 'issue_discards=1' in
        1. `sudo mousepad /etc/lvm/lvm.conf`
    3. Check encrypted drive with
        1. `sudo dmsetup table /dev/mapper/sdX#_crypt`
        2. make sure it has '1 allow_discards'
    4. Remove or check "discard" is not used in the `fstab`
        1. `sudo mousepad /etc/fstab`
    5. Run TRIM manually or check for errors
        1. `sudo fstrim -v /home`
    6. If any changes were made, run
        1. `sudo update-initramfs -c -k all`
4. If installing in Virtualbox, install additions by
    1. `sudo apt install virtualbox-guest-utils virtualbox-guest-dkms dkms`
    1. To share a folder, make a permanent machine folder then run
        1. `sudo usermod -a -G vboxsf username`
    1. To share a USB port
        1. `sudo usermod -a -G vboxusers username`
5. Set window tile keybinds similar to Microsoft Windows.
    1. Open terminal and run
        1. `xfce4-settings-manager`
    1. Go to window manager
        1. Open the Keyboard tab
            1. Set the "Tile window to the left" (and right)
6. Backbutton in firefox to backsapce.
    1. Type `about:config` in the address bar
	1. Look for `browser.backspace_action` in the list
	1. Change the Value to 0.
7. If needed, install Intel wireless driver.
    1. Download driver from <http://intellinuxwireless.org/?n=Downloads>
    1. Navigate to download folder
        1. `tar xvzf iwlwifi-XXX.tgz`
        1. `cd iwlwifi-XXX/`
        1. `sudo cp iwlwifi-XXX.ucode /lib/firmware`
8. Check partition sizes.
    1. `df -h`
    1. `btrfs filesystem df`
    1. `btrfs filesystem show`
9. Install packages from a newer release.
    1. <http://askubuntu.com/questions/103320/install-packages-from-newer-release-without-building-apt-pinning>
    1. `apt install <package> -t yakkety`
11. Modify or redirect home folder names.
    1. change in `/home/username/.config/user-dir.dirs`
12. Change ownership of extra storage drives or partitions.
    1. `sudo chown -R username /partition`
13. Change desktop lock keybind.
    1. Go to settings editor
    2. xfce4-keyboard-shortcuts
    3. new commands custom property
        1. property: `/commands/custom/<super>l`
        2. type: `string`
        3. value: `xflock4`
14. Format a USB drive.
    1. `df`
    1. `umount /dev/sdc1`
    1. `mkfs.vfat /dev/sdc1`
15. Create a dm-crypt LUKS encrypted external drive. Reference <https://gitlab.com/cryptsetup/cryptsetup/wikis/FrequentlyAskedQuestions>.
    
	1. Find the external drive (assume the filesystem is /dev/sdb1 and it's mount location /media/USERNAME/*)
	   
        ~~~bash
        df
        ~~~

	2. Unmount it

        ~~~bash
        umount /media/USERNAME/*
        ~~~

	3. Quickly wipe old filesystems. wipefs clears the first superblock.

        ~~~bash
        sudo wipefs -a /dev/sdb1
        ~~~

	4. Create the LUKS container (follow on-screen intructions)

        ~~~bash
        sudo cryptsetup luksFormat /dev/sdb1
        ~~~

	5. Check the passphrase iteration count. The key slot default is 1 second of PBKDF2 hashing. The volume key default (MK iterations) is 0.125 seconds. You can set the key slot with `cryptsetup luksFormat -i 15000 <target device>`

        ~~~bash
        sudo cryptsetup luksDump /dev/sdb1
        ~~~

	6. Map the container to /dev/mapper/backup1

        ~~~bash
        sudo cryptsetup luksOpen /dev/sdb1 backup1
        ~~~

	7. Create a filesystem in the mapped container

        ~~~bash
        sudo mkfs.btrfs --label backup1 /dev/mapper/backup1
        ~~~

	8. Mount the filesystem (right after creation; using lzo compression)

        ~~~bash
        mount -o compress=lzo /dev/mapper/backup1 /mnt
        ~~~

	9. Mount the filesystem (day to day use as a portable external drive; using lzo compression). You can either create an fstab entry or mount using the command line.

        - Using an fstab entry

            ~~~bash
            # Get the UUID of the mounted and unlocked /dev/mapper/ filesystem
            sudo blkid
            ~~~
        Add the following entry to `/etc/fstab`

            ~~~bash
            UUID=YOUR-UUID /media/backup1 btrfs noauto,defaults,noatime,compress=lzo 0 0
            ~~~
        Now it will automatically mount at `/media/backup1`. The `noauto` option is used in the fstab entry to prevent automatically mounting the drive at boot time. If you leave this option off, then your computer will fail to boot and you will need to edit the fstab in recovery mode. The `nofail` option can be used for drives that are usually going to be mounted at boot time.

            Change ownership of the new mount point so you can perform cut/copy/paste, etc.

            ~~~bash
            sudo chown -R USERNAME /media/backup1
            ~~~

       - Using the terminal

            ~~~bash
            # The OS will automatically mount the drive and ask for passphrase to unlock. Then...
            df
            sudo umount /media/USERNAME/*
            sudo mount -o compress=lzo /dev/dm-4 /media/backup1
            sudo chown -R USERNAME /media/backup1
            ~~~

16. Fix backlight adjustment issues.
	1. `sudo apt install xbacklight`
	2. `xbacklight -set XX`
17. Turn on USB keyboard password input for dmcrypt login.
	1. Check current modules being used with `lsmod`
	2. In a terminal, open
		- `sudo /etc/initramfs-tools/mousepad modules`
           Then add the following

            ~~~
            usbhid
            hid
            hid_logitech_dj
            hid_logitech_hidpp
            ~~~
        - `sudo update-initramfs -u`
20. Add icons to desktop panel.
    1. Go to `/usr/share/applications/` and drag the application to the panel.
21. If unable to boot because fstab is bad (This is also helpful for situations that do not involve fstab).
    1. Reboot computer
    2. At grub menu, choose advanced options and boot into recovery mode.
    3. Enter the Root Session.
    4. Because you're in read only mode, remount with write privileges.

        ~~~bash
        mount -o remount,rw /
        ~~~

    5. Edit the fstab

        ~~~bash
        nano /etc/fstab
        ~~~

22. Re-label a btrfs filesystem.
	
    ~~~bash
    # First unmount, then rename
    sudo umount /dev/dm-4
    sudo btrfs filesystem label /dev/dm-4 NEWNAME
    ~~~

23. Stop system error pop ups.

    Sometimes a system error will be reported and cause a warning pop up over multiple restarts. You can remove this by either

    1. `sudo rm /var/crash/*`
    2. `gksu nano /etc/default/apport` and set `enabled=0`

24. Randomize MAC address.

    This is based on <https://fedoramagazine.org/randomize-mac-address-nm/>. To randomize wifi connections, create the file `/etc/NetworkManager/conf.d/00-macrandomize.conf` and add the following:

    ~~~bash
    # can use 'random' or 'stable' below
    [device]
    wifi.scan-rand-mac-address=yes

    [connection]
    wifi.cloned-mac-address=stable
    ethernet.cloned-mac-address=stable
    connection.stable-id=${CONNECTION}/${BOOT}
    ~~~

    Then restart networkmanager with `systemctl restart NetworkManager`.

25. Change owner of entire directory.

    ~~~bash
    sudo chown -R <username> *
    ~~~

26. Let apt fix dependency issues automatically.

    ~~~bash
    sudo apt --fix-broken install
    ~~~

27. GPG bug fix when adding keys behind a proxy: use the option `http-proxy=`

    ~~~bash
    sudo apt-key adv --keyserver keyserver.ubuntu.com --keyserver-options http-proxy=http://PROXYADDRESS --recv-keys GPGKEY
    ~~~

## <a name="software"></a>Software install suggestions

### Apt packages

<pre class="scroll"><code>#======================================================================
# Update
#======================================================================
sudo apt update
sudo apt dist-upgrade
#======================================================================
# Remove unwanted software from xubuntu-desktop recommends
#======================================================================
sudo apt purge '^brltty.*' '^espeak.*' '^hplip.*' '^libhpmud0.*' '^libsane-hpaio.*' '^parole.*' '^printer-driver.*' '^speech-dispatcher.*' '^whoopsie.*' '^libwhoopsie0.*' '^popularity-contest.*' '^pidgin.*'
sudo apt autoremove
sudo apt autoclean
#======================================================================
# Install software
#======================================================================
# general
sudo apt install iotop software-properties-common p7zip-full curl libdbd-sqlite3 audacious usb-creator-gtk chromium-browser texinfo libimobiledevice-dev
#sudo apt --no-install-recommends install gnucash
# r
sudo apt install jags pandoc pandoc-citeproc gcc gfortran libudunits2-dev libssl-dev libgit2-dev libssh2-1-dev
# git
sudo apt install make libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip
# power
sudo apt install tlp tlp-rdw powertop
sudo tlp start
sudo powertop --auto-tune
#======================================================================
# Install other repository software
#======================================================================
#------
# Repos
#------
# R
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran35/"
sudo add-apt-repository ppa:marutter/c2d4u3.5
# KeepassXC
sudo add-apt-repository ppa:phoerious/keepassxc
# Lyx
sudo add-apt-repository ppa:lyx-devel/release
# Libreoffice
sudo add-apt-repository ppa:libreoffice/ppa
# mpv
sudo add-apt-repository ppa:mc3man/mpv-tests
# git
sudo add-apt-repository ppa:git-core/ppa
# Virtualbox
sudo add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" && wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
# Gimp
sudo apt-add-repository ppa:otto-kesselgulasch/gimp
# Inkscape
sudo add-apt-repository ppa:inkscape.dev/stable
# sublime text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
#--------
# Install
#--------
sudo apt update
# R
sudo apt install r-base r-base-dev libopenblas-base liblapack3 libcairo2-dev libxt-dev
# keepassXC
sudo apt install keepassxc
# Lyx
sudo apt --no-install-recommends install lyx
# libreoffice
sudo apt --no-install-recommends install libreoffice-writer
sudo apt install libreoffice-calc
# mpv
sudo apt install mpv
# git
sudo apt install git
# Virtualbox
sudo apt install virtualbox
# Gimp
sudo apt install gimp
# Inkscape
sudo apt install inkscape
# sublime text
sudo apt-get install sublime-text
#======================================================================
# Cleanup
#======================================================================
sudo apt update
sudo apt dist-upgrade
sudo apt autoclean
sudo apt autoremove
</code></pre>

### Manual packages

2. Install texlive 2018. Reference <http://tex.stackexchange.com/a/95373>.
    1. Download Texlive and run

        ~~~ bash
        wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
        tar -zxvf install-tl-unx.tar.gz
        sudo apt install perl-tk
        cd install-tl*
        sudo ./install-tl --gui
        ~~~
        
        - Choose the small scheme (just download fonts and packages as you need them)
        - Choose Recommended fonts, Mathematics packages, and LuaTeX packages
        - Make sure to "create symlinks in system directories"

    2. Edit `/etc/environment` by adding 

        ~~~ bash
        :/usr/local/texlive/2018/bin/x86_64-linux:/usr/local/texlive/2018/texmf-dist/doc/man:/usr/local/texlive/2018/texmf-dist/doc/info:
        ~~~

    3. Make apt see the local install by:

        ~~~ bash
        sudo apt install equivs --no-install-recommends
        sudo apt install freeglut3
        mkdir /tmp/tl-equivs
        cd /tmp/tl-equivs
        equivs-control texlive-local
        # copy this http://www.tug.org/texlive/files/debian-equivs-2018-ex.txt to
        mousepad texlive-local
        equivs-build texlive-local
        sudo dpkg -i texlive-local_2018-1_all.deb
        ~~~

    4. Access tlmgr using either of

   	   - `sudo env PATH="$PATH" tlmgr --gui`
	   - `sudo /usr/local/texlive/2018/bin/x86_64-linux/tlmgr --gui`

    5. Update texlive. Remove the old texlive with the commands below, then Re-run the install commands.

        ~~~ bash
        # Remove old apt local install
        sudo apt remove texlive-local

        # Remove the old texlive directories
        sudo rm -rf /usr/local/texlive/2018/
        sudo rm -rf /usr/local/texlive/texmf-local/
        sudo rm -rf /home/USERNAME/.texlive2018/
        sudo rm -rf /var/lib/texmf/

        # Remove the old texlive symlinks (Make sure there's nothing else in there)
        sudo rm /usr/local/bin/*
        sudo -rf rm /usr/local/share/man/*
        sudo rm /usr/local/share/info/*

        # Update the font cash
        sudo fc-cache -fsv
        ~~~
2. Install Gnucash

    The current version of gnucash is 3.3, but only 2.6.19 is available in the 18.04 archives. The following will allow you to install gnucash 3.3 on ubuntu 18.04
    1. See the reference documentation at <https://wiki.gnucash.org/wiki/Ubuntu>.
    3. Since the current package in the ubuntu package repo is too old, from <https://packages.debian.org/sid/amd64/libboost-regex1.62.0/download>, download and install:

        ~~~bash
        libboost-regex1.62.0_1.62.0+dfsg-10_amd64.deb
        ~~~
    4. Install
        ~~~bash
        sudo apt install guile-2.2-libs libaqbanking35 libboost-date-time1.62.0 libboost-filesystem1.62.0 libboost-locale1.62.0 libboost-system1.62.0 libgwenhywfar60 libktoblzcheck1v5 libofx7 libaqbanking35-plugins guile-2.2 libfinance-quote-perl libhtml-tableextract-perl libdate-manip-perl libjs-jquery libcrypt-ssleay-perl
        ~~~
    2. From <http://ftp.us.debian.org/debian/pool/main/g/gnucash/>, download and install:

        ~~~bash
        gnucash-common_3.3-2_all.deb
        gnucash_3.3-2_amd64.deb
        ~~~

3. Install redshift
    1. Install from repo

        ~~~bash
        sudo apt-get install redshift redshift-gtk
        ~~~
    1. Append the following to geoclue's config with `sudo nano /etc/geoclue/geoclue.conf`

        ~~~bash
        [redshift]
        allowed=true
        system=false
        users=
        ~~~
    1. Create a redshift config file with `nano ~/.config/redshift.conf`
    
        ~~~bash
        [redshift]
        temp-day=5500
        temp-night=2700
        location-provider=manual

        [manual]
        lat=4X
        lon=-8X
        ~~~
4. Modify sublime text's settings
    1. at Preferences -> Distration Free, add the line `"update_check": false,`
    1. Add `127.0.0.1 license.sublimehq.com` to `/etc/hosts`.
4. Install Grub Customizer (used to modify the grub menu for dual boot).
    1. `sudo add-apt-repository ppa:danielrichter2007/grub-customizer`
    1. `sudo apt update`
    1. `sudo apt install grub-customizer`
5. Install Texmaker.
    - Download from <http://www.xm1math.net/texmaker/download.html>

        ~~~bash
        sudo dpkg -i *.deb
        sudo apt install -f
        ~~~

6. Install Rstudio.
    - Download from <https://www.rstudio.com/products/rstudio/download/preview/>

        ~~~bash
        sudo dpkg -i *.deb
        sudo apt install -f
        ~~~

7. Install Teamviewer.
    - Download from <https://www.teamviewer.com/en/download/linux/>

        ~~~bash
        sudo dpkg -i *.deb
        sudo apt install -f
        ~~~

7. Install Anydesk.
    - Download from <https://anydesk.com/download?os=linux>

        ~~~bash
        sudo dpkg -i *.deb
        sudo apt install -f
        ~~~

    - Anydesk uses a dark pattern of forcing autostart of a background service/system tray icon. Just deleting the /etc/xdg/autostart/anydesk* file no longer prevents the auto startup. To stop it, you needed to comment out all lines/delete the following file:

        ~~~bash
        /etc/systemd/system/anydesk.service
        ~~~

10. Install Skype.
    - Download from <https://www.skype.com/en/get-skype/>

        ~~~bash
        sudo dpkg -i *.deb
        sudo apt install -f
        ~~~

11. Install Bleachbit.
    - Download from <https://www.bleachbit.org/download/linux>

        ~~~bash
        sudo dpkg -i *.deb
        sudo apt install -f
        ~~~

14. Install Brother printer drivers.
    1. Download from <http://support.brother.com/g/b/productsearch.aspx?c=us&lang=en&content=dl>
        1. `gunzip linux-brprinter-installer-2.*.gz`
        1. `sudo bash linux-brprinter-installer-2.*`
        1. Enter machine name
        1. When you see the message "Will you specify the DeviceURI ?" USB Users: Choose N(No). Network Users: Choose Y(Yes).
    1. If scanner isn't working
        1. `brsaneconfig4 -a name=Scanner model='modelname' ip='ip-address'`
    1. Check network mapping with
        1. `nmap 'IP range'/24`
