---
title: Static Website Hosting on Amazon S3
author: Brett Klamer
description: How to host a static website on Amazon S3.
diversions: Non-Statistical
tags:
date: 2015-12-12
slug:
---

# Static Website Hosting on Amazon S3

Setting up a static site on Amazon S3 is relatively simple and very cheap. Here are the steps on how to set up the site.

1. Sign up at <http://aws.amazon.com/> and choose the basic (free) plan. Click 'Launch Management Console'.
2. Click S3 and create a new bucket. Name the bucket your domain name, e.g. "example.com".
3. Select the 'Properties' for your newly created bucket and then click on 'Static Website Hosting'. Select 'Enable website hosting', enter `index.html` for the index document, and enter `404.html` for the error document. Make sure to save the changes.
4. Double click on the newly created bucket. Upload the relevant files for the root domain and create folders for your other files to be uploaded to.
5. Select all files and folders, then click 'Actions' and click 'Make Public'. Another way to make files public is by adding a bucket policy (See Properties -> Permissions):

    ~~~ AWS
    {
     "Version": "2012-10-17",
     "Id": "My Policies",
     "Statement": [
       {
         "Sid": "Allow Global Access",
         "Effect": "Allow",
         "Principal": {
           "AWS": "*"
         },
         "Action": "s3:GetObject",
         "Resource": "arn:aws:s3:::example.com/*"
       }
     ]
    }
    ~~~
6. The files will now be accessible at an address similar to `http://s3-us-west-2.amazonaws.com/example.com/index.html` and `http://example.com.s3-website-us-west-2.amazonaws.com/`. You need to edit your domain's DNS settings (probably at your registar) by creating a CNAME record with the following information:

    ~~~ DNS
    Name: example.com
    Type: CNAME
    Record: example.com.s3-website-us-west-2.amazonaws.com
    ~~~

    Make sure `www.example.com` will behave as expected.
7. Test and make sure everything is working!

Also check out the official AWS guide here (2023): https://docs.aws.amazon.com/AmazonS3/latest/userguide/website-hosting-custom-domain-walkthrough.html

## Pricing

The estimated cost for

- 1GB Standard Storage
- 10,000 PUT/COPY/POST/LIST Requests
- 10,000 GET and Other Requests
- 1GB Data Transfer Out
- 1GB Data Transfer In

is only $0.09 per month ($1.08 per year). You can check other costs at <http://calculator.s3.amazonaws.com/index.html>.
