---
title: Raspberry Pi
author: Brett Klamer
description: Raspberry Pi setup instructions.
diversions: Non-Statistical
tags:
date: 2017-11-30
slug:
---

# Raspberry Pi

Setup instructions for a raspberry pi.

## Create bootable micro sd card

1. Find sd card with `sudo fdisk -l`.
2. Transfer raspbian with `sudo dd bs=4M if=2017-11-29-raspbian-stretch.img of=/dev/mmcblk0 status=progress`. If that fails, then you can use `gnome-disk-utility` to restore the image to the sd card.

## First boot

1. Change default password `raspberry` to your own password.
2. Update the OS and firmware.

    ```
    sudo apt update
    sudo apt full-upgrade
    sudo apt-get autoremove
    sudo apt-get autoclean
    sudo apt-get install rpi-update
    sudo rpi-update
    ```
	  
3. Disable autologin in system preferences.
4. Disable anyone from using sudo without a password prompt by commenting out line in `/etc/sudoers.d/010_pi-nopasswd`.

## Format micro sd card to F2FS

```
# run on pi and main computer
sudo apt-get install f2fs-tools
# insert micro sd into other computer
# check for locations
sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL
# remount for consistent naming
sudo umount /dev/mmcblk0p1
sudo umount /dev/mmcblk0p2
sudo mkdir /media/boot
sudo mkdir /media/root
sudo mount -t vfat /dev/mmcblk0p1 /media/boot
sudo mount -t ext4 /dev/mmcblk0p2 /media/root
# make backup
sudo cp -a /media/root /home/user/backup-root
# create f2fs file system
sudo umount /dev/mmcblk0p2
sudo mkfs.f2fs -f /dev/mmcblk0p2
# restore root
sudo mount -t f2fs /dev/mmcblk0p2 /media/root
sudo cp -a /home/user/backup-root/* /media/root
# Modify boot files
## /dev/mmcblk0p2  /      f2fs    defaults,noatime,discard  0       1
sudo nano /media/root/etc/fstab
## rootfstype=f2fs
sudo nano /media/boot/cmdline.txt
```

## Encrypt

Use strategy from <https://wiki.debian.org/TransparentEncryptionForHomeFolder>
