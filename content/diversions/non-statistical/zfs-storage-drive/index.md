---
title: ZFS Storage Drive
author: Brett Klamer
description: Create a ZFS storage drive.
diversions: Non-Statistical
tags:
date: 2020-11-29
lastmod: 2021-01-02
slug:
---

# ZFS Storage Drive

Commands to work with a ZFS drive on an Ubuntu OS.

## Create a new ZFS storage drive

```{bash}
# Install ZFS tools
sudo apt install zfsutils-linux

# Create a ZFS pool named 'tank'
## get disk id at /dev/disk/by-id/*
sudo zpool create -f -o ashift=12 -O mountpoint=none tank /dev/disk/by-id/XXXX

# Create an encrypted dataset
sudo zfs create -o encryption=aes-256-gcm -o keyformat=passphrase -o keylocation=prompt -o mountpoint=/tank/encrypted tank/encrypted

# Create an unencrypted dataset
#zfs create -o encryption=off -o mountpoint=/tank/not-encrypted tank/not-encrypted

# Specify compression
sudo zfs set compression=lz4 tank/encrypted

# Specify owners
sudo chown -R NAME /tank/encrypted
```

## Working with ZFS drives

```{bash}
# Mount pool
sudo zpool import tank
sudo zfs mount -l tank/encrypted

# Unmount pool
sudo zpool export tank

# Check pools
zfs get all
zfs get -p encryption,keystatus,pbkdf2iters
sudo zpool list -v
zfs list -o name,used,avail,refer,encryptionroot,mounted,mountpoint -S encryptionroot

# Scrub (error check) pools
## start
sudo zpool scrub tank
## check status
sudo zpool status -v tank
## stop running scrub
sudo zpool scrub -s tank

# Check compression
## Show the compressed space of all files in the pool
zfs get used tank
## Show the uncompressed space of all files in the pool
zfs get logicalused tank
## Show the average compression ratio of the pool 
zfs get compressratio tank

# Spin down disk
## Immediately
#sudo zpool export tank
#lsblk
#sudo umount /dev/sdb1
#sudo hdparm -y /dev/sdb
## After 25*5 seconds
#sudo zpool export tank
#lsblk
#sudo hdparm -S 25 /dev/sdb

# Upgrade file system version on disk
# For ubuntu OS, https://launchpad.net/~jonathonf/+archive/ubuntu/zfs
# is a commonly recommended source for upgrading the OS ZFS.
## Display list of file systems that are not updated
zfs upgrade
zpool upgrade
## Display currently supported file system version
zfs upgrade -v
zpool upgrade -v
## Upgrade file system on all imported pools
#zfs -a upgrade
#zpool -a upgrade

# Destroy pool
#zpool destroy tank

# Check size of folders at current location
du -chd 1 | sort -h
```

## SMART attributes

```{bash}
# Install smartmontools
sudo apt install smartmontools

# Check attributes
sudo smartctl -a /dev/sdX
sudo smartctl -i /dev/sdX
sudo smartctl -c /dev/sdX
```
