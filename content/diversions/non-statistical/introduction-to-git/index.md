---
title: Introduction to Git
author: Brett Klamer
description: A small list of tips for using git.
diversions: Non-Statistical
tags:
date: 2014-08-01
lastmod: 2018-04-01
slug:
---

# Introduction to Git

A few things that are useful as I'm learning git.

## References

- <http://git-scm.com/book>

## Definitions

- **Working Directory** - Directory which contains the files of interest. Where you add, remove, and modify the files of interest.
- **Index** - aka cache. Directory that git uses as a staging area to contain the files and folders ready to be committed to the repository. Git only sees the working directory files after being added to the index. - [ref](https://www.youtube.com/watch?v=8dhZ9BXQgc4#t=23m30s)
- **Commit** - Saves the current index to the repository.
- **Repository** - Just an overall reference. The `.git` folder that contains the most recent state of the files you're working on. You can compress and copy the `.git` directory to transfer the entire repository.
- **Origin** - Commonly used branch name for remote repositories.
- **Master** - Default name for the local repository branch. The "tree trunk" of the branch.
- **Branch** - A reference to a particular version of your project. Normally everything is done with commits to the master branch. "A branch is basically a commit that's at the head of an object chain." - [ref](https://www.youtube.com/watch?v=8dhZ9BXQgc4#t=17m55s). You can delete a branch after merging with the master and not lose any information.
- **HEAD** - A reference to the current branch that will be committed to.
- **Diff between HEAD and index** - "Changed things not yet committed." - [ref](https://www.youtube.com/watch?v=8dhZ9BXQgc4#t=25m15s)
- **Diff between index and working directory** - "Changed things not yet added." - [ref](https://www.youtube.com/watch?v=8dhZ9BXQgc4#t=25m25s)

## Commands

- `git init` - Starts or initializes an empty git repository. Adds the `.git` directory to your working directory.
- `git add` - Updates the index using the current content found in the working directory. - [ref](http://stackoverflow.com/a/2948576)
    - `git add .` - Add to index only files created/modified and not those deleted
    - `git add -u` - Add to index only files deleted/modified and not those created
    - `git add -A` - Do both operation at once, add to index all files
- `git commit` -  Saves the current content of the index, and information about the changed, added, or removed files, to the repository.
- `git commit -a` - Combines the `git add` and `git commit` command into one line. Be careful as it will add all tracked files to the index and then commit them.
- `git status` - Lets you know of any changes or differences.
- `git remote add <alias> <url>` - Adds a remote repository named "alias" to the service at "url".
- `git remote -v` - Verifies the remote repository.
- `git push <alias> <branch>` - Updates the remote repository "alias" with the index from the local repository "branch".
- `git clone <url>` - Creates a local git repository from an existing remote repository.
- `git merge <diffBranch>` - Merges the files changed in the "diffBranch" to the branch you are currently in.
- `git log` - Shows all commit messages for the current branch.
- `git fetch <remoteBranch>` - Updates the remote branch information on the local repository.
- `git remote remove origin` - Remove a remote repository (github, bitbucket, etc.)
- `git reset HEAD <file>` - Undo a `git add <file>`.
- `git config --list` -  show git configuration details of repository.

## Common Tasks

### Write a commit message

1. Be consistent.
1. Separate subject from body with a blank line.
2. Limit subject to 50 characters and body to 72 characters.
3. Capitalize the subject line and don't end with a period.
4. Use imperative mood in the subject line
   - Fix ... (A bug fix)
   - Document ... (Documentation only changes)
   - Add ... (Add a feature or new code)
   - Remove ... (Remove a feature or old code)
   - Change ... (Change how a feature works)
   - Refactor ... (A code change that neither fixes bugs or adds features)
   - Style ... (Changes that do not affect the meaning of the code: white-space, formatting, etc)
   - Tag version ... (When pushing new version)
   - Update ... (Updating a text post)
5. The body should explain the motivation for the change and contrast this with previous behavior.

Reference [Chris Beams](http://chris.beams.io/posts/git-commit/) and [Angular](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#commit-message-format)


### Create a new remote repository from an existing working directory

1. Set up the `.gitignore` file to ignore your desired files or folders.
2. Open a terminal in your working directory.

    ~~~ bash
    git init
    git add -A
    git commit -m 'First commit'
    git remote add origin <url>
    git remote -v
    git push origin master
    ~~~

### Typical work flow

1. Edit files in working directory.
2. `git status`
3. `git add <files>`
    - Use `git add -p <file>` to enter an interactive environment to selectively choose different 'hunks' to include or exclude.
        - `y` adds the current chunk, `n` skips the current chunk, and `s` splits the current chunk into smaller pieces so you can select the smaller component.
4. `git commit -m "message"`
    - or leave off -m to pop up a text editor for the message. the first line is the 'subject' and the lines below are for further comments.
5. `git push origin master`

### Reset (delete) the git repository

1. Delete the `.git` folder of the local project.
2. Create a new git repository.
3. `git push --force -u origin master`. This will force the remote repository to match the local repository.

### Check for differences

- Check for changes between working directory and index of the local repository.
    - `git diff`
    - `git status`
- Check for changes between the index and the HEAD (most recent commit of the local repository).
    - `git diff –cached`
    - `git status`
- Files that have been added, ready to commit, but not yet committed.
    - `git diff HEAD  --name-only`
- Check for changes between the most recent commit of the local repository and most recent commit of the remote repository.

    ~~~ bash
    git fetch origin/master
    git branch -a
    git diff master origin/master
    ~~~

- See the difference between the most recent commit and the previous commit.
    - `git diff master^ master`

### Remove files or folders from the index

- Remove a folder
	- `git rm -r --cached <folderName>`

- Remove a file
	- `git rm --cached <fileName>`

### Undo a commit.

- `git reset --soft HEAD~1`
	- Undo previous commit. Useful if commited something unwanted, but have not pushed it yet. Won't affect files in working tree.

### Create .gitignore

- `*.ext`
  - Won't track any file with that extension.
- `*string`
  - Won't track any file that includes that string.
- `dir/`
  - Won't track any directory (in any location) with that name.
- `/dir`
  - Won't track any base level directory with that name.
- `/dir/**/*.ext`
  - Won't track any `.ext` file in `/dir` or any of it's subdirectories.

### Tag a release version

~~~ bash
git tag -a v0.1.0 -m 'package version 0.1.0'
git push origin v0.1.0
~~~

### Split a repository

Suppose your git repository has multiple directories you wish to split into their own git repos. The script from <https://github.com/vangorra/git_split> will automate this process along with transferring the appropriate git history to the new repository.

### Change author name and email

This will leave the commit dates unchanged, but the commit hashes will change.

~~~ bash
git filter-branch -f --env-filter "GIT_AUTHOR_NAME='NEW-NAME'; GIT_AUTHOR_EMAIL='NEW-EMAIL'; GIT_COMMITTER_NAME='NEW-NAME'; GIT_COMMITTER_EMAIL='NEW-EMAIL';"
git push origin master --force
~~~

### Change commit date to author date

The commit hashes will change.

~~~ bash
git filter-branch -f --env-filter 'GIT_COMMITTER_DATE=$GIT_AUTHOR_DATE; export GIT_COMMITTER_DATE'
git push origin master --force
~~~

### Change commit messages

This will change the commit dates and the commit hashes. See my note on reversing commit dates if you do not want them to change. Reference <http://stackoverflow.com/a/1186549>.

~~~ bash
git rebase -i --root # change 'pick' to 'e' to edit
git commit --amend
git rebase --continue
~~~

### Remove a tracked file or directory after adding to .gitignore

Suppose you previously committed a file or directory and now have added it to the `.gitignore`. It will still be tracked by `git status`. If you would like git to forget about this file but are ok with the file staying in the commit history, then run the below.

``` bash
# for files
git rm --cached <file>
git add .
git commit -m "Removed ignored files"

# for directories
git rm -r --cached <your directory>
git add .
git commit -m "Removed ignored directory"
``` 

## Solutions for Errors

### Increase buffer size

increase the git buffer size if you receive errors trying to upload over https and you have very large files.

~~~bash
# The number of bytes of your largest file in the directory
git config --global http.postBuffer 157286400
~~~
