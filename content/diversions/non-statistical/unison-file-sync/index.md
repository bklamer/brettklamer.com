---
title: Unison File Sync
author: Brett Klamer
description: Unison file sync tutorial.
diversions: Non-Statistical
tags:
date: 2021-03-07
slug:
---

# Unison File Sync

Suppose you want to sync files between two LAN computers over SSH. One computer, 'primary', may be a mobile/primary device. The other computer, 'secondary', may be a server/desktop that will run the ssh daemon. Below, I describe how to allow the 'primary' access to the 'secondary' and then sync files. This setup is the minimal routing to make things work, but we could also run ssh daemons on both if two way operation was needed.

## Install

On both computers, run

```bash
sudo apt install unison unison-gtk
```

On the secondary computer

```bash
# Install the SSH daemon to listen for logins
sudo apt install openssh-server

# let ssh traffic in
sudo ufw allow ssh
```

## Configure SSH

On both computers, run

```bash
# primary or secondary
ssh-keygen -t rsa -b 4096 -f .ssh/unison-primary
```

The public key of the primary computer should be copied to the file `~/.ssh/authorized_keys` on the secondary computer.

Append the following to the `/etc/sshd_config` file on the secondary computer:

```bash
AuthorizedKeysFile .ssh/authorized_keys

# Disable all auth by default
PasswordAuthentication no
PermitEmptyPasswords no
PubkeyAuthentication no
PermitRootLogin no

# Allow auth from local network
Match Address  192.168.88.*
    PubkeyAuthentication yes
    # if you want, you can even restrict to a specified user
    AllowUsers primary_name@primary_ip
```

Test the ssh connection from the primary computer using:

```bash
ssh -i .ssh/unison-primary secondary_name@secondary_ip
```

Test if unison will connect to secondary computer by running the following from the primary computer:

```bash
ssh -i .ssh/unison-primary secondary_name@secondary_ip unison -version
```

## Configure Unison

Add the following to `~/.unison/default.prf` on the primary computer:

```bash
# primary and secondary paths to sync
root = /home/primary_name/path
root = ssh://secondary_name@secondary_ip//home/secondary_name/path

# Can ignore files (Name) or paths (Path)
ignore = Name *.Rhistory
ignore = Name */.Rproj.user
ignore = Name */.git

# force will do a one way (overwriting) push from primary to secondary.
# Comment out if you want two way sync.
force = /home/primary_name/data

# SSH settings
sshargs = -i /home/primary_name/.ssh/unison-primary

# Ignore file permissions
perms = 0
dontchmod = true
```

## Run unison

Experiment on a test directory. If you created `default.prf`, simply running `unison` from the command line on the primary computer should start the file sync. Alternatively, you can use the gui we installed earlier.

Unfortunately, syncing git repositories is not supported <https://stackoverflow.com/a/31629250>. So make sure to ignore this directory in `default.prf`.

Official documentation is located at <https://www.cis.upenn.edu/~bcpierce/unison/download/releases/stable/unison-manual.html>.
