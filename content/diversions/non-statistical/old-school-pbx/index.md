---
title: Old School PBX
author: Brett Klamer
description: Information for ATT Spirit and Panasonic PBXs.
diversions: Non-Statistical
tags:
date: 2016-07-31
slug:
---

# Old School PBX

Analog phones aren't dead yet. I recently replaced an old ATT Spirit 308 with a Panasonic KX-TA824 PBX. Quite exciting going from 1980s to 2000s tech. As you might expect, there is little support online and what's left may disappear any time. I created a repository on bitbucket with various software and PDF manuals. Check it out here <https://bitbucket.org/bklamer/phones>
