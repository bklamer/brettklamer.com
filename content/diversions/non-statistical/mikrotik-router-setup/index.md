---
title: MikroTik Router Setup
author: Brett Klamer
description: Setup instructions for MikroTik routers.
diversions: Non-Statistical
tags:
date: 2016-10-23
lastmod: 2022-04-24
slug:
---

# MikroTik Router Setup

Below is for a MikroTik router (RouterOS 6.49.6) with WAN 192.168.0.0 (ether1) and LAN 192.168.88.0/24 (ether2-N and wlan1-2).

First, log into the web configuration page at http://192.168.88.1. Reset the router configuration and start with a blank slate. Next, update the firmware and routerOS followed by rebooting the device. On the quickset page, You will probably want the "Home AP Dual" setting as it provides easy setup of wireless and guest wireless networks. The Wireless tab provides further settings you will want to change (such as the security profile).

Once that is finished, you can log in through SSH to quickly finish the setup process

~~~ bash
ssh admin@192.168.88.1

# Remove admin username and update password
/user add name=myname password=mypassword group=full
/user disable admin
/user set myname password="mypassword"

# Disable unneeded services
## 0-telnet 1-ftp 2-www 3-ssh 4-www-ssl 5-api 6-winbox 7-api-ssl
/ip service disable 0,1,5,6,7
/tool bandwidth-server set enabled=no
/ip dns set allow-remote-requests=no
/ip socks set enabled=no
/ip proxy set enabled=no
/ip upnp set enabled=no
/ip cloud set ddns-enabled=no update-time=no

# Enable better ssh encryption (then reboot the device)
/ip ssh set strong-crypto=yes
/ip ssh regenerate-host-key

# Turn off MAC based service servers (they run on WAN).
/tool mac-server set allowed-interface-list=none
/tool mac-server mac-winbox set allowed-interface-list=none
/tool mac-server ping set enabled=no

# Disable "Router Management Overlay Network" RoMON
/tool romon set enabled=no

# Disable router neighbor discovery
/ip neighbor discovery-settings set discover-interface-list=none

# NTP clock synchronization
/system ntp client set enabled=yes 
/system ntp client set servers=0.pool.ntp.org,1.pool.ntp.org,2.pool.ntp.org,3.pool.ntp.org

# Stop spoofed traffic from leaving router into the WAN
/ip settings set rp-filter=strict

# Firewall setup
# http://wiki.mikrotik.com/wiki/Manual:IP/Firewall/Filter#Basic_examples
# http://wiki.mikrotik.com/wiki/Bruteforce_login_prevention
# https://wiki.mikrotik.com/wiki/Manual:Securing_Your_Router
# https://help.mikrotik.com/docs/display/ROS/Building+Your+First+Firewall
## Protect router itself (icmp is optional)
/ip firewall filter
add action=accept chain=input comment="default configuration" connection-state=established,related
add action=accept chain=input src-address-list=allowed_to_router
add action=accept chain=input protocol=icmp
add action=drop chain=input
/ip firewall address-list
add address=192.168.88.2-192.168.88.254 list=allowed_to_router

## Protect LAN devices
/ip firewall address-list
add address=0.0.0.0/8 comment=RFC6890 list=not_in_internet
add address=172.16.0.0/12 comment=RFC6890 list=not_in_internet
add address=192.168.0.0/16 comment=RFC6890 list=not_in_internet
add address=10.0.0.0/8 comment=RFC6890 list=not_in_internet
add address=169.254.0.0/16 comment=RFC6890 list=not_in_internet
add address=127.0.0.0/8 comment=RFC6890 list=not_in_internet
add address=224.0.0.0/4 comment=Multicast list=not_in_internet
add address=198.18.0.0/15 comment=RFC6890 list=not_in_internet
add address=192.0.0.0/24 comment=RFC6890 list=not_in_internet
add address=192.0.2.0/24 comment=RFC6890 list=not_in_internet
add address=198.51.100.0/24 comment=RFC6890 list=not_in_internet
add address=203.0.113.0/24 comment=RFC6890 list=not_in_internet
add address=100.64.0.0/10 comment=RFC6890 list=not_in_internet
add address=240.0.0.0/4 comment=RFC6890 list=not_in_internet
add address=192.88.99.0/24 comment="6to4 relay Anycast [RFC 3068]" list=not_in_internet

/ip firewall filter
add action=fasttrack-connection chain=forward comment=FastTrack connection-state=established,related
add action=accept chain=forward comment="Established, Related" connection-state=established,related
add action=drop chain=forward comment="Drop invalid" connection-state=invalid log=yes log-prefix=invalid
add action=drop chain=forward comment="Drop tries to reach not public addresses from LAN" dst-address-list=not_in_internet in-interface=bridge log=yes log-prefix=!public_from_LAN out-interface=!bridge
add action=drop chain=forward comment="Drop incoming packets that are not NAT`ted" connection-nat-state=!dstnat connection-state=new in-interface=ether1 log=yes log-prefix=!NAT
add action=jump chain=forward protocol=icmp jump-target=icmp comment="jump to ICMP filters"
add action=drop chain=forward comment="Drop incoming from internet which is not public IP" in-interface=ether1 log=yes log-prefix=!public src-address-list=not_in_internet
add action=drop chain=forward comment="Drop packets from LAN that do not have LAN IP" in-interface=bridge log=yes log-prefix=LAN_!LAN src-address=!192.168.88.0/24

/ip firewall filter
  add chain=icmp protocol=icmp icmp-options=0:0 action=accept \
    comment="echo reply"
  add chain=icmp protocol=icmp icmp-options=3:0 action=accept \
    comment="net unreachable"
  add chain=icmp protocol=icmp icmp-options=3:1 action=accept \
    comment="host unreachable"
  add chain=icmp protocol=icmp icmp-options=3:4 action=accept \
    comment="host unreachable fragmentation required"
  add chain=icmp protocol=icmp icmp-options=8:0 action=accept \
    comment="allow echo request"
  add chain=icmp protocol=icmp icmp-options=11:0 action=accept \
    comment="allow time exceed"
  add chain=icmp protocol=icmp icmp-options=12:0 action=accept \
    comment="allow parameter bad"
  add chain=icmp action=drop comment="deny all other types"

# Set DNS servers
## Set use-peer-dns=no so that the router doesn't use DHCP's DNS. 
## Enter number 0 for ether1.
/ip dhcp-client set use-peer-dns=no
/ip dns set servers=208.67.222.222,9.9.9.9,8.8.8.8

## Check print output to make sure allow-remote-requests is "no". 
## If it says yes, others can use your router as a DNS server
/ip dns print

# IPv6 is disabled by default, but if needed
## Disable IPv6 Neighbour Discovery 
/ipv6 nd set [find] disabled=yes

## Create an address-list from which you allow access to the device
/ipv6 firewall address-list add address=fd12:672e:6f65:8899::/64 list=allowed

## IPv6 firewall filter rule
/ipv6 firewall filter
add action=accept chain=input comment="allow established and related" connection-state=established,related
add chain=input action=accept protocol=icmpv6 comment="accept ICMPv6"
add chain=input action=accept protocol=udp port=33434-33534 comment="defconf: accept UDP traceroute"
add chain=input action=accept protocol=udp dst-port=546 src-address=fe80::/16 comment="accept DHCPv6-Client prefix delegation."
add action=drop chain=input in-interface=sit1 log=yes log-prefix=dropLL_from_public src-address=fe80::/16
add action=accept chain=input comment="allow allowed addresses" src-address-list=allowed
add action=drop chain=input
/ipv6 firewall address-list
add address=fe80::/16 list=allowed
add address=xxxx::/48 list=allowed
add address=ff02::/16 comment=multicast list=allowed

## Protect the LAN devices
/ipv6 firewall filter
add action=accept chain=forward comment=established,related connection-state=established,related
add action=drop chain=forward comment=invalid connection-state=invalid log=yes log-prefix=ipv6,invalid
add action=accept chain=forward comment=icmpv6 in-interface=!sit1 protocol=icmpv6
add action=accept chain=forward comment="local network" in-interface=!sit1 src-address-list=allowed
add action=drop chain=forward log-prefix=IPV6

# make a backup - will be located in files
export file=mikrotik-backup
~~~

Now that you're done setting up the router, use these sites to double check everything is working as expected:

- <https://www.dnsleaktest.com/>
- <https://www.grc.com/shieldsup>
- <https://routersecurity.org/testrouter.php>
- <https://test-ipv6.com/>
