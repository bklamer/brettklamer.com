---
title: Securing email at the DNS level
author: Brett Klamer
description: How to secure email at the DNS level
diversions: Non-Statistical
tags:
date: 2022-11-19
lastmod: 
slug:
---

# Securing email at the DNS level

## Securing unused domains

Prevent mail spoofing from unused domains by rejecting all mail.

- MX
    - Type: `MX`
    - Host/Name: `@` or blank
    - Value/Answer: `.`
    - Priority: `0`
- SPF
    - Type: `TXT`
    - Host/Name: `@` or blank
    - Value/Answer: `v=spf1 -all`
- DMARC
    - Type: `TXT`
    - Host/Name: `_dmarc`
    - Value/Answer: `v=DMARC1;p=reject;sp=reject;adkim=s;aspf=s;rua=mailto:dmarcreport@yourdomain.tld`
- DKIM
    - Type: `TXT`
    - Host/Name: `*._domainkey`
    - Value/Answer: `v=DKIM1; p=`

## Securing used domains

Prevent mail spoofing from domains that send and receive mail by requiring authentication.

- MX: Your email host will generally provide three records of decreasing priority as below.
    - Type: `MX`
    - Host/Name: `@` or blank
    - Value/Answer: Provided by your email host. Usually something like `subdomain.domain.tld`
    - Priority: Specified by your email host. Usually `10` to `30`.
- SPF
    - Type: `TXT`
    - Host/Name: `@` or blank
    - Value/Answer: Use the domain(s) specified by your email host. Usually something like `v=spf1 include:domain.tld ~all`
- DMARC
    - Type: `TXT`
    - Host/Name: `_dmarc`
    - Value/Answer: `v=DMARC1;p=reject;sp=reject;adkim=s;aspf=s;rua=mailto:dmarcreport@yourdomain.tld;ruf=mailto:dmarcfailure@yourdomain.tld;fo=1`
        - You will want to verify delivery success before going to "p=reject"
- DKIM
    - Type: `TXT` or `CNAME`
        - If `TXT`: you insert the provided key
        - If `CNAME`: you insert a link to the key
    - Host/Name: Use the value specified by your email host. Usually something like `subdomain._domainkey`
    - Value/Answer:
        - If `TXT`: Provided by your email host. Usually something like `v=DKIM1; p=76E6F0...`
        - If `CNAME`: Provided by your email host.  Usually something like `subdomain._domainkey.domain.tld`
