---
title: The Complete Installation Guide for Xubuntu 22.04
author: Brett Klamer
description: How to install Xubuntu and other software.
diversions: Non-Statistical
tags:
date: 2022-12-09
lastmod: 2023-05-01
slug:
---

# The Complete Installation Guide for Xubuntu 22.04

This is a guide for installing Xubuntu 22.04 &ndash; the hard way. The partitioning scheme is laid out as:

~~~
+-------------------------++-----------++-----------++---------------------------+
|                         ||           ||           || Logical volume1 XX GB     |
|                         ||           ||           || /dev/mapper/vgxubuntu-root|
| 2 GB                    ||           || 128 MB    ||_ _ _ _ _ _ _ _ _ _ _ _ _ _|
| dm-crypt LUKS LVM       || 2 MB      || EFI       || dm-crypt LUKS LVM         | 
| /dev/mapper/LUKS_BOOT   || bios_boot || System    || /dev/mapper/sda5_crypt    |
|_ _ _ _ _ _ _ _ _ _ _ _ _|| GRUB Core || Partition || _ _ _ _ _ _ _ _ _ _ _ _ _ |
| dm-crypt LUKS partition ||           ||           || dm-crypt LUKS encrypted   |
| /dev/sda1               || /dev/sda2 || /dev/sda3 || /dev/sda5                 |
+-------------------------++-----------------------------------------------------+
~~~

## Warnings

1. You may need to change some options for your install.
2. Test in a virtual machine before use. Real hardware will still be different.
3. If grub gets installed on the USB installation device, simply reboot with the installation USB, log in, and run `sudo dpkg-reconfigure grub-pc`. This will show a text interface where you can choose the installation disk(s).

## Sections

- [Installation](#install)
- [Post install](#post-install)
- [Software](#software)

## <a name="install"></a>Install Xubuntu 22.04 with dm-crypt LUKS encryption for all partitions

1. Download Xubuntu 22.04 at <https://xubuntu.org/download>.
1. Create a bootable USB. I suggest using one of
    - startup disk creator: `sudo apt install usb-creator-gtk`
    - `sudo add-apt-repository ppa:mkusb/ppa; sudo apt update; sudo apt install mkusbl; sudo mkusb-dus`
    - <https://github.com/jsamr/bootiso>
1. Reboot computer from USB.
1. Installer boot menu
    1. Try Xubuntu without installing
1. Make sure you are using UEFI on the computer. You can check if the installation media is using UEFI by running the following code in a terminal while testing the installation `[ -d /sys/firmware/efi ] && echo "EFI boot" || echo "Legacy boot"`.
1. Follow directions from <https://help.ubuntu.com/community/Full_Disk_Encryption_Howto_2019> for full partition encryption.
    1. Open the terminal

        ```
        #----------------------------------------------------------------------
        # Identify installation device
        #----------------------------------------------------------------------
        # Switch to root user.
        sudo -i

        # Determine the target drive location.
        lsblk

        # Save shortcut reference to drive location (non-NVME drive).
        #export DEV="/dev/sda"

        # Save shortcut reference to drive location (NVME drive).
        export DEV="/dev/nvme0n1"

        # Shortcut reference to encrypted device mapper without leading `/dev/`.
        export DM="${DEV##*/}"

        # NVME devices need a 'p' before partition number. i.e. "nvme0n1p1".
        export DEVP="${DEV}$( if [[ "$DEV" =~ "nvme" ]]; then echo "p"; fi )"
        export DM="${DM}$( if [[ "$DM" =~ "nvme" ]]; then echo "p"; fi )"

        #----------------------------------------------------------------------
        # Partitioning
        #----------------------------------------------------------------------
        # Check for pre-existing partitions.
        sgdisk --print $DEV

        # ***If safe to delete all partitions***
        #sgdisk --zap-all $DEV

        # If anything goes wrong, check Gparted or `fdisk -l $DEV`.
        sgdisk --new=1:0:+2G $DEV
        sgdisk --new=2:0:+2M $DEV
        sgdisk --new=3:0:+128M $DEV
        sgdisk --new=5:0:0 $DEV
        sgdisk --typecode=1:8301 --typecode=2:ef02 --typecode=3:ef00 --typecode=5:8301 $DEV
        sgdisk --change-name=1:/boot --change-name=2:GRUB --change-name=3:EFI-SP --change-name=5:rootfs $DEV
        sgdisk --hybrid 1:2:3 $DEV

        #----------------------------------------------------------------------
        # LUKS Encryption
        #----------------------------------------------------------------------
        # Make sure to use luks version 1 on /boot/ since GRUB requires it.
        cryptsetup luksFormat --type=luks1 ${DEVP}1

        # Set up LUKS on the system partition.
        cryptsetup luksFormat ${DEVP}5

        #  Open the encrypted devices.
        cryptsetup open ${DEVP}1 LUKS_BOOT
        cryptsetup open ${DEVP}5 ${DM}5_crypt
        ls /dev/mapper/

        #----------------------------------------------------------------------
        # Format file systems
        #----------------------------------------------------------------------
        mkfs.ext4 -L boot /dev/mapper/LUKS_BOOT
        mkfs.vfat -F 16 -n EFI-SP ${DEVP}3

        #----------------------------------------------------------------------
        # LVM Logical Volume Manager
        #----------------------------------------------------------------------
        # Naming scheme for different releases of ubuntu
        flavour="$( sed -n 's/.*cdrom:\[\([^ ]*\).*/\1/p' /etc/apt/sources.list )"
        release="$( lsb_release -sr | tr -d . )"
        if [ ${release} -ge 2204 ]; then VGNAME="vg${flavour,,}"; else VGNAME="${flavour}--vg"; fi 
        export VGNAME

        # Create Volumes
        pvcreate /dev/mapper/${DM}5_crypt
        vgcreate "${VGNAME}" /dev/mapper/${DM}5_crypt
        # If you want a swap partition/volume
        #lvcreate -L 1G -n swap_1 "${VGNAME}"
        lvcreate -l 80%FREE -n root "${VGNAME}"
        ```
    
    1. Keep terminal open, but switch back to proceeding with installation.

1. Installer main menu
    1. Keyboard layout
        1. English (US)
    1. Updates and other software
        1. Download updates while installing Xubuntu
        1. Install third-party software
    1. Installation type
        1. Something else
            1. Select the root file-system device for formatting (`/dev/mapper/vgxubuntu-root`), press the Change button, choose Use As Ext4 and Mount point `/`.
            1. *If you created a swap volume*: Select the swap device (`/dev/mapper/vgxubuntu-swap_1`), press the Change button, choose Use as swap area.
            1. Select the Boot file-system device for formatting (`/dev/mapper/LUKS_BOOT`), press the Change button. choose Use as Ext4... and Mount point /boot
            1. Select the boot-loader device (`/dev/nvme0n1p1` for example). Boot-loader device should always be a raw disk not a partition or device-mapper node.
            1. Press the Install Now button to write the changes to the disk and press the Continue button.
    1. Where are you
    1. Who are you
        - After finishing this step, immediately perform next step. The next step needs to be run before installation is finished in the background?
    1. Open the terminal
        1. `while [ ! -d /target/etc/default/grub.d ]; do sleep 1; done; echo "GRUB_ENABLE_CRYPTODISK=y" > /target/etc/default/grub.d/local.cfg`
            - Check file for successful addition of text.
    1. After successful installation, choose continue testing.
    1. Open the terminal

        ```
        #-----------------------------------------------------------------------
        # Change-root environment to work in the newly installed OS
        #-----------------------------------------------------------------------
        mount /dev/mapper/${VGNAME}-root /target
        for n in proc sys dev etc/resolv.conf; do mount --rbind /$n /target/$n; done
        chroot /target
        mount -a

        #-----------------------------------------------------------------------
        # Configure cryptsetup-initramfs and key file
        #-----------------------------------------------------------------------
        apt install -y cryptsetup-initramfs
        
        # Note that since we chroot'd to `/target` the following changes will be
        # made in the files at the /target path.
        echo "KEYFILE_PATTERN=/etc/luks/*.keyfile" >> /etc/cryptsetup-initramfs/conf-hook
        echo "UMASK=0077" >> /etc/initramfs-tools/initramfs.conf
        
        # Create a randomised key-file of 4096 bits (512 bytes), secure it, and add it to the LUKS volumes
        mkdir /etc/luks
        dd if=/dev/urandom of=/etc/luks/boot_os.keyfile bs=512 count=1
        
        chmod u=rx,go-rwx /etc/luks
        chmod u=r,go-rwx /etc/luks/boot_os.keyfile
        
        cryptsetup luksAddKey ${DEVP}1 /etc/luks/boot_os.keyfile
        cryptsetup luksAddKey ${DEVP}5 /etc/luks/boot_os.keyfile
        
        # Add the keys to the crypttab
        echo "LUKS_BOOT UUID=$(blkid -s UUID -o value ${DEVP}1) /etc/luks/boot_os.keyfile luks,discard" >> /etc/crypttab
        echo "${DM}5_crypt UUID=$(blkid -s UUID -o value ${DEVP}5) /etc/luks/boot_os.keyfile luks,discard" >> /etc/crypttab
        
        # Update the initialramfs files to add the cryptsetup unlocking scripts and the key-file
        update-initramfs -u -k all
        ```

1. Reboot the computer and remove installation media. You should be asked for the password at GRUB.

## <a name="post-install"></a>Post install options

1. Set audio/sound/volume level using keyboard shortcuts.

    In Settings -> Keyboard -> Application Shortcuts, click add, then insert

    ~~~bash
    amixer -D pulse set Master 5%+
    amixer -D pulse set Master 5%-
    amixer -D pulse set Master toggle
    ~~~
1. Set window tile keybinds.
    1. Open terminal and run
        1. `xfce4-settings-manager`
    1. Go to window manager
        1. Open the Keyboard tab
            1. Set the "Tile window to the left" (and right)
1. Set backbutton in firefox to backsapce.
    1. Type `about:config` in the address bar
	  1. Look for `browser.backspace_action` in the list
	  1. Change the Value to 0.
1. Change desktop lock keybind.
    1. Go to settings editor
    1. xfce4-keyboard-shortcuts
    1. new commands custom property
        1. property: `/commands/custom/<super>l`
        1. type: `string`
        1. value: `xflock4`
1. Fix laptop screen brightness steps

    In Settings -> Power Manager -> General, there is an option for "Brightness step count". Increase this to a value such as 20.
    
    Otherwise, tools such as [acpilight](https://gitlab.com/wavexx/acpilight), [light](https://github.com/haikarainen/light), and [xbacklight](https://www.x.org/releases/X11R7.5/doc/man/man1/xbacklight.1.html) may be of help. If you `sudo make install` acpilight, then you will need to run `sudo usermod -a -G video USERNAME` so that sudo is not required to execute `xbacklight`. You can then bind the command to a key combo under Settings -> Keyboard -> Application Shortcuts.
1. Enable the firewall.
    
    ~~~bash
    # https://help.ubuntu.com/community/UFW
    # https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw/
    sudo ufw enable
    sudo ufw default deny outgoing
    sudo ufw default deny incoming

    sudo ufw allow out to any port 80
    sudo ufw allow out to any port 443
    sudo ufw allow out to any port 53

    # additional ports to allow out
    # https://www.cups.org/doc/firewalls.html
    # brother printer: 54925, 54926, 137, 161

    sudo ufw reload
    sudo ufw status verbose

    # If anything goes wrong, you can reset ufw
    #sudo ufw --force reset
    ~~~
1. Edit the hosts file
    1. Grab the hosts file from <https://github.com/StevenBlack/hosts>
    1. paste into `/etc/hosts`
1. Update the Linux kernel.
    1. If you want a specific kernel
        1. Download the following from <http://kernel.ubuntu.com/~kernel-ppa/mainline/>
            1. `linux-headers-5.*-generic_*_amd64.deb`
            1. `linux-headers-5.*_all.deb`
            1. `linux-image-unsigned-5.*-generic_*_amd64.deb`
            1. `linux-modules-5.*-generic_*_amd64.deb`
        1. Open terminal in download location and run
            1. `sudo dpkg -i linux-headers*.deb`
            1. `sudo dpkg -i linux-modules*.deb`
            1. `sudo dpkg -i linux-image*.deb`
            1. `sudo update-grub`
        1. Restart computer
        1. Check kernel being used with
            1. `uname -a`
        1. Remove old kernels if `/boot` gets full
1. Check disk io/r/w transactions
    1. `sudo iotop -oPa`
1. If there is a separate partition or disk that needs to be mounted and unencrypted at boot. Reference <http://ubuntuforums.org/showthread.php?t=837416>.
    1. Check UUID of partitioning
        1. `sudo blkid`
    1. Check block size of / (root) partition for nice key size (likely 4096)
        1. `sudo blockdev --getbsz /dev/mapper/system-root`
    1. Create random keyfile in /root
        1. `sudo dd if=/dev/urandom of=/root/keyfile bs=4096 count=1`
            - the bs= value should be the block size we just found
    1. Make keyfile read only to root
        1. `sudo chmod 0400 /root/keyfile`
    1. Add keyfile to LUKS partition of /dev/sdX#_crypt
        1. `sudo cryptsetup luksAddKey /dev/sdX#_crypt /root/keyfile`
            - Enter existing password for /dev/sdX#_crypt
    1. Create mapper
        1. `sudo mousepad /etc/crypttab`
        1. add '/root/keyfile' to replace 'none' for /dev/sdX#_crypt
            - example: `sdX#_crypt UUID=XXX /root/keyfile luks`
    1. Mount the drive (if needed)
        1. `sudo mousepad /etc/fstab`
            - example: `/dev/mapper/sdX#_crypt /<mount point> btrfs relatime 0 2`
    1. Update settings in initramfs images
        1. `sudo update-initramfs -u -k all`
1. TRIM for SSDs.
    1. Reference <http://blog.neutrino.es/2013/howto-properly-activate-trim-for-your-ssd-on-linux-fstrim-lvm-and-dmcrypt/>.
    1. Enable Trim on dm-crypt
        1. Open `/etc/crypttab`
            1. `sudo mousepad /etc/crypttab`
            1. If needed, add 'discard' to the options for `sdX#_crypt`.
    1. Make sure LVM has 'issue_discards=1' in
        1. `sudo mousepad /etc/lvm/lvm.conf`
    1. Check encrypted drive with
        1. `sudo dmsetup table /dev/mapper/sdX#_crypt`
        2. make sure it has '1 allow_discards'
    1. Remove or check "discard" is not used in the `fstab`
        1. `sudo mousepad /etc/fstab`
    1. Run TRIM manually or check for errors
        1. `sudo fstrim -v /home`
    1. If any changes were made, run
        1. `sudo update-initramfs -c -k all`
1. If installing in Virtualbox, install additions by
    1. `sudo apt install virtualbox-guest-utils virtualbox-guest-dkms dkms`
    1. To share a folder, make a permanent machine folder then run
        1. `sudo usermod -a -G vboxsf username`
    1. To share a USB port
        1. `sudo usermod -a -G vboxusers username`
1. If needed, install Intel wireless driver.
    1. Download driver from <http://intellinuxwireless.org/?n=Downloads>
    1. Navigate to download folder
        1. `tar xvzf iwlwifi-XXX.tgz`
        1. `cd iwlwifi-XXX/`
        1. `sudo cp iwlwifi-XXX.ucode /lib/firmware`
1. Check partition sizes.
    1. `df -h`
1. Modify or redirect home folder names.
    1. change in `/home/username/.config/user-dir.dirs`
1. Change ownership of extra storage drives or partitions.
    1. `sudo chown -R username /partition`
1. Format a USB drive.
    1. `df`
    1. `umount /dev/sdc1`
    1. `mkfs.vfat /dev/sdc1`
1. Create a dm-crypt LUKS encrypted external drive. 

    Reference <https://gitlab.com/cryptsetup/cryptsetup/wikis/FrequentlyAskedQuestions>.
    
    1. Find the external drive (assume the filesystem is /dev/sdb1 and it's mount location /media/USERNAME/*)
    
        ~~~bash
        df
        ~~~

    2. Unmount it

        ~~~bash
        umount /media/USERNAME/*
        ~~~

    3. Quickly wipe old filesystems. wipefs clears the first superblock.

        ~~~bash
        sudo wipefs -a /dev/sdb1
        ~~~

    4. Create the LUKS container (follow on-screen intructions)

        ~~~bash
        sudo cryptsetup luksFormat /dev/sdb1
        ~~~

    5. Check the passphrase iteration count. The key slot default is 1 second of PBKDF2 hashing. The volume key default (MK iterations) is 0.125 seconds. You can set the key slot with `cryptsetup luksFormat -i 15000 <target device>`

        ~~~bash
        sudo cryptsetup luksDump /dev/sdb1
        ~~~

    6. Map the container to /dev/mapper/backup1

        ~~~bash
        sudo cryptsetup luksOpen /dev/sdb1 backup1
        ~~~

    7. Create a filesystem in the mapped container

        ~~~bash
        sudo mkfs.btrfs --label backup1 /dev/mapper/backup1
        ~~~

    8. Mount the filesystem (right after creation; using lzo compression)

        ~~~bash
        mount -o compress=lzo /dev/mapper/backup1 /mnt
        ~~~

    9. Mount the filesystem (day to day use as a portable external drive; using lzo compression). You can either create an fstab entry or mount using the command line.

        - Using an fstab entry

            ~~~bash
            # Get the UUID of the mounted and unlocked /dev/mapper/ filesystem
            sudo blkid
            ~~~
        
            Add the following entry to `/etc/fstab`

            ~~~bash
            UUID=YOUR-UUID /media/backup1 btrfs noauto,defaults,noatime,compress=lzo 0 0
            ~~~
            Now it will automatically mount at `/media/backup1`. The `noauto` option is used in the fstab entry to prevent automatically mounting the drive at boot time. If you leave this option off, then your computer will fail to boot and you will need to edit the fstab in recovery mode. The `nofail` option can be used for drives that are usually going to be mounted at boot time.

            Change ownership of the new mount point so you can perform cut/copy/paste, etc.

            ~~~bash
            sudo chown -R USERNAME /media/backup1
            ~~~

       - Using the terminal

            ~~~bash
            # The OS will automatically mount the drive and ask for passphrase to unlock. Then...
            df
            sudo umount /media/USERNAME/*
            sudo mount -o compress=lzo /dev/dm-4 /media/backup1
            sudo chown -R USERNAME /media/backup1
            ~~~

1. Stop system error pop ups.

    Sometimes a system error will be reported and cause a warning pop up over multiple restarts. You can remove this by either

    1. `sudo rm /var/crash/*`
    2. `gksu nano /etc/default/apport` and set `enabled=0`

1. Randomize MAC address.

    This is based on <https://fedoramagazine.org/randomize-mac-address-nm/>. To randomize wifi connections, create the file `/etc/NetworkManager/conf.d/00-macrandomize.conf` and add the following:

    ~~~bash
    # can use 'random' or 'stable' below
    [device]
    wifi.scan-rand-mac-address=yes

    [connection]
    wifi.cloned-mac-address=stable
    ethernet.cloned-mac-address=stable
    connection.stable-id=${CONNECTION}/${BOOT}
    ~~~

    Then restart networkmanager with `systemctl restart NetworkManager`.

1. Change owner of entire directory.

    ~~~bash
    sudo chown -R <username> *
    ~~~

1. Let apt fix dependency issues automatically.

    ~~~bash
    sudo apt --fix-broken install
    ~~~

1. GPG bug fix when adding keys behind a proxy: use the option `http-proxy=`

    ~~~bash
    sudo apt-key adv --keyserver keyserver.ubuntu.com --keyserver-options http-proxy=http://PROXYADDRESS --recv-keys GPGKEY
    ~~~
1. Fix bluetooth audio stuttering.

    Open a terminal and run

    ~~~bash
    sudo mousepad /etc/bluetooth/audio.conf
    ~~~

    Then add the following text to the new file:

    ~~~bash
    [General]
    Enable=Source,Sink,Media,Socket
    ~~~

    Finally, restart the bluetooth service

    ~~~bash
    sudo service bluetooth restart
    ~~~
1. Fix bluetooth audio not working.

    Open a terminal and run

    ~~~bash
    lsmod | grep btusb
    sudo rmmod btusb
    lsmod | grep btusb
    sudo modprobe btusb
    lsmod | grep btusb
    bluetoothctl
    scan on
    ~~~
1. Fix QT scaling for hidpi displays.

    Open `~/.profile` and append
    
    ~~~bash
    export QT_SCALE_FACTOR=2
    ~~~

## <a name="software"></a>Software install suggestions

### Apt packages

<pre class="scroll"><code>#----------------------------------------------------------------------
# Update apt
#----------------------------------------------------------------------
sudo apt update
sudo apt full-upgrade

#----------------------------------------------------------------------
# Remove unwanted software
#----------------------------------------------------------------------
sudo apt purge '^brltty.*' '^espeak.*' '^hplip.*' '^libhpmud0.*' '^libsane-hpaio.*' '^parole.*' '^printer-driver.*' '^speech-dispatcher.*' '^whoopsie.*' '^libwhoopsie0.*' '^popularity-contest.*' '^pidgin.*'

# Cleanup
sudo apt autoremove
sudo apt autoclean

#----------------------------------------------------------------------
# Install software
#----------------------------------------------------------------------
# General
sudo apt install git libssl-dev curl build-essential checkinstall autoconf automake libdbd-sqlite3 software-properties-common iotop p7zip-full audacious flatpak gimp

# ios device plug and play
sudo apt install usbmuxd libimobiledevice6 libimobiledevice-utils

# Install dependencies for R and RStudio
sudo apt install libxml2-dev libicu-dev zlib1g-dev make pandoc libcurl4-openssl-dev
sudo apt install libclang-dev libpq5

#----------------------------------------------------------------------
# Install other repository software
#----------------------------------------------------------------------
# Repos
## KeepassXC
sudo add-apt-repository ppa:phoerious/keepassxc
## mpv
sudo add-apt-repository ppa:mc3man/mpv-tests
## Inkscape
sudo add-apt-repository ppa:inkscape.dev/stable
## sublime text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list

# Install
sudo apt update
sudo apt install keepassxc mpv inkscape sublime-text

#----------------------------------------------------------------------
# Cleanup
#----------------------------------------------------------------------
sudo apt update
sudo apt full-upgrade
sudo apt autoclean
sudo apt autoremove
</code></pre>

### Manual packages

1. Install [rig](https://github.com/r-lib/rig)

    rig automatically installs [pak](https://pak.r-lib.org/) in the user library. 
    pak is nice because it installs R packages as binaries instead of from source.
    However, OS package dependencies are installed in the background using sudo,
    and we need to allow a graphical interface to insert the password. So we solve 
    that by running:
    
    ```{bash}
    sudo apt install ssh-askpass ssh-askpass-gnome
    ```
    
    then append to the file `/etc/sudo.conf`
    
    ```{bash}
    # append to /etc/sudo.conf
    Path askpass /usr/bin/ssh-askpass
    ```
    
    Each OS package dependency is executed in a seperate process, which means each
    process requires its own password authentication. We need to add a global sudo
    time limit for all processes.
    
    ```{bash}
    sudo visudo -f /etc/sudoers.d/timestamp_type
    ```
    
    then paste in the following text:
    
    ```{bash}
    # specify the timeout type (usual default=tty)
    Defaults:USERNAME timestamp_type=global

    # specify the timeout interval (usual default=15)
    Defaults:USERNAME timestamp_timeout=2
    ```
    
    Finally, we install rig:
    
    ```{bash}
    `which sudo` curl -L https://rig.r-pkg.org/deb/rig.gpg -o /etc/apt/trusted.gpg.d/rig.gpg
    `which sudo` sh -c 'echo "deb http://rig.r-pkg.org/deb rig main" > /etc/apt/sources.list.d/rig.list'
    `which sudo` apt update
    `which sudo` apt install r-rig
    ```
    
    and then install R
    
    ```{bash}
    rig add release
    ```
    
    and can use `pak::pkg_install()` without any issues.

1. Install [GnuCash](https://www.gnucash.org/)

    ```{bash}
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    flatpak install flathub org.gnucash.GnuCash
    
    # uninstall
    #flatpak uninstall org.gnucash.GnuCash
    
    # Update stock quotes
    # https://wiki.gnucash.org/wiki/Flatpak
    flatpak run --command=gnucash-cli org.gnucash.GnuCash --quotes get /path/to/file.gnucash
    ```
    
1. Install [texlive](https://www.tug.org/texlive/) 2022

    Reference <http://tex.stackexchange.com/a/95373>.
    
    1. Run

        ~~~ bash
        sudo apt install wget perl-tk

        wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
        tar -zxvf install-tl-unx.tar.gz
        cd install-tl*
        sudo ./install-tl --gui
        ~~~
        
        - Choose the small scheme (just download fonts and packages as you need them)
        - Choose Recommended fonts, Mathematics packages, and LuaTeX packages
        - Make sure to "create symlinks in system directories"

    1. Run following from the shell. It will add the lines to `/etc/environment`. Or add to `~/.profile` manually.

        ~~~ bash
        export MANPATH="$MANPATH:/usr/local/texlive/2022/texmf-dist/doc/man"
        export INFOPATH="$INFOPATH:/usr/local/texlive/2022/texmf-dist/doc/info"
        export PATH=/usr/local/texlive/2022/bin/x86_64-linux:$PATH
        ~~~

    1. Make apt see the local install by:

        ~~~ bash
        sudo apt install equivs --no-install-recommends
        sudo apt install freeglut3
        mkdir /tmp/tl-equivs
        cd /tmp/tl-equivs
        equivs-control texlive-local
        # copy this http://www.tug.org/texlive/files/debian-equivs-2022-ex.txt to
        mousepad texlive-local
        equivs-build texlive-local
        sudo dpkg -i texlive-local_2022-1_all.deb
        sudo apt install -f
        ~~~

    1. Access tlmgr using `sudo tlmgr --gui`

    1. Update texlive. Remove the old texlive with the commands below, then Re-run the install commands.

        ~~~ bash
        # Remove old apt local install
        sudo apt remove texlive-local

        # Remove the old texlive directories
        sudo rm -rf /usr/local/texlive/2022/
        sudo rm -rf /usr/local/texlive/texmf-local/
        sudo rm -rf ~/.texlive2022/
        sudo rm -rf /var/lib/texmf/

        # Remove the old texlive symlinks (Make sure there's nothing else in there)
        sudo rm /usr/local/bin/*
        sudo -rf rm /usr/local/share/man/*
        sudo rm /usr/local/share/info/*

        # Update the font cash
        sudo fc-cache -fsv
        ~~~

1. Install [Redshift](https://github.com/jonls/redshift)
    1. Install from repo

        ~~~bash
        sudo apt-get install redshift redshift-gtk
        ~~~
    1. Append the following to geoclue's config with `sudo nano /etc/geoclue/geoclue.conf`

        ~~~bash
        [redshift]
        allowed=true
        system=false
        users=
        ~~~
    1. Create a redshift config file with `nano ~/.config/redshift.conf`
    
        ~~~bash
        [redshift]
        temp-day=5500
        temp-night=2700
        location-provider=manual

        [manual]
        lat=4X
        lon=-8X
        ~~~
    1. Alternative software
        - https://sr.ht/~kennylevinsen/wlsunset/
        - https://gitlab.com/chinstrap/gammastep
1. Modify sublime text's settings
    1. At Preferences -> Distraction Free, add the line `"update_check": false,`
    1. Add `127.0.0.1 license.sublimehq.com` to `/etc/hosts`.

1. Install [Rstudio](https://posit.co/)

    Download from <https://posit.co/download/rstudio-desktop/>

    ~~~bash
    sudo dpkg -i *.deb
    sudo apt install -f
    ~~~

1. Install [Teamviewer](https://www.teamviewer.com/en-us/)
    
    Download from <https://www.teamviewer.com/en/download/linux/>

    ~~~bash
    sudo dpkg -i *.deb
    sudo apt install -f
    ~~~

1. Install [Anydesk](https://anydesk.com/en)
    
    Download from <https://anydesk.com/download?os=linux>

    ~~~bash
    sudo dpkg -i *.deb
    sudo apt install -f
    ~~~

    Anydesk uses a dark pattern of forcing autostart of a background service/system tray icon. To stop it, you needed to delete the following file:

    ~~~bash
    /etc/systemd/system/anydesk.service
    ~~~

1. Install Brother printer drivers.
    
    Download from <http://support.brother.com/g/b/productsearch.aspx?c=us&lang=en&content=dl>
    
    1. `gunzip linux-brprinter-installer-2.*.gz`
    1. `sudo bash linux-brprinter-installer-2.*`
    1. Enter machine name
    1. When you see the message "Will you specify the DeviceURI ?" USB Users: Choose N(No). Network Users: Choose Y(Yes).
    1. If scanner isn't working
        1. `brsaneconfig4 -a name=Scanner model='modelname' ip='ip-address'`
    1. Check network mapping with
        1. `nmap 'IP range'/24`
