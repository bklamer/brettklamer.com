---
title: Install Eventmachine in Ruby 2 for Windows
author: Brett Klamer
description: How to install eventmachine in Ruby 2 for Windows.
diversions: Non-Statistical
tags:
date: 2014-02-01
slug:
---

# Install Eventmachine in Ruby 2 for Windows

As of Ruby 2.0.0-p353 and DevKit 4.7.2 you cannot install eventmachine (up to at least 1.0.3). This bug has been around since at least March of 2013 as indicated in the following:

<ul>
<li><a href="https://github.com/eventmachine/eventmachine/issues/408">https://github.com/eventmachine/eventmachine/issues/408</a></li>
<li><a href="https://github.com/eventmachine/eventmachine/pull/411">https://github.com/eventmachine/eventmachine/pull/411</a></li>
<li><a href="https://github.com/eventmachine/eventmachine/issues/412">https://github.com/eventmachine/eventmachine/issues/412</a></li>
<li><a href="https://github.com/eventmachine/eventmachine/pull/450">https://github.com/eventmachine/eventmachine/pull/450</a></li>
<li><a href="https://stackoverflow.com/questions/17361958/eventmachine-gem-install-fail/17386686">https://stackoverflow.com/questions/17361958/eventmachine-gem-install-fail/17386686</a></li>
</ul>

Since the Ruby maintainers seem to be ignoring the issue, who knows when it will be fixed. Not knowing much about Ruby, I couldn't get around the problem following just one example. After combining a few things, here are the steps needed to fix the error.

## 1. Install eventmachine

Install eventmachine using `gem install eventmachine`. Ignore the errors for now.

## 2. Edit `project.h`


Find the file `project.h` at or around

~~~
C:\Ruby200-x64\lib\ruby\gems\2.0.0\gems\eventmachine-1.0.3\ext
~~~

and use <a href="https://github.com/eventmachine/eventmachine/pull/450/files">Claudius Coenen's fix</a> at line 97-98 to redefine

~~~
typedef int pid_t;
#endif
~~~

to

~~~
#ifndef _PID_T_
#define _PID_T_
typedef int pid_t;
#endif /* _PID_T_ */
#endif /* OS_WIN32 */
~~~

## 3. Install git

Download git from <a href="http://git-scm.com/download/win">here</a> and install. Choose the option to add git.exe to the PATH. When it's finished, restart the computer.

## 4. Install eventmachine (this time for real)

Using <a href="https://stackoverflow.com/questions/17361958/eventmachine-gem-install-fail/17386686">Fernando Vieira's answer</a> (which was edited to include the relevant info), run

~~~ bash
gem build eventmachine.gemspec
~~~

from the folder at or around

~~~
C:\Ruby200-x64\lib\ruby\gems\2.0.0\gems\eventmachine-1.0.3
~~~

Which should have created the file `eventmachine-1.0.3.gem`. Next, copy that file to a temporary folder, and from that folder run

~~~ bash
gem install eventmachine-1.0.3.gem --local
~~~

And it's finally installed!
