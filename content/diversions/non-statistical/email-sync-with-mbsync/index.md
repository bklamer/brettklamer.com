---
title: Email Sync With mbsync
author: Brett Klamer
description: Email management with mbsync.
diversions: Non-Statistical
tags:
date: 2017-12-19
slug:
---

# Email Sync With mbsync

[mbsync](http://isync.sourceforge.net/mbsync.html) is a command line utility for email synchronization. It's a little arcane, but is fast and has many features. Before using, you will need to create a default configuration file at `~/.mbsyncrc` (it's mandatory to have one). The configuration settings below will create a local copy of your email account without the danger of propagating back any changes.

```
# Note that whitespace below is important
CopyArrivalDate yes # Keeps the time stamp based message sorting intact.

IMAPAccount ACCOUNTNAME # Account reference name
Host        IMAPDOMAIN.com
User        USER@EMAILDOMAIN.com
#Pass       **** # mbsync will prompt for pass if left blank
SSLType 	IMAPS # If you need to connect to port 993
SSLVersions TLSv1.2

IMAPStore ACCOUNTNAME-remote
Account   ACCOUNTNAME

MaildirStore ACCOUNTNAME-local
Path         ~/ACCOUNTNAME-local/
Inbox        ~/ACCOUNTNAME-local/Inbox
SubFolders   Verbatim

Channel  ACCOUNTNAME
Master   :ACCOUNTNAME-remote:
Slave    :ACCOUNTNAME-local:
Patterns * # Will copy the remote email account as is
Create   Slave
Expunge  Slave
Sync     Pull
MaxSize  100m # Don't download any email greater than this
```

Before running, create the maildirstore directories. Start synchronizing by running `mbsync ACCOUNTNAME` from the terminal. After using, `~/.mbsync/` will be created and contain synchronization state files.

More than one account can be synchronized by creating two blocks in the configuration file. Just create a separate section in `~/.mbsyncrc` with a different account name. `mbsync -a` will run synchronization for all channels.

The `SubFolders` command requires mbsync >=1.3 so you may need to compile from source if your package manager is not up to date.
