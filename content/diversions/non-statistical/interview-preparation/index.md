---
title: Interview Preparation
author: Brett Klamer
description: Some tips on preparing for interviews.
diversions: Non-Statistical
tags:
date: 2016-02-04
slug:
---

# Interview Preparation

## Resources

- [Elements of Programming Interviews](https://amazon.com/dp/1479274836) by Adnan Aziz, et al.
- [Cracking the Coding Interview](https://amazon.com/dp/0984782850) by Gayle McDowell
- [Programming Interviews Exposed](https://amazon.com/dp/1118261364) by John Mongan, et al.

## Behavioral and Non-technical Questions

Companies generally look for someone with the right personality, skill set, and prior experience. Think about what the company is and your potential role when answering the following questions.

1. Tell me about yourself...
    - Create a narrative that rehashes your resume, starting with your undergraduate education. You may be speaking with someone who hasn't looked at your resume...
2. Tell me about your experience...
    - Talk about your previous assignments/projects: what you learned, what went right, what went wrong, etc.
3. What are your weaknesses?
    - Use a legitimate weakness but then spin it into how it can actually be beneficial, how you overcome it, etc.
4. Where do you see yourself in 5 years / What are your career goals?
    - Where I'll be in five years is unknown, but it will be built on the knowledge I've accumulated up to now.
    - Explain why you want this job and how it fits into your career.
    - Remember that some positions are designed to be temporary.
5. Why are you looking to change jobs?
    - Change in environment, factors out of your control, etc.
6. What are your salary requirements?
    - Give a range of salaries with your desired amount at the low end. Check <http://glassdoor.com> to determine the minimum amount you should offer.
    - If offered the job, always give a counter offer around 10% higher. You have nothing to lose and it's your last chance to bargain. You are powerless once employed. HR is not your advocate and their only goal is to hire you at the lowest salary possible and keep you employed at the lowest salary possible. Try to use email for salary negotiation.
7. What is your salary history?
    - Explain that you expect market appropriate compensation and your compensation for the previous employment isn't relevant.
8. Why should we hire you?
    - Explain why you want to work at the company and why you are a good match. Stay positive.
9. Why do you want to work for this company?
    - Show what you know about the company and why you are excited about it.

Recreate the following table and fill in with keywords (McDowell 2011) You should be able to tell a short story for all of these questions for any major areas found on your resume. Have it available when conducting phone interviews.

| Common Questions         | Project 1 | Project 2 | Project 3 |
|--------------------------|-----------|-----------|-----------|
| Most Challenging         |           |           |           |
| What You Learned         |           |           |           |
| Most Interesting         |           |           |           |
| Hardest Bug              |           |           |           |
| Enjoyed Most             |           |           |           |
| Conflicts with Teammates |           |           |           |

## Technical Questions

1. Clarify the question.
2. Think out loud.

## Questions to Ask the Interviewer

1. How much of the day will be spent programming vs. writing reports?
1. How many meetings will we be having each week?
1. What measures are taken for data security?
1. Do you use software I may not be familiar with?
1. Can I create public, open source repositories for the types of projects I would be working on?
1. Was there always someone in this position or were duties spread across multiple people?
1. What made past employees excel in this role?
1. What kind of person would not work out well in this role?
1. How can I improve my qualifications to be a better fit?
1. When do you face the toughest time or have the longest days in this position?
1. Can you tell me about the team I'd be most closely working with?
1. What are you most proud of in your work so far?
1. What problem is your company solving? -> Who has this problem? -> What's your solution?
1. Are you happy working here?
1. how do you manage employee performance evaluations? What is the career advancement process like?
1. Can you tell me a bit more about (an interesting thing they previously mentioned).

## Personal References

Academic jobs openly request and provide references. Industry may request them, but tend to not provide them. Be aware of this difference for your future career goals. Always have a group of people you know will provide strong recommendations. Create this group starting in college and grow it as much as you can. Don't assume companies will only ask for three references, they may ask for five or even more.
