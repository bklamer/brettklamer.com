---
title: Tips for Editing Product Images
author: Brett Klamer
description: Some tips on editing product images using an open source workflow.
diversions: Non-Statistical
tags:
date: 2015-04-15
slug:
---

# Tips for Editing Product Images

## Composition
Gordon Food Service has a nice guide for packaged and raw products. <https://www.gfs.com/files/pdf/literature/gfsphotographystandards.pdf>

## Create Outlined Image in Gimp
If you don't have proper lighting and backgrounds, it might be best to use an outline image with transparent background.

1. Open in Gimp
    1. Layer
        1. Transparency
            1. Add Alpha Channel
    1. Scissor Tool
        1. Check Antialiasing
        2. Check Feather edges (8)
        3. Check Interactive Boundary
        2. Zoom in on Image
        3. Start clicking boundary points
        1. Click on the starting boundary point when done
        5. Click in center of scissor area to select it
    6. Click the Select Button
        1. Invert
        7. Click the Keyboard Delete Button to delete the background.
    8. Image button
        1. Autocrop Image
    2. Save a `.xcf` file for future editing
    3. Export a `.png` file for use.

## Rotating Images in Gimp
1. Tools Button
    1. Transform Tools
        1. Rotate

## Trim or Autocrop Alpha Channel with ImageMagick
This automatically crops images from a transparent alpha channel background.

~~~ bash
mogrify -trim +repage *.png
~~~

- `mogrify` is an ImageMagick command to modify images by overwriting the original. Make sure you have a backup.
- `-trim` trims image edges.
- `+repage` is needed or the command will cause offset issues. `+repage` removes/resets the virtual canvas meta-data from the images. See more [here](http://www.imagemagick.org/script/command-line-options.php#repage)
- `*.png` applies actions to and overwrites all `.png` files in the folder.

## Resize Edited Photos With ImageMagick
Simple resizing.

~~~ bash
mogrify -resize WxH *.png
~~~

- `mogrify` is an ImageMagick command to modify images by overwriting the original. Make sure you have a backup.
- `-resize WxH` resizes an image to `W` px in width and `H` px in height.
- `*.png` applies actions to and overwrites all `.png` files in the folder.

To create images with the same canvas size. (useful for thumbnails or other such needs.)

~~~ bash
mogrify -gravity center -background transparent -extent WxH *.png
~~~

- `mogrify` is an ImageMagick command to modify images by overwriting the original. Make sure you have a backup.
- `-gravity center` centers the image inside the transparent background.
- `-background transparent` makes the resulting canvas transparent.
- `-extent WxH` Sets the canvas size to `W` px in width and `H` px in height.
- `*.png` applies actions to and overwrites all `.png` files in the folder.

## Reduce File Size using PNGCrush

~~~ bash
for file in *.png ; do pngcrush -brute "$file" "${file%.png}-crushed.png" && mv "${file%.png}-crushed.png" "$file" ; done
~~~

## Delete Files with Name Containing "foo"

~~~ bash
# try without -delete to see what it will apply to.
find -type f -name '*foo*' -delete
~~~

## Rename Files by Appending "foo" to End

~~~ bash
for file in *.png; do mv $file "${file%.png}-foo.png"; done
~~~

## Rename Files by Removing "foo"

~~~ bash
# "-f" to force rename files in current directory
rename -f 's/foo//' *.png
~~~

## Rename Files Based on csv File
Will rename all files in folder from the first `files.csv` column name to the second `files.csv` column name. Useful when combined with the duplication script below. Based on <http://askubuntu.com/a/438580>

~~~ bash
sed 's/"//g' files.csv | while IFS=, read orig new; do mv "$orig" "$new"; done
~~~
- `sed 's/"//g' files.csv` will remove the quotes
- `IFS=,`  split the input on ","
- `while read orig new; do ... done` will read each input line, split it on the value of `$IFS` (a comma) and save the 1st field as `$orig` and the rest as `$new`
- `mv "$orig" "$new"` will rename the files as requested

## Duplicate a File `N` Times
This will duplicate `foo.png` `N` times and name the files sequentially: `foo-001.png`, `foo-002.png`, ... `foo-N.png`.
    
~~~ bash
for n in {001..N}; do cp foo.png foo-$n.png; done
~~~
