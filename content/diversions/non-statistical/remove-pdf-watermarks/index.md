---
title: Remove PDF Watermarks
author: Brett Klamer
description: How to remove text based pdf watermarks.
diversions: Non-Statistical
tags:
date: 2015-11-10
slug:
---

# Remove PDF Watermarks

Remove pdf watermarks using [sed](https://www.gnu.org/software/sed/manual/sed.html) and [pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/).

~~~ bash
# Uncompress the pdf
pdftk infile.pdf output outfile.pdf uncompress

# Delete the first page
pdftk infile.pdf cat 2-end output outfile.pdf

# Delete the text 'foo'. Escape special characters with '\'.
sed -e "s/foo/ /" infile.pdf > outfile.pdf

# Delete links (don't modify the string)
sed -e "s/\/URI/ /" infile.pdf > outfile.pdf
sed -e "s/\/Link/ /" infile.pdf > outfile.pdf

# Compress the pdf
pdftk infile.pdf output outfile.pdf compress
~~~
