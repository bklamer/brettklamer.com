---
title: The Complete Installation Guide for Xubuntu 14.04
author: Brett Klamer
description: How to install Xubuntu and other software for a variety of cases.
diversions: Non-Statistical
tags:
date: 2014-06-01
lastmod: 2015-07-19
slug:
---

# The Complete Installation Guide for Xubuntu 14.04

This is a guide for installing Xubuntu 14.04 the hard way. There are four main install options

1. [Install Xubuntu 14.04 with dm-crypt LUKS encryption for root and home partitions](#install)
2. [Reinstall Xubuntu 14.04 with dm-crypt LUKS encryption for root and home partitions, but save the contents of the old home partition](#reinstall)
3. [Install Xubuntu 14.04 alongside a Windows installation (dual boot)](#wininstall)
4. [Reinstall Xubuntu 14.04 alongside a Windows installation (dual boot)](#winreinstall)

along with some [tips](#tips) and [software suggestions](#software). The partitioning scheme is laid out as

~~~
+-----------++-------------------------------------------------------+
|           || Logical vol1 20GB         | Logical vol2 20GB+        |
|           || /dev/mapper/system-root   | /dev/mapper/system-home   |
| Boot      ||_ _ _ _ _ _ _ _ _ _ _ _ _ _|_ _ _ _ _ _ _ _ _ _ _ _ _ _|
| partition || dm-crypt LUKS LVM partition                           | 
| /dev/sda1 || /dev/mapper/sdb2_crypt                                |
|           ||_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|
|           || dm-crypt LUKS encrypted partition                     |
|           || /dev/sda2                                             |
+-----------++-------------------------------------------------------+
~~~

## Warnings

1. You may need to change some options for your install
2. Test in a virtual machine before use. Real hardware will still be different.
3. Installing Windows after setting up the encrypted Linux OS can cause various issues. Stay away from this unless you have a high level understanding of Linux. The fix (detailed [here](http://stephentanner.com/restoring-grub-for-an-encrypted-lvm.html); may not be complete) would be to
    1. Boot into a Live CD
    2. Decrypt and map the LVM for mounting
    3. Mount the boot and root partitions
    4. Run grub-install for the disk(s) (not partition) of your choice
4. Grub gets installed on the first ordered disk by the ubuntu installation iso. It's possible the usb installation device is listed as first in disk order and will be installed with grub. In this case, simply reboot with the installation usb, login, and run `sudo dpkg-reconfigure grub-pc`. This will show a text interface where you can choose the installation disk(s).

## <a name="install"></a>1. Install Xubuntu 14.04 with dm-crypt LUKS encryption for root and home partitions

1. Download minimal Ubuntu 14.04 at <https://help.ubuntu.com/community/Installation/MinimalCD>
2. Use [unetbootin](http://unetbootin.sourceforge.net/) to transfer to usb
3. Reboot computer from usb
4. Installer boot menu
    1. Advanced options
    2. Expert install
5. Installer main menu
    1. Choose language
        1. Language: English
        1. Country: United States
        1. Country for default settings: United States
        1. Additional Locales: none (continue)
    2. Configure the keyboard
        1. No, don't detect keyboard layout
        1. Country of origin: English (US)
        1. Keyboard layout: English (US)
    2. Detect network hardware
    2. Configure the network
        1. Primary network interface: wlan0 (wireless) or eth0 (wired)
        1. Wireless network: choose ssid
        1. Wireless network type: wpa/wpa2 psk
        1. wpa/wpa2 passphrase: password
        1. Yes, autoconfigure networking
        1. Continue with 3 seconds wait time
        1. Hostname: ubuntu
        1. Domain name: none (continue)
    2. Choose a mirror of the Ubuntu archive
        1. Protocol for file downloads: http
        1. Ubuntu archive for mirror country: United States
        1. Ubuntu archive mirror: us.archive.ubuntu.com
        1. HTTP proxy: none (continue)
    2. Download installer components
        1. Installer components to load: none (continue)
    2. Detect virtual driver disks
    2. Set up users and passwords
        1. Yes, enable shadow passwords
        1. No, do not allow login as root
        1. Name for new user: name
        1. Username for account: name
        1. Password for new user: pass
        1. No, do not encrypt home directory (eCryptfs-based)
    2. Configure the clock
        1. Yes, set the clock using NTP
        1. NTP server: ntp.ubuntu.com
        1. Yes, time zone is correct
    2. Detect disks
    2. Partition disks
        1. Manual
        1. Select the disk for partitioning (Only if safe to wipe current partition table! Otherwise select free space or already created partitions.)
            1. Yes, create new empty partition table
            1. Choose gpt partition table type
        1. Select free space – to create /boot partition
            1. Create a new partition
            1. Create partition size of 500 MB in size
                1. Beginning of partition
                1. Name: boot
                1. Use as: EXT4 file system
                1. Mount point: /boot
                1. Mount options: relatime
                1. Bootable flag: on
                1. Done setting up the partitioning
        1. Select free space – to create encrypted partition
            1. Create a new partition
            1. Partition size needs to be enough for both / (root) ~20GB and /home ~20GB-4TB (sizes dependent upon individual use cases)
                1. Beginning of partition
                1. Name: crypt
                1. Use as: Physical volume for encryption
                1. Done setting up the partitioning
        1. Configure the encrypted volumes
            1. Yes, write changes to disk
            1. Finish
            1. Enter passphrase for the encrypted partition
        1. Select encrypted volume partition (sdX2_crypt)
            1. Use as: physical volume for LVM
            1. Done setting up the partitioning
        1. Configure the Logical Volume Manager
            1. Yes, write changes to disk
            1. Create volume group
                1. Name: system
                1. Select encrypted volume partition: /dev/mapper/sdX2_crypt
            1. Create logical volume (for root)
                1. Select system
                1. Name: root
                1. Volume size around 20GB
            1. Create logical volume (for /home)
                1. Select system
                1. Name: home
                1. Volume size: the rest of the volume or whatever you desire
            1. Finish
        1. Create partitions for / (root) and /home
            1. Select LVM partition for / (root)
                1. Use as: btrfs file system (backup on a different filesystem. Use ext4 or xfs for stability)
                1. Mount point: / (root)
                1. Mount options: relatime
                1. Done setting up the partitioning
            1. Select LVM partition for /home
                1. Use as: btrfs file system (backup on a different filesystem. Use ext4 or xfs for stability)
                1. Mount point: /home
                1. Mount options: relatime
                1. Done setting up the partitioning
		1. Finish partitioning and write to disk
            1. No, ignore swap warning (assuming ram is good enough)
            1. Yes, write changes to disk
    2. Install Base System
        1. Kernel to install: Linux-generic
        1. Drivers to include in the initrd: targeted
	2. Configure package manager
        1. Yes, use restricted software
        1. Yes, use universe software
        1. Yes, use multiverse software
        1. Yes, use backported software
        1. Select all services to use
    2. Select and install software
        1. Install security updates automatically
        1. Select Xubuntu desktop
    2. Install GRUB boot loader
        1. Yes, Install GRUB boot loader to the MBR
    2. Finish the installation
        1. Yes, system clock is set to UTC
        1. Remove install media and continue for reboot
		
## <a name="reinstall"></a>2. Reinstall Xubuntu 14.04 with dm-crypt LUKS encryption for root and home partitions, but save the contents of the old home partition.

1. Assuming that Ubuntu was installed as in [#1](#install)
1. Save a copy of the crypttab
    1. Use mousepad to see the file or save a copy on external media
        1. `sudo mousepad /etc/crypttab`
    1. Make a copy to the home directory
        1. `sudo cp /etc/crypttab /home/crypttab-copy`
2. Save a copy of the fstab
    1. Use mousepad to see the file or save a copy on external media
        1. `sudo mousepad /etc/fstab`
    1. Make a copy to the home directory
        1. `sudo cp /etc/fstab /home/fstab-copy`
3. Take note of the partition naming scheme
    1. `sudo blkid`
        1. Find 'crypto_LUKS' partition device
            - Suppose '/dev/sdX2' is the partition device
        1. Find 'LVM2_member' device node name
            - Suppose 'sdX2_crypt' is the device node name
    2. These will be used to create the system mapping and open the encrypted partition
        - Example: `cryptsetup luksOpen /dev/sdX2 sdX2_crypt`
3. Installer boot menu
    1. advanced options
    1. expert install
3. Installer main menu
    1. Choose language
        1. Language: English
        1. Country: United States
        1. Country for default settings: United States
        1. Additional Locales: none (continue)
    1. Configure the keyboard
        1. No, don't detect keyboard layout
        1. Country of origin: English (US)
        1. Keyboard layout: English (US)
    1. Detect network hardware
    1. Configure the network
        1. Primary network interface: wlan0 (wireless) or eth0 (wired)
        1. Wireless network: choose ssid
        1. Wireless network type: wpa/wpa2 psk
        1. wpa/wpa2 passphrase: password
        1. Yes, autoconfigure networking
        1. Continue with 3 seconds wait time
        1. Hostname: ubuntu
        1. Domain name: none (continue)
    1. Choose a mirror of the Ubuntu archive
        1. Protocol for file downloads: http
        1. Ubuntu archive for mirror country: United States
        1. Ubuntu archive mirror: us.archive.ubuntu.com
        1. HTTP proxy: none (continue)
    1. Download installer components
        1. Installer components to load: none (continue)
    2. Detect virtual driver disks
    1. Set up users and passwords
        1. Yes, enable shadow passwords
        1. No, do not allow login as root
        1. Name for new user: (same as previous installation)
        1. Username for account: (same as previous installation)
        1. Password for new user: pass
        1. No, do not encrypt home directory (eCryptfs-based)
    1. Configure the clock
        1. Yes, set the clock using NTP
        1. NTP server: ntp.ubuntu.com
        1. Yes, time zone is correct
    1. Detect disks
    1. Execute a shell
        1. Continue
        1. NOTE: use the device and name information that was found from before
        1. Shell commands (to create mapping and open encrypted partition)
            1. `cryptsetup luksOpen /dev/sdX2 sdX2_crypt`
                - enter passphrase when prompted
            1. `exit`
    1. Detect disks
    1. Partition disks
        1. Manual
        1. Select root partition below: 'LVM VG system, LV root – XX GB Linux device-mapper (Linear)'
            1. Use as: btrfs
            1. Format the partition: yes, format it
            1. Mount point: / (root)
            1. Mount options: relatime
            1. Done setting up the partition
        1. Select home partition below: 'LVM VG system, LV home – XX GB Linux device-mapper (Linear)'
            1. Use as: btrfs (use the current filesystem in use!)
            1. Format the partition: NO, keep existing data
            1. Mount point: /home
            1. Mount options: relatime
            1. Done setting up the partition
        1. Select the /boot partition
            1. Name: boot
            1. Use as: Ext4
            1. Format the partition: yes, format it
            1. Mount point: /boot
            1. Mount options: relatime
            1. Bootable flag: on
            1. Done setting up the partition
        1. Select the main dm-crypt/LUKS partition
            1. Name: crypt
            1. Use as: physical volume for encryption
            1. Erase data: NO
            1. Done setting up the partition
        1. Finish partitioning and write changes to disk
            1. No, ignore swap warning  (assuming ram is good enough)
            1. Yes, write changes to disk
    1. Install the base system
        1. Kernel to install: linux-generic
        1. Drivers to include in the initrd: targeted
    1. Configure package manager
        1. Yes, use restricted software
        1. Yes, use universe software
        1. Yes, use multiverse software
        1. Yes, use backported software
        1. Services to use: select all
    1. Select and install software
        1. Install security updates automatically
        1. Select Xubuntu desktop
    1. Install GRUB boot loader
        1. Yes, Install GRUB boot loader to the MBR
    1. Execute a shell
        1. Continue
        1. Shell commands
            1. `mount`
                - check mounted partitions
            1. `cp /target/home/crypttab-copy /target/etc/crypttab`
                - copy over old crypttab
            1. `cat /target/etc/crypttab`
                - check contents of copied file
            1. `cat /target/etc/fstab`
                - check fstab to make sure it's the same as the previous installation
                - see previous with `cat /target/home/fstab-copy`
            1. `exit`
                - the finish installation step runs update-initramfs so you don't have to do it now
    1. Finish the Installation
        1. Yes, system clock is set to UTC
        1. Remove install media and continue for reboot

## <a name="wininstall"></a>3. Install Xubuntu 14.04 alongside a Windows installation (dual boot)

1. Assumed Windows is already installed. You may need extra steps if secure boot is on, fast boot is on, UEFI is used, dynamic partitions are used, or the Windows install is a factory image.
2. Do not delete the 'system reserved' partition for Windows. Ubuntu needs this for easy recognition of the Windows installation.
2. If you cannot see the Windows partitions while running the Ubuntu partitioning menu, you will need to run [gdisk fixparts](http://www.rodsbooks.com/fixparts/). (Windows used some sort of gpt/mbr hybrid that Linux doesn't understand.)
2. Follow all steps in [#1](#install) while making the following 2 exceptions
    1. Partition disks
        1. Manual
        1. Ignore the Windows ntfs #1 and #2 partitions (assuming the C: partition doesn't take up the entire disk. You will need to shrink the C: partition if it takes up the whole disk.)
        1. select free space – to create /boot partition...	(continue as in [#1](#install))
	1. Finish the installation
        1. No, system clock is not set to UTC (Windows is not compatible with UTC)
        1. Remove install media and continue for reboot

## <a name="winreinstall"></a>4. Reinstall Xubuntu 14.04 alongside a Windows installation (dual boot)

1. Follow all steps in [#2](#reinstall) while making the following exception
    1. Finish the Installation
        1. No, system clock is not set to UTC (Windows is not compatible with UTC)
        1. Continue

## <a name="tips"></a>5. Post install suggestions

1. Update the Linux kernel
    1. If you want a specific kernel
        1. Download the following from <http://kernel.ubuntu.com/~kernel-ppa/mainline/>
            1. `linux-headers-3.XX-generic_3.XX_amd64.deb`
            1. `linux-headers-3.XX_all.deb`
            1. `linux-image-3.XX-generic_3.XX_amd64.deb`
        1. Open terminal in download location and run
            1. `sudo dpkg -i linux-headers*.deb`
            1. `sudo dpkg -i linux-image*.deb`
            1. `sudo update-grub`
        1. Restart computer
        1. Check kernel being used with
            1. `uname -a`
        1. Remove old kernels if `/boot` gets full
    1. If you want the LTS updated Kernel. Reference <https://wiki.ubuntu.com/Kernel/LTSEnablementStack>.
        1. `sudo apt-get install --install-recommends linux-generic-lts-utopic xserver-xorg-lts-utopic libgl1-mesa-glx-lts-utopic libegl1-mesa-drivers-lts-utopic`
2. Update btrfs
    1. Check version with
        1. `btrfs --version`
    1. Download from <https://launchpad.net/ubuntu/+source/btrfs-tools>
    1. Install using software center
    1. Check disk io/r/w transactions
        1. `sudo iotop -oPa`
3. If there is a separate partition or disk that needs to be mounted and unencrypted at boot. Reference <http://ubuntuforums.org/showthread.php?t=837416>.
    1. Check UUID of partitioning
        1. `sudo blkid`
    1. Check block size of / (root) partition for nice key size (likely 4096)
        1. `sudo blockdev --getbsz /dev/mapper/system-root`
    1. Create random keyfile in /root
        1. `sudo dd if=/dev/urandom of=/root/keyfile bs=4096 count=1`
            - the bs= value should be the block size we just found
    1. Make keyfile read only to root
        1. `sudo chmod 0400 /root/keyfile`
    1. Add keyfile to LUKS partition of /dev/sdX#_crypt
        1. `sudo cryptsetup luksAddKey /dev/sdX#_crypt /root/keyfile`
            - Enter existing password for /dev/sdX#_crypt
    1. Create mapper
        1. `sudo mousepad /etc/crypttab`
        1. add '/root/keyfile' to replace 'none' for /dev/sdX#_crypt
            - example: `sdX#_crypt UUID=XXX /root/keyfile luks`
    1. Mount the drive (if needed)
        1. `sudo mousepad /etc/fstab`
            - example: `/dev/mapper/sdX#_crypt /<mount point> btrfs relatime 0 2`
    1. Update settings in initramfs images
        1. `sudo update-initramfs -u -k all`
4. TRIM for SSDs
    1. Reference <http://blog.neutrino.es/2013/howto-properly-activate-trim-for-your-ssd-on-linux-fstrim-lvm-and-dmcrypt/>.
    1. Enable Trim on dm-crypt
        1. Open `/etc/crypttab`
            1. `sudo mousepad /etc/crypttab`
            1. If needed, add 'discard' to the options for `sdX#_crypt`.
    2. Make sure LVM has 'issue_discards=1' in
        1. `sudo mousepad /etc/lvm/lvm.conf`
    3. Check encrypted drive with
        1. `sudo dmsetup table /dev/mapper/sdX#_crypt`
        2. make sure it has '1 allow_discards'
    4. Remove or check "discard" is not used in the `fstab`
        1. `sudo mousepad /etc/fstab`
    4. Turn on weekly TRIM cron for non-intel/samsung SSDs.
        1. `sudo mousepad /etc/cron.weekly/fstrim`
        2. change line to
            - `exec fstrim-all --no-model-check`
    5. Run TRIM manually or check for errors
        1. `sudo fstrim -v /home`
    6. If any changes were made, run
        1. `sudo update-initramfs -c -k all`
4. If installing in Virtualbox, install additions by
    1. `sudo apt-get install virtualbox-guest-utils dkms`
    1. To share a folder, make a permanent machine folder then run
        1. `sudo usermod -a -G vboxsf username`
    1. To share a usb port
        1. `sudo usermod -a -G vboxusers username`
5. Set window tile keybinds similar to Microsoft Windows
    1. Open terminal and run
        1. `xfce4-settings-manager`
    1. Go to window manager
        1. Open the Keyboard tab
            1. Set the "Tile window to the left" (and right)
6. Backbutton in firefox to backsapce
    1. Type `about:config` in the address bar
	1. Look for `browser.backspace_action` in the list
	1. Change the Value to 0.
7. If needed, install Intel wireless driver
    1. Download driver from <http://intellinuxwireless.org/?n=Downloads>
    1. Navigate to download folder
        1. `tar xvzf iwlwifi-XXX.tgz`
        1. `cd iwlwifi-XXX/`
        1. `sudo cp iwlwifi-XXX.ucode /lib/firmware`
8. Check partition sizes
    1. `df -h`
    1. `btrfs filesystem df`
    1. `btrfs filesystem show`
9. Install packages from a newer release
    1. <http://askubuntu.com/questions/103320/install-packages-from-newer-release-without-building-apt-pinning>
    1. `apt-get install <package> -t utopic`
11. Modify or redirect home folder names
    1. change in `/home/username/.config/user-dir.dirs`
12. Change ownership of extra storage drives or partitions
    1. `sudo chown -R username /partition`
13. Change desktop lock keybind
    1. Go to settings editor
    2. xfce4-keyboard-shortcuts
    3. new commands custom property
        1. property: `/commands/custom/<super>l`
        2. type: `string`
        3. value: `xflock4`
14. Format a USB drive
    1. `df`
    1. `umount /dev/sdc1`
    1. `mkfs.vfat /dev/sdc1`
16. Fix backlight adjustment issues.
	1. `sudo apt-get install xbacklight`
	2. `xbacklight -set XX`
17. Allow usb keyboard password input for dmcrypt login
	1. Check current modules being used with `lsmod`
	2. Navigate to `/etc/initramfs-tools/` and open a terminal
		- `sudo mousepad modules`
		- add (If using a wireless logitech keyboard)

            ~~~
            usbhid
            hid
            hid_logitech_dj
            ~~~
        - `sudo update-initramfs -u`
18. Disable useless apport error messages "System program problem detected"
	1. `sudo rm /var/crash/*`
	2. `gksudo gedit /etc/default/apport`
		- Set enabled=0
	3. `sudo restart apport`

## <a name="software"></a>6. Software install suggestions

1. Install texlive 2014. follow directions from <http://www.ilogisch.de/2013/11/install-vanilla-texlive-in-buntu/>
    1. Download Texlive and run

        ~~~ bash
        wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
        tar -zxvf install-tl-unx.tar.gz
        sudo apt-get install perl-tk
        cd install-tl*
        sudo ./install-tl --gui
        ~~~

    1. Edit `/etc/environment` by adding 

        ~~~ bash
        :/usr/local/texlive/2014/bin/x86_64-linux:/usr/local/texlive/2014/texmf-dist/doc/man:/usr/local/texlive/2014/texmf-dist/doc/info:
        ~~~

    2. Make tlmgr available to sudo using either of
	    1. `sudo env PATH="$PATH" tlmgr --gui`
		2. `sudo /usr/local/texlive/2014/bin/x86_64-linux/tlmgr --gui`
    4. Make apt see the local install by:

        ~~~ bash
        sudo apt-get install equivs
        mkdir /tmp/tl-equivs
        cd /tmp/tl-equivs
        equivs-control texlive-local
        # copy this http://www.tug.org/texlive/files/debian-equivs-2014-ex.txt to
        mousepad texlive-local
        equivs-build texlive-local
        sudo dpkg -i texlive-local_2014-1_all.deb
        ~~~

1. Install script: check each line before running
   <div class="aside"><i>2015-03-18</i> <br> Thanks to <a href="http://www.rgtti.com/">Romano Giannetti</a>  for alerting me to a safer method of using the purge command. For reference: <a href="http://askubuntu.com/questions/210976/apt-get-remove-with-wildcard-removed-way-more-than-expected-why">http://askubuntu.com/questions/210976/apt-get-remove-with-wildcard-removed-way-more-than-expected-why</a></div>

    <pre class="scroll"><code>#======================================================================
    ## Update
    #======================================================================
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get dist-upgrade #dist-upgrade may install new packages or remove installed packages if that is necessary to satisfy dependencies
    #======================================================================
    ## Remove unwanted software from xubuntu-desktop recommends
    #======================================================================
    sudo apt-get purge '^abiword.*' '^brltty.*' '^espeak.*' '^gmusicbrowser.*' '^gnumeric.*' '^hplip.*' '^libhpmud0.*' '^libsane-hpaio.*' '^parole.*' '^printer-driver.*' '^speech-dispatcher.*' '^whoopsie.*' '^popularity-contest.*'
    sudo apt-get autoremove
    sudo apt-get autoclean
    sudo apt-get clean
    #======================================================================
    ## Install software
    #======================================================================
    sudo apt-get install iotop software-properties-common jags p7zip-full vlc curl pandoc libdbd-sqlite3 gcc gfortran audacious unetbootin chromium-browser texinfo
    #======================================================================
    ## Install repository software
    #======================================================================
    ######
    # PPAs
    ######
    # R PPA
    sudo add-apt-repository "deb http://cran.rstudio.com/bin/linux/ubuntu $(lsb_release -cs)/"
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
    sudo add-apt-repository ppa:marutter/c2d4u
    # KeepassX PPA
    sudo add-apt-repository ppa:keepassx/daily
    # Freefilesync PPA
    sudo add-apt-repository ppa:freefilesync/ffs
    # Lyx PPA
    sudo add-apt-repository ppa:lyx-devel/release
    # Libreoffice PPA
    sudo add-apt-repository ppa:libreoffice/ppa
    # Gnucash PPA
    wget -q -O - http://archive.getdeb.net/getdeb-archive.key | sudo apt-key add -
    sudo sh -c 'echo "deb http://archive.getdeb.net/ubuntu trusty-getdeb apps" >> /etc/apt/sources.list.d/getdeb.list'
    # smplayer PPA
    sudo add-apt-repository ppa:rvm/smplayer
    # git PPA
    sudo add-apt-repository ppa:git-core/ppa
    # Spideroak PPA
    sudo add-apt-repository "deb http://apt.spideroak.com/ubuntu-spideroak-hardy/ release restricted"
    curl https://spideroak.com/dist/spideroak-apt-2013.asc | sudo apt-key add -
    # Virtualbox PPA
    sudo add-apt-repository "deb http://download.virtualbox.org/virtualbox/debian trusty contrib"
    wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
    # Ubuntu-Tweak PPA
    sudo add-apt-repository ppa:tualatrix/ppa
    # Dockbarx PPA
    sudo add-apt-repository ppa:dockbar-main/ppa
    # Texstudio PPA
    sudo add-apt-repository ppa:blahota/texstudio
    # Task Coach PPA
    sudo add-apt-repository ppa:taskcoach-developers/ppa
    # Node.js PPA
    sudo apt-add-repository ppa:chris-lea/node.js
    # Gimp PPA
    sudo apt-add-repository ppa:otto-kesselgulasch/gimp
    # Inkscape PPA
    sudo add-apt-repository ppa:inkscape.dev/stable
    # Keepass2 PPA
    sudo add-apt-repository ppa:jtaylor/keepass
    ########
    # Update
    ########
    sudo apt-get update
    #########
    # Install
    #########
    # R
    sudo apt-get install r-base r-base-dev libopenblas-base liblapack3 libcairo2-dev libxt-dev
    # keepassX
    sudo apt-get install keepassx
    # Freefilesync
    sudo apt-get install freefilesync
    # Lyx
    sudo apt-get --no-install-recommends install lyx
    # libreoffice
    sudo apt-get --no-install-recommends install libreoffice-writer
    sudo apt-get install libreoffice-calc
    # Gnucash
    sudo apt-get --no-install-recommends install gnucash
    # smplayer
    sudo apt-get --no-install-recommends install smplayer
    # git
    sudo apt-get install git
    # Spideroak
    sudo apt-get install spideroak
    # Virtualbox - update version number based on current release
    sudo apt-get install virtualbox-4.3
    # Ubuntu-Tweak
    sudo apt-get --no-install-recommends install ubuntu-tweak
    # Dockbarx
    sudo apt-get --no-install-recommends install xfce4-dockbarx-plugin
    # Texstudio
    sudo apt-get install texstudio
    # Task coach
    sudo apt-get install taskcoach
    # Node.js
    sudo apt-get install nodejs
    # Gimp
    sudo apt-get install gimp
    # Inkscape
    sudo apt-get install inkscape
    # Keepass2
    sudo apt-get install keepass2
    #======================================================================
    ## ruby (install git before ruby)
    # instructions from https://gorails.com/setup/ubuntu/14.04
    #======================================================================
    sudo apt-get install curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev
    cd
    git clone git://github.com/sstephenson/rbenv.git .rbenv
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    exec $SHELL
    git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
    echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
    exec $SHELL
    git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash
    rbenv install 2.2.1
    rbenv global 2.2.1
    ruby -v
    echo "gem: --no-ri --no-rdoc" > ~/.gemrc
    gem install bundler
    #======================================================================
    ## Cleanup
    #======================================================================
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get dist-upgrade #dist-upgrade may install new packages or remove installed packages if that is necessary to satisfy dependencies
    sudo apt-get autoclean
    sudo apt-get clean
    sudo apt-get autoremove</code></pre>

1. Install f.lux
    1. Download xflux64.tgz from <https://justgetflux.com/linux.html>
    1. Extract the xflux binary
    1. Place the binary in `/usr/bin/`
        - `sudo cp /home/username/Downloads/xflux /usr/bin/xflux`
    1. You will probably want it to start at every boot. Add the following to the end of `/home/username/.profile`

        ~~~
        # Start x.flux
        xflux -z <zip code> -k <temperature>
        ~~~
1. Install Grub Customizer (used to modify the grub menu for dual boot)
    1. `sudo add-apt-repository ppa:danielrichter2007/grub-customizer`
    1. `sudo apt-get update`
    1. `sudo apt-get install grub-customizer`
1. Install Texmaker
    1. Download from <http://www.xm1math.net/texmaker/download.html#linux>
        1. `sudo dpkg -i *.deb`
        1. `sudo apt-get install -f`
1. Install Rstudio
    1. Download from <http://www.rstudio.com/products/rstudio/download/preview/>
        1. `sudo dpkg -i *.deb`
        1. `sudo apt-get install -f`
13. Install Teamviewer
    1. Download from <http://download.teamviewer.com/download/teamviewer_linux.deb>
        1. `sudo dpkg -i *.deb`
        1. `sudo apt-get install -f`
14. Install axcrypt
    1. Download from <https://www.axantum.com/AxCrypt/MoreDownloads.html>
        1. `chmod +x axcrypt-2.0.*`
        1. `sudo apt-get install libmono-winforms2.0-cil`
15. Install Sublime Text 3
    1. Download from <http://www.sublimetext.com/3>
        1. `sudo dpkg -i *.deb`
16. Install Jitsi
    1. Download from <https://download.jitsi.org/jitsi/debian/>
        1. `sudo dpkg -i *.deb`
        1. `sudo apt-get install -f`
17. Install Skype
    1. Download from <http://www.skype.com/en/download-skype/skype-for-linux/>
        1. `sudo dpkg -i *.deb`
        1. `sudo apt-get install -f`
18. Install Bleachbit
    1. Download from <http://bleachbit.sourceforge.net/download/linux>
        1. `sudo dpkg -i *.deb`
        1. `sudo apt-get install -f`
19. Install Truecrypt
    1. Download truecrypt 7.1a from <https://github.com/DrWhax/truecrypt-archive>
    1. `tar xfvz truecrypt-7.1a-linux-x64.tar.gz`
    1. `./truecrypt-7.1a-setup-x64`
20. Install Brother printer drivers
    1. Download from <http://support.brother.com/g/b/productsearch.aspx?c=us&lang=en&content=dl>
        1. `gunzip linux-brprinter-installer-2.0.0-1.gz`
        1. `sudo bash linux-brprinter-installer-*.*.*-*`
        1. Enter machine name
        1. When you see the message "Will you specify the DeviceURI ?" USB Users: Choose N(No). Network Users: Choose Y(Yes).
    1. If scanner isn't working
        1. `brsaneconfig4 -a name=Scanner model='modelname' ip='ip-address'`
    1. Check network mapping with
        1. `nmap 'IP range'/24`
19. Alternative to f.lux: Install Redshift
    1. Manual install
        1. Download from <https://github.com/jonls/redshift>
        1. Compile manually

            ~~~ bash
            sudo apt-get install autopoint intltool libtool libdrm-dev libxcb1-dev libxcb-randr0-dev libx11-dev libxxf86vm-dev libgeoclue-dev libglib2.0-dev python3
            ./bootstrap
            ./configure --prefix=$HOME/redshift/root \
              --with-systemduserunitdir=$HOME/.config/systemd/user
            make
            sudo make install
            ~~~
    2. PPA Install
        1. Download .deb from <https://launchpad.net/~landronimirc/+archive/ubuntu/staging/+packages>

20. Alternative Texlive install
    1. Download the install script 'install-tl-ubuntu' from <https://github.com/scottkosty/install-tl-ubuntu>
        - It creates several files in the install location. Packages will be managed with tlmgr. Use this before installing anything else tex related so dependencies are consistent. It installs the full Texlive so you will need around 3.4GB disk space
    2. Open a terminal in download location and run
        - `chmod +x ./install-tl-ubuntu
sudo ./install-tl-ubuntu`
