---
title: Photos
author: Brett Klamer
description: Photos of interesting things.
diversions: Non-Statistical
tags:
date: 2013-03-01
slug:
---

# Photos
	
<div class="pure-g">

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./laramie/happyjack1.jpg">
<img src="./laramie/happyjack1.jpg"
alt="happyjack1">
</a>
<aside>
<span>
Laramie
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./laramie/happyjack2.jpg">
<img src="./laramie/happyjack2.jpg"
alt="happyjack2">
</a>
<aside>
<span>
Laramie
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./laramie/moose1.jpg">
<img src="./laramie/moose1.jpg"
alt="moose1">
</a>
<aside>
<span>
Laramie
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./laramie/rr1.jpg">
<img src="./laramie/rr1.jpg"
alt="rr1">
</a>
<aside>
<span>
Laramie
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./laramie/snow1.jpg">
<img src="./laramie/snow1.jpg"
alt="snow1">
</a>
<aside>
<span>
Laramie
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./laramie/snow3.jpg">
<img src="./laramie/snow3.jpg"
alt="snow3">
</a>
<aside>
<span>
Laramie
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./sheridan/mule.jpg">
<img src="./sheridan/mule.jpg"
alt="mule">
</a>
<aside>
<span>
Sheridan
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./sheridan/polo.jpg">
<img src="./sheridan/polo.jpg"
alt="polo">
</a>
<aside>
<span>
Sheridan
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./sheridan/river2.jpg">
<img src="./sheridan/river2.jpg"
alt="river2">
</a>
<aside>
<span>
Sheridan
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./sheridan/sunrise.jpg">
<img src="./sheridan/sunrise.jpg"
alt="sunrise">
</a>
<aside>
<span>
Sheridan
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./sheridan/trail.jpg">
<img src="./sheridan/trail.jpg"
alt="trail">
</a>
<aside>
<span>
Sheridan
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./sheridan/valley.jpg">
<img src="./sheridan/valley.jpg"
alt="valley">
</a>
<aside>
<span>
Sheridan
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./yellowstone/buff.jpg">
<img src="./yellowstone/buff.jpg"
alt="buffalo">
</a>
<aside>
<span>
Yellowstone
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./yellowstone/canyon.jpg">
<img src="./yellowstone/canyon.jpg"
alt="canyon">
</a>
<aside>
<span>
Yellowstone
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./yellowstone/falls.jpg">
<img src="./yellowstone/falls.jpg"
alt="falls">
</a>
<aside>
<span>
Yellowstone
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./yellowstone/geo.jpg">
<img src="./yellowstone/geo.jpg"
alt="geothermal">
</a>
<aside>
<span>
Yellowstone
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./yellowstone/lily.jpg">
<img src="./yellowstone/lily.jpg"
alt="lily">
</a>
<aside>
<span>
Yellowstone
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./yellowstone/teton.jpg">
<img src="./yellowstone/teton.jpg"
alt="teton">
</a>
<aside>
<span>
Yellowstone
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./colorado/deer.jpg">
<img src="./colorado/deer.jpg"
alt="deer">
</a>
<aside>
<span>
Colorado
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./colorado/snake.jpg">
<img src="./colorado/snake.jpg"
alt="snake">
</a>
<aside>
<span>
Colorado
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./colorado/elk.jpg">
<img src="./colorado/elk.jpg"
alt="elk">
</a>
<aside>
<span>
Colorado
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./colorado/fox.jpg">
<img src="./colorado/fox.jpg"
alt="fox">
</a>
<aside>
<span>
Colorado
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./alaska/ice.jpg">
<img src="./alaska/ice.jpg"
alt="ice">
</a>
<aside>
<span>
Alaska
</span>
</aside>
</div>

<div class="photo-box pure-u-1 pure-u-md-1-2">
<a href="./grand-canyon/canyon.jpg">
<img src="./grand-canyon/canyon.jpg"
alt="canyon">
</a>
<aside>
<span>
Grand Canyon
</span>
</aside>
</div>

</div>
