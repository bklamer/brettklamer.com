---
title: How to Use imageoptim in Middleman
author: Brett Klamer
description: Install the necessary files to get imageoptim in Middleman.
diversions: Non-Statistical
tags:
date: 2014-09-25
slug:
---

# How to Use imageoptim in Middleman

Using [imageoptim](https://github.com/plasticine/middleman-imageoptim) in middleman isn't very straightforward on ubuntu. Here is how I did it.

## Install and Prepare the [imageoptim](https://github.com/plasticine/middleman-imageoptim) Middleman Extension
- Update ruby and install `middleman-imageoptim`

    ~~~ bash
    gem update
    gem install middleman-imageoptim
    ~~~

- Add `imageoptim` to `config.rb`

    ~~~ bash
    configure :build do
    activate :imageoptim
    end
    ~~~

- Add `imageoptim` to `Gemfile`

    ~~~ bash
    gem "middleman-imageoptim"
    ~~~

## Install and Prepare the [image_optim](https://github.com/toy/image_optim) ruby gem
- install `image_optim`

    ~~~ bash
    gem install image_optim
    ~~~

- Install dependencies

    ~~~ bash
    sudo apt-get install -y advancecomp gifsicle jhead jpegoptim libjpeg-progs optipng pngcrush pngquant
    ~~~

- Install `pngout`
    - Download the static linux binary from [here](http://www.jonof.id.au/kenutils). (I observed no real difference between the static vs. non-static)
    - Extract it
    - Open the `x86_64` folder
    - rename the file to `pngout`
    - Open a terminal at that location and run

        ~~~ bash
        sudo cp pngout /usr/bin
        ~~~

Now it should work! The optimization may take a while if you have a lot of images.
