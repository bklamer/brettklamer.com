---
title: LaTeX Résumé Template
author: Brett Klamer
description: A Moderncv Based LaTeX Résumé Template.
diversions: Non-Statistical
tags:
date: 2014-11-02
lastmod: 2016-11-25
slug:
---

# LaTeX Résumé Template

## Moderncv Based Template
A Moderncv based LaTeX template with built in cover letter and references. Make sure to compile with LuaLaTeX as XeLaTeX has a long-term bug in xdvipdfmx. CFF type OTF fonts with non 1000 EM size render with inflated glyphs in Adobe Reader ([source](http://www.ntg.nl/pipermail/dev-luatex/2009-March/002420.html)). This affects the FontAwesome icons. Guess how long I didn't know this and guess who uses Adobe Reader...

[moderncv-resume.tex](https://bitbucket.org/bklamer/resume/src)

<div class="pure-g">
<div class="pure-u-1 pure-u-md-1-2">
<img alt="Cover Letter" src="./moderncv-cover-letter.png" style="border:1px solid #D1D0CE"/>
</div>
<div class="pure-u-1 pure-u-md-1-2">
<img alt="Moderncv Resume" src="./moderncv-resume.png" style="border:1px solid #D1D0CE"/>
</div>
</div>

## A Jean-luc Doumont Inspired Template

A minimal, visually appealing design based on Jean-luc Doumont's [Curriculum Vitae booklet](http://www.principiae.be/pdfs/ECV-1.01.pdf). In accordance with his style, you will need to manually edit the spacings and word placement to fit your design requirements.

[minimal-resume.tex](https://bitbucket.org/bklamer/resume/src)

<div class="pure-g">
<div class="pure-u-1 pure-u-md-1-2">
<img alt="Minimal Resume" src="./minimal-resume.png" style="border:1px solid #D1D0CE"/>
</div>
</div>
<br>

Or if bullets are what you need:

[minimal-bullet-resume.tex](https://bitbucket.org/bklamer/resume/src)

<div class="pure-g">
<div class="pure-u-1 pure-u-md-1-2">
<img alt="Minimal Resume" src="./minimal-bullet-resume.png" style="border:1px solid #D1D0CE"/>
</div>
</div>
<br>
