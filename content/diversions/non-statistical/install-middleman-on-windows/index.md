---
title: Install Middleman 3 on Windows
author: Brett Klamer
description: How to install Middleman 3 on Windows.
diversions: Non-Statistical
tags:
date: 2014-01-01
slug:
---

# Install Middleman 3 on Windows

Middleman is a static site generator based on Ruby. The Windows installation isn't as easy as it should have been (at least for my case), so here are a few tips on getting things started.

## 1. Grab the Installers

<del>As of 2014-01-27, I couldn't get Ruby 2.0.0-p353 (x64) and Middlmean 3.2 to play nicely together. 
So, you may have to stick with the latest 1.9 version of Ruby.</del>

Update! The problem was that eventmachine 1.0.3 couldn't be installed in ruby 2.0.0. 
I have a detailed fix [here](/diversions/non-statistical/install-eventmachine-in-ruby-2-for-windows/).

Download Ruby and its corresponding development kit from <a href="http://rubyinstaller.org/downloads/">here</a>.
Since Middleman is a Ruby gem, we'll install that from the console in the coming steps.

## 2. Install Ruby

When installing Ruby, make sure to add Ruby executables to the PATH (everything is done from the command line). 
It should have installed to `C:\ruby193` or whatever version/location you have. 
Next, extract the development kit to `C:\ruby193\devkit`.
to install the development kit, open up a command prompt and run the following commands: (you might have to run them from the devkit folder)

~~~
ruby dk.rb init
ruby dk.rb install
gem update --system
~~~

## 3. Install Middleman

From the command prompt, run 

~~~
gem install middleman
~~~

## 4. Setup Templates

The default templates should be located somewhere around

~~~
C:\Ruby193\lib\ruby\gems\1.9.1\gems\middleman-core-3.2.1\lib\middleman-core\templates
~~~

If you're first starting out, take a look at the default templates and change them to your needs. 
In order to use your own template in windows, you will need to create a folder in the windows user directory called `.middleman`.
To create a folder in windows that begins with a dot, you will need to add an additional dot at the end. Windows will automatically remove this extra dot. So this

~~~
C:\Users\username\.middleman.
~~~

becomes

~~~
C:\Users\username\.middleman
~~~

You can then create your own custom template folders within the `.middleman` folder. The custom template folder should contain a config.rb file and the source folder. 
As previously mentioned, you can check out the default templates to model these after.
