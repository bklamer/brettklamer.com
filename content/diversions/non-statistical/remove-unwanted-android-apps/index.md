---
title: Remove Unwanted Android Apps
author: Brett Klamer
description: Remove unwanted android apps.
diversions: Non-Statistical
tags:
date: 2018-11-04
lastmod: 
slug:
---

# Remove Unwanted Android Apps

1. Ubuntu usually has the USB debugging drivers already installed. If on windows, you may need to go to device manager, uninstall the default driver and (usually automatically) upgrade the device driver. Manufacturer drivers for windows are located [here](https://developer.android.com/studio/run/oem-usb#Drivers).
2. Download the ADB binary files from <https://dl.google.com/android/repository/platform-tools-latest-linux.zip>.
3. On the phone, go to settings and tap the 'build number' 7 times to enable developer mode.
4. Turn on USB debugging from the developer options.
5. Plug in the phone and turn on file transfer mode.
6. Open a shell in the previously downloaded ADB directory and enter

    ~~~{bash}
    # find device
    adb devices
    # enter device shell
    adb shell
    # see list of packages
    pm list packages
    # delete package
    pm uninstall -k --user 0 PACKAGENAME
    ~~~

7. You can also browse package names with the app "App inspector" by Projectoria. Install from google play.
