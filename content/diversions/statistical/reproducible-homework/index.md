---
title: Reproducible Homework Template for LaTeX and Knitr
author: Brett Klamer
description: LaTeX and knitr template for reproducible homework that integrates R code and results.
diversions: Statistical
tags:
date: 2014-01-02
lastmod: 2014-07-05
slug:
---

# Reproducible Homework Template for LaTeX and Knitr

The following may be used as a LaTeX and knitr based homework template. Before compiling you will need to install

1. A LaTeX system: Texlive (for Linux) or MikTeX (for Windows)
2. The R statistical software
2. A LaTeX text editor: Texmaker or RStudio (see [here](/diversions/statistical/configure-texmaker-to-use-knitr-in-linux/) for help with Texmaker)
3. All LaTeX package dependencies
4. All R library dependencies

Grab the .Rnw file based on your choice of LaTeX preference [at the bitbucket repository](https://bitbucket.org/bklamer/homework/src)

All should compile into a PDF that looks similar to this

![reproducible homework example image](./reproducible-homework.png)
