---
title: Getting Started With Julia
author: Brett Klamer
description: How to install and start using Julia.
diversions: Statistical
tags:
date: 2014-11-08
slug:
---

# Getting Started With Julia

- <http://julialang.org/>
- <https://github.com/JuliaLang/julia>

## Install Julia

Grab the installer for Windows at <http://julialang.org/downloads/>. If using Ubuntu, run the following:

~~~ bash
sudo add-apt-repository ppa:staticfloat/juliareleases
sudo add-apt-repository ppa:staticfloat/julia-deps
sudo apt-get update
sudo apt-get install julia
~~~

## Install Light Table and Juno

R users are quite spoiled by RStudio. Julia used to have [Julia Studio](http://forio.com/labs/julia-studio/), but it seems the developers from [Forio](http://forio.com/) who developed Julia Studio are [no longer employed there](https://groups.google.com/forum/#!searchin/julia-users/julia$20studio/julia-users/N-__nw3Wd04/eHshKzndRqgJ). It may be a while (or never) before development starts up again. In the meantime, [Juno](http://junolab.org/), and even more recently [JuliaBox](https://juliabox.org/), may be the way forward for Julia IDEs. Since JuliaBox isn't quite ready, I chose to go with Juno.

1. Download Light Table from <http://lighttable.com/>
2. Follow the instructions from [here](http://www.brandonrubin.me/2014/06/23/install-light-table-on-ubuntu-with-launcher/)
    1. `tar -xzf LightTableLinux*.tar.gz`
    2. `sudo mv LightTable/ /opt`
    3. `sudo mousepad /usr/share/applications/light-table.desktop`
  
        ~~~
        [Desktop Entry]
        Version=1.0
        Name=Light Table
        GenericName=Text Editor
        Exec=/opt/LightTable/LightTable
        Terminal=false
        Icon=/opt/LightTable/core/img/lticon.png
        Type=Application
        Categories=GTK;Utility;TextEditor;Application;IDE;Development;
        ~~~

    4. `sudo ln -s /opt/LightTable/LightTable /usr/local/bin/ltable`
3. Install the Juno plugin within Light Table
    1. Open Light Table and hit `Ctrl+Space`.
    2. Type in "show plugin manager" and select.
    3. Search for "juno" and install.
    4. Hit `Ctrl+Space` and select "user behaviors".
    5. Change "default" to "june" or "june-night" in the config file. ([june not a typo](http://junolab.org/docs/settings.html))
    6. Restart Light Table. It should start spinning up Julia and show it's connected.

I've noticed, at least under Ubuntu, that after loading a Julia package, Light Table and Julia peg the cpu at 7%. I'm assuming this is a bug... For an alternative, check out the [Sublime Text Julia plugin](https://github.com/quinnj/Sublime-IJulia).

## Running Code

To evaluate a line of code in Juno, use `Ctrl-Enter`. Run `Pkg.installed()` to see the currently installed Julia packages.

- [Julia Documentation](https://docs.julialang.org)
- [Juno Documentation](http://junolab.org/docs/)
- [Learn Julia in Y Minutes](http://learnxinyminutes.com/docs/julia/)
- [Statistics Packages](http://juliastats.org)

### Plotting

~~~ julia
# Install the Gadfly graphics package
Pkg.add("Gadfly")
# Load the Gadfly Graphics package
using Gadfly
# Plot a function
plot(sin, 0, 2)
~~~

### Statistics

~~~ julia
# Install some extra statistics packages
Pkg.add("HypothesisTests")
Pkg.add("Distributions")
# Load the packages
using HypothesisTests
using Distributions
# Create some random data
rnorm = rand(Normal(), 30)
# Create a confidence interval
ci(OneSampleTTest(rnorm))
~~~
