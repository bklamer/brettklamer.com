---
title: Free Statistics Books
author: Brett Klamer
description: Free Statistics books and other resources.
diversions: Statistical
tags:
date: 2014-01-03
lastmod: 2021-05-11
slug:
---

# Free Statistics Books

## Statistics Books

<ul>
<li><a href="https://web.stanford.edu/~hastie/ISLRv2_website.pdf">An Introduction to Statistical Learning</a> (application based) by Trevor Hastie, et al.</li>
<li><a href="http://statweb.stanford.edu/~tibs/ElemStatLearn/printings/ESLII_print10.pdf">The Elements of Statistical Learning</a> (math based) by Trevor Hastie, et al.</li>
<li><a href="http://web.stanford.edu/~hastie/StatLearnSparsity/index.html">Statistical Learning with Sparsity: The Lasso and Generalizations</a> by Trevor Hastie, et al.</li>
<li><a href="https://www.webpages.uidaho.edu/~stevel/519/Hewson_2009.pdf">Multivariate Statistics with R</a> by Paul J. Hewson</li>
<li><a href="http://istics.net/pdfs/multivariate.pdf">Multivariate Statistics: Old School</a> by John I. Marden</li>
<li><a href="http://istics.net/pdfs/mathstat.pdf">Mathematical Statistics: Old School</a> by John I. Marden</li>
<li><a href="http://users.stat.umn.edu/~gary/book/fcdae.pdf">A First Course in
Design and Analysis of Experiments</a> by Gary W. Oehlert</li>
<li><a href="http://infolab.stanford.edu/~ullman/mmds/book0n.pdf">Mining of Massive Datasets</a> by Jure Leskovec, et al.</li>
<li><a href="http://www.stat.cmu.edu/~cshalizi/ADAfaEPoV/ADAfaEPoV.pdf">Advanced Data Analysis from an Elementary Point of View</a> by Cosma Rohilla Shalizi</li>
</ul>

## R Programming Books
<ul>
<li><a href="http://www.burns-stat.com/pages/Tutor/R_inferno.pdf">The R Inferno</a> by Patrick Burns</li>
<li><a href="http://adv-r.had.co.nz/">Advanced R</a> by Hadley Wickham</li>
<li><a href="http://r-pkgs.had.co.nz/">R Packages</a> by Hadley Wickham</li>
<li><a href="https://r4ds.hadley.nz/">R for Data Science</a> by Hadley Wickham and Garrett Grolemund</li>
<li><a href="http://www.r-project.org/doc/bib/R-books.html"> Other books related to R</a></li>
<li><a href="http://www.r-project.org/other-docs.html"> Various contributed documentation</a></li>
</ul>

### Official R Manuals

<ul>
<li><a href="http://cran.r-project.org/doc/manuals/R-intro.pdf">An Introduction to R</a></li>
<li><a href="http://cran.r-project.org/doc/manuals/R-FAQ.pdf">R FAQ</a></li>
<li><a href="http://cran.r-project.org/doc/manuals/R-admin.pdf">R Installation and Administration</a></li>
<li><a href="http://cran.r-project.org/doc/manuals/R-data.pdf">R Data Import/Export</a></li>
<li><a href="http://cran.r-project.org/doc/manuals/R-exts.pdf">Writing R Extensions</a></li>
<li><a href="http://cran.r-project.org/doc/manuals/R-ints.pdf">R Internals</a></li>
<li><a href="http://cran.r-project.org/doc/manuals/R-lang.pdf">R Language Definition</a></li>
</ul>

## Python Programming Books

<ul>
<li><a href="http://www.greenteapress.com/thinkpython/thinkpython.pdf">Think Python</a> by Allen Downey</li>
<li><a href="http://greenteapress.com/complexity/thinkcomplexity.pdf">Think Complexity</a> by Allen Downey</li>
<li><a href="http://learnpythonthehardway.org/book/">Learn Python the Hard Way</a> by Zed Shaw</li>
<li><a href="https://greenteapress.com/thinkstats2/thinkstats2.pdf">Think Stats: Exploratory Data Analysis in Python</a> by Allen Downey</li>
</ul>
