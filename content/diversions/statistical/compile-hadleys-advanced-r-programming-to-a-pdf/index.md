---
title: Compile Hadley's Advanced R to a PDF
author: Brett Klamer
description: How to compile Hadley's Advanced R Book to a PDF.
diversions: Statistical
tags:
date: 2013-12-01
lastmod: 2017-12-02
slug:
---

# Compile Hadley's Advanced R to a PDF

Hadley Wickham&#39;s book, <em>Advanced R</em>, is published through Chapman and Hall. The source is available on <a href="https://github.com/hadley/adv-r/">GitHub</a> and a version compiled for the web is at <a href="http://adv-r.hadley.nz/">http://adv-r.had.co.nz/</a>. Even though the HTML format is nice, I still like to have a PDF around. Here's how to compile it from the source. (Please consider buying a copy to support his work.)

## Download the Source

Extract to your folder of choice. <a href="https://github.com/hadley/adv-r/archive/master.zip">https://github.com/hadley/adv-r/archive/master.zip</a>

## Install R Package Dependencies

~~~ r
# Check error messages for needed cran packages. Development packages are 
# mentioned below.
devtools::install_github("hadley/sloop")
devtools::install_github("hadley/emo")
~~~

## Compile the book

```{r}
bookdown::render_book("index.Rmd", output_format = "bookdown::pdf_book")
```
