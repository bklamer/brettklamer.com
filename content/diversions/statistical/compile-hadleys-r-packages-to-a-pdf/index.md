---
title: Compile Hadley's R Packages to a PDF
author: Brett Klamer
description: How to compile Hadley's R Packages Book to a PDF.
diversions: Statistical
tags:
date: 2014-09-01
lastmod: 2016-03-26
slug:
---

# Compile Hadley's R Packages to a PDF

Hadley Wickham&#39;s book, *R Packages*, is now published through O’Reilly. The source is available on <a href="https://github.com/hadley/r-pkgs/">GitHub</a> and a version compiled for the web is at <a href="http://r-pkgs.had.co.nz/">http://r-pkgs.had.co.nz/</a>. Even though the HTML format is nice, I still like to have a PDF around. Here's how to compile it from the source. (Please consider buying a copy to support his work.)

You will need R, LaTeX, pandoc, and knitr to create the PDF.

## Download the Source

Extract to your folder of choice. <a href="https://github.com/hadley/r-pkgs/archive/master.zip">https://github.com/hadley/r-pkgs/archive/master.zip</a>

## Install R Package Dependencies

First check if you have the latest version of R installed and update all installed packages.

~~~ r
# Install CRAN packages
install.packages(c("rmarkdown", "pryr", "nycflights13", "png", 
                   "stringi", "lubridate", "testthat"))

# Install development packages
## Note that r-pkgs still uses the old bookdown package Hadley originally
## developed. The new bookdown package is being developed by rstudio/Yihui Xie.
## Since there is currently an issue in the oldbookdown package, You can install
## my fork to get things working.
devtools::install_github("bklamer/oldbookdown")
~~~

## Install TeX Dependencies

If you have never used XeLaTeX, there may be a few packages you will need to manually install in MiKTeX or TeX Live.

- Install system fonts for XeLaTeX access.

    ~~~ bash
    # For ubuntu users...
    sudo apt-get install fonts-inconsolata
    sudo fc-cache -fv
    ~~~

- Install missing LaTeX packages to your TeX distribution.
  - `upquote.sty`
  - `emptypage.sty`

    ~~~ bash
    # TeX Live Manager access for Ubuntu
    sudo env PATH="$PATH" tlmgr --gui
    ~~~

## Use Hadley's Build Script

1. Double click the `r-pkgs.Rproj` file and open with RStudio.
2. Open the `/book/build-book.r` file.
3. Run the `build-book.r` file line by line to see how it works and catch any errors. You're most likely to run into issues at line 36: `lapply(chapters, render_chapter)` or line 57 when the LaTeX compilation starts.

My resulting PDF is 3.7MB and 203 pages as of 2015-04-15. It seems to be an exact duplicate of the html version (although there may be some errors hiding in the corners).

## Better PDF Output

Now you can modify the LaTeX source to change the book's appearance. For example, modify the document class of `book/r-packages.tex` with

~~~ latex
\documentclass[letterpaper, 11pt, oneside]{scrbook}
~~~

Then re-run `book/build-book.r`. The sky is the limit for any other changes you wish to make...
