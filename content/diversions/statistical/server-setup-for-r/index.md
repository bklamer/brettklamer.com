---
title: Server Setup for R
author: Brett Klamer
description: How to setup a server for use with R.
diversions: Statistical
tags:
date: 2018-05-21
lastmod:
slug:
---

# Server Setup for R

This is a basic guide to use R on a remote server. It will work for any provider (AWS, Google compute, Linode, Vultr. etc.) where you can deploy an Ubuntu server.

Remote servers are useful if you need more compute resources. Most companies provide options with 8-32+ cores and 32GB-1TB+ of ram. They are usually advertised as standard, CPU compute, or high memory instances. Expect to pay anywhere from $0.25 to $10+ per hour. Most are KVM based, but dedicated or bare metal servers can also be found.

Some providers may throttle their standard servers, so you may want to specifically look for instances that support high CPU use. Digital Ocean seems to restrict their larger instances until you have sent them your birth certificate, passport, and genome sequence. There are a lot of hosting providers out there, so don't waste your time if one is giving you problems.

## Deploy server

Deploy a server with Ubuntu LTS, then:

``` bash
# Log in from your local computer
ssh root@X.X.X.X

# Update Ubuntu
apt update && apt full-upgrade

# Install newer kernel for 16.04
apt install --install-recommends linux-generic-hwe-16.04 xserver-xorg-hwe-16.

# Install other software as needed
apt install firefox iotop software-properties-common curl pandoc libdbd-sqlite3 gcc gfortran texinfo

# Install R
# R PPA
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
add-apt-repository ppa:c2d4u.team/c2d4u4.0+
apt update
apt install r-base r-base-dev libopenblas-base liblapack3 libudunits2-dev libgsl2 libgsl-devlibcairo2-dev libxt-dev

# Another OS update
apt upgrade

# Reboot the server
reboot

# Add a non-root user
adduser x
usermod -aG sudo x
```

## VNC

### Install

This will allow you to remote into a graphical environment. Note that you won't be able to use Rstudio on the remote machine over VNC. This is a known issue and Rstudio doesn't support this use case. Some people have reported success with alternative VNC setups, but it's probably best to just avoid this use case.

``` bash
# SSH into remote server and run the below:
sudo apt update

# Install xfce
sudo apt install xfce4 xfce4-goodies 

# Need this for vnc copy and paste
sudo apt install autocutsel

# Install tightvnc
sudo apt install tightvncserver

# Prepare vnc to be used
vncserver
vncserver -kill :1
mv ~/.vnc/xstartup ~/.vnc/xstartup.bak

# Modify ~/.vnc/xstartup
cat > file.txt <<EOF
#!/bin/bash
xrdb $HOME/.Xresources  
autocutsel -fork  
startxfce4 &
EOF

# Make ~/.vnc/xstartup executable
sudo chmod +x ~/.vnc/xstartup
```

If you need to install VNC on the local computer:

``` bash
sudo apt update
sudo apt install vncviewer
```

### General use

For starting and stopping a new VNC session:

``` bash
# SSH into remote server and run the below:

# Starting a session
vncserver :1 -geometry 1680x1000 -depth 24

# Ending a session
vncserver -kill :1
```

``` bash
# Run on local computer to start GUI on remote server
vncviewer X.X.X.X:1
```

## Install Rstudio server

We'll use nginx as the web server. After installation, you can log in to an Rstudio session at `https://X.X.X.X:8787`. More information is found here <https://support.rstudio.com/hc/en-us/articles/200552306-Getting-Started>.

```
# SSH into remote server and run the below:
sudo update

# You will see a greeting page at http://x.x.x.x after running the next line
sudo apt install nginx
#sudo service nginx stop
#sudo service nginx start
#sudo service nginx restart

# Install Rstudio server
# Check for updated versions here:
# https://www.rstudio.com/products/rstudio/download-server/
# Log in at https://X.X.X.X:8787
sudo apt install gdebi-core
wget https://download2.rstudio.org/rstudio-server-1.1.447-amd64.deb
sudo gdebi rstudio-server-1.1.447-amd64.deb
```

## Transfer files

``` bash
# Copy directory from remote to local
scp -r x@X.X.X.X:/remote/path ~/local/path

# Copy directory from local to remote
scp -r ~/local/path x@X.X.X.X:/remote/path

# Copy directory from remote to remote
ssh -A -t user1@remote1 scp ~/remote1/path user2@remote2:~/remote2/path
```
