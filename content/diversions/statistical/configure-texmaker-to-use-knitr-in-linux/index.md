---
title: Configure Texmaker to use Knitr in Linux
author: Brett Klamer
description: How to Configure Texmaker to use knitr in a Linux OS.
diversions: Statistical
tags:
date: 2014-06-01
lastmod: 2014-06-30
slug:
---

# Configure Texmaker to use Knitr in Linux

This was tested in Ubuntu, but should work with various other OSs.

1. Install knitr
2. Open Texmaker
  1. User -> User Commands -> Edit User Commands
  2. Menu item
      - knitr
  3. Command
        
        ~~~
        Rscript -e "knitr::knit('%.Rnw')"|pdflatex -synctex=1 -interaction=nonstopmode %.tex|evince %.pdf
        ~~~

Note how the commands are seperated by `|`. First, the knit function is run on the .Rnw file, then pdflatex compiles the .tex file into a PDF, and finally the PDF is opened with Evince. You can add and subtract commands by cutting and pasting or using the 'wizard' button. For example, if you were creating a more substantial document with citations (running Biber as the backend for BibLaTeX instead of BibTeX)

~~~
Rscript -e "knitr::knit('%.Rnw')"|pdflatex -synctex=1 -interaction=nonstopmode %.tex|biber %.bcf|pdflatex -synctex=1 -interaction=nonstopmode %.tex
~~~

Using the knit function instead of the knit2pdf function was intentional. The knit2pdf function depends on texi2pdf and it's just cleaner to call pdflatex manually.
