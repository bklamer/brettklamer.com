---
title: Compile Hadley's ggplot2 Book to a PDF
author: Brett Klamer
description: How to compile Hadley's ggplot2 Book to a PDF.
diversions: Statistical
tags:
date: 2015-09-08
lastmod: 2016-03-27
slug:
---

# Compile Hadley's ggplot2 Book to a PDF

You will need R, LaTeX, pandoc, and Linux to create the PDF. Please consider purchasing a copy to support his work.

## Install R Package Dependencies

~~~ r
# Install CRAN packages
install.packages(c("Lahman", "broom", "mapproj", "wesanderson", "USAboundaries", 
                   "scales", "ggplot2", "ggplot2movies","tidyr", "directlabels", 
                   "ggthemes", "Hmisc", "readr"))

# Install development packages
## Note that the book still uses the old bookdown package Hadley originally
## developed. The new bookdown package is being developed by rstudio/Yihui Xie.
## Since there is currently an issue in the oldbookdown package, You can install
## my fork to get things working.
devtools::install_github("bklamer/oldbookdown")
~~~

## Install Pandoc Dependencies

~~~ bash
# For ubuntu users...
sudo apt-get install pandoc-citeproc
~~~

## Download the Repository

~~~ bash
git clone https://github.com/hadley/ggplot2-book.git
~~~

## File and Directory Fixes

Delete the `.tex` files in `book/tex/` so that everything will be recompiled. Create the empty directories `book/tex/_figures` and `book/tex/diagrams`.

## Build the book

~~~ bash
# Run in terminal at directory root
make
~~~

The resulting pdf was 15.1MB and 283 pages.
