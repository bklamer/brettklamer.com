---
title: How to Encrypt Data Before Sharing
author: Brett Klamer
description: Different ways to encrypt data, or any document, before sharing with someone else.
diversions: Statistical
tags:
date: 2014-07-01
lastmod: 2020-08-28
slug:
---

# How to Encrypt Data Before Sharing

There are well defined rules for [what kind of data you should deliver to a statistician](https://github.com/jtleek/datasharing), but not for how that data should be delivered. Large corporations, research groups, and universities may have there own guidelines and rules for secure data transfer (well, [maybe not](https://www.reuters.com/article/us-google-goldman-leak/idUSKBN0F729I20140702)), but it seems everyone else is left behind with email attachments, Dropbox, and USB drives. This guide outlines a few encryption options to consider when dealing with personally identifiable information or other types of private data.

Note: inform the recipient what software was used to encrypt the file. You may not be able to decrypt software A encrypted files with software B.

## Password Management

Before we get into encryption, the most important thing to understand is password management. The basic idea is to abandon memorizing passwords for multiple sites and create a single password that gives access to a database of passwords. In a perfect world, you would have a different, randomly generated, password for each individual website, computer login, encrypted file, etc. In the real world, we can partially recreate this without too much sacrifice.

Another thing to remember is that encryption algorithms are only as strong as the input password. You may see AES-256, AES-192, or AES-128 bit encryption algorithms, but they will all have the same amount of protection if choosing an easy, dictionary based password. In general, the bit size and randomness of the password have control over the decryption attack time. This is why using a password manager to create a long, randomly generated password is important.

### Using Keepass

1. Download Keepass from <http://keepass.info/>. Windows users can use the standard download. Linux and Mac OS users can use the latest alpha release from <https://keepassxc.org/>.
2. Create a new database and database password using [Bruce Schneier's method of password creation](https://www.schneier.com/blog/archives/2014/03/choosing_secure_1.html). It must be long, secure, easy to input, and something you will never forget.
3. Choose the services you wish to use when you can't access the password manager. These might be the login passwords for your computer(s), email, university/work computer, etc. Try to keep this list small and convenient. Now create a set of easily memorable, and secure, passwords for each service.
4. Get familiar with Keepass by creating new entries for these items with your chosen passwords.
5. Use Keepass to create a unique, random password for every existing and new service you use.
6. Since the password database is encrypted with a strong password, you can upload it to your online sync service and backup in any manner you are familiar with.

## File Encryption with Public-Key Cryptography

Public-key cryptography (or Asymmetric cryptography) provides solutions for encryption, public keys, and digital signatures. It works by using the public key to encrypt and a private key to decrypt. This sounds great, but its complicated structure and poor software implementations means very few people (intentionally) use it. Public-key cryptography was first used by the masses with PGP (Pretty Good Privacy) by Phil Zimmermann and made into an open protocol with OpenPGP. The [GNU project's GnuPG (GPG)](https://gnupg.org/) is a free, open source version that the majority of people use to implement the OpenPGP protocol. Public-key encryption is probably the best option for those who can convince both (or more) parties to securely implement GPG.

### Using GPG

GPG is included in most Linux distributions. If it is not, use your version of `sudo apt-get install gnupg`. Windows users can download from <http://gpg4win.org/download.html> and Mac users can download from <https://gpgtools.org/>.

Since there are a few different graphical plugin and OS choices, I will present the steps needed through the command line. This keeps things simple and universal, but highlights the usability problem of GPG. (This is mostly a reproduction of <https://www.gnupg.org/gph/en/manual.html>.)

1. Create a key pair for your identity (Both parties must follow these steps)
    - The key pair is your private key and public key. The private key is always kept private and never seen by anyone else. The public key is given to anyone you wish to share data with.
    - `gpg --gen-key`
        - RSA and RSA (default)
        - 4096 bit key size
        - Key validation length is up to you. If you're serious about using public-key encryption, you want to have an expiration date. A good choice might be 5 years.
        - Identity
            - Note that you can create multiple user IDs after creation time. The current ID cannot be edited after creation.
            - Real name: FirstName LastName
            - Email address: your@email.com
            - Comment: a comment or leave blank
            - (O)kay
        - Create a password
            - Note that this password is not your public or private key. The password is linked to your private key such that the only way to use the private key is by knowing the password.
            - If it needs more entropy, simply push the terminal to the side and start working on something else on the computer (it may take a while). Or, if you're really into it, you can sit there and wiggle the mouse around trying to guess when enough entropy is created :)
2. Create a revocation certificate
    - The revocation certificate is used to invalidate your key pair in case you forget your password or the private key is compromised.
    - No one should see this file until it is needed to be made publicly known. Encrypt and backup. (... use a separate encryption method)
    - `gpg --output revoke.asc --gen-revoke --armor your@email.com`
        - Create a revocation certificate? y, Yes
        - Reason: 0, no reason specified (we don't know yet...)
        - Optional description: My key pair is broken or untrustworthy. Do not use! (or something to that extent)
        - enter password
3. Strengthen default algorithms
    - Check supported algorithms
        - `gpg --version`
    - `gpg --edit-key your@email.com`
    - `showpref`
    - `setpref AES256 AES192 CAMELLIA256 CAMELLIA192 TWOFISH CAST5 SHA512 SHA384 SHA256 SHA224 SHA1 RIPEMD160 ZLIB BZIP2 ZIP Uncompressed`
       - Really update the preferences? y, Yes
4. Export your public key
    - This is the file you share with the other party or make publicly known. You can publish on your website or send in an email. The fingerprint should be verified by the recipient.
    - `gpg --output your.public.key --armor --export your@email.com`
5. Export your private key
    - No one should see this file. Encrypt and backup. (... use a seperate encryption method)
    - `gpg --output your.private.key --armor --export-secret-keys your@email.com`
6. Import another parties public key
    - Only do this after establishing trust.
    - Import their key
        - `gpg --import their.public.key`
        - `gpg --list-keys`
            - Check if everything looks ok
        - `gpg --edit-key their@email.com`
        - `fpr`
            - Double check the fingerprint
        - `quit`
    - Sign and trust (Optional, not needed)
        - `gpg --edit-key their@email.com`
        - `sign`
            - Signs imported key and adds them to your web of trust.
            - You can locally sign a key (not publicly sign) by using
                - `gpg --lsign-key their@email.com`
        - `trust`
            - Gives option to set a trust level of party
        - `quit`
        - `gpg --output their.public.key --armor --export their@email.com`
            - Now you may send them their new public key which will indicate you have signed their key and increase their/your web of trust. Encrypt and sign the file before sending.
            - They should verify the contents before importing.
7. Encrypt a file
    - `gpg --sign --encrypt -r their@email.com -r your@email.com filename.ext`
        - use `--compress-algo none` for no compression
        - `--sign` is used to make a digital signature. It verifies the integrity of the file and verifies it originated with you.
        - `-r` or `--recipient` is for recipient. They will be able to decrypt. You must have imported their public key.
        - The encrypted document can only be decrypted by someone with a private key that complements one of the recipients' public keys. In particular, you cannot decrypt a document encrypted by you unless you included your own public key in the recipient list.
        - The original file is left intact. You may need to securely delete the original yourself if necessary.
        - If you have many files, first zip them into a compressed container, then encrypt and sign the container.
8. Decrypt a file
    - `gpg --output filename.ext --decrypt filename.ext.gpg`
        - Make sure it reports a good signature
9. Sign an unencrypted file
    - `gpg --sign filename.ext`
10. Verify an unencrypted signed file
    - `gpg --verify filename.ext.gpg`
11. Encrypt a text message suitable for email
    - `gpg --sign --encrypt --armor -r their@email.com -r your@email.com message.txt`
    - Then you can copy and paste the encrypted message or attach the created `message.txt.asc`
12. Decrypt a text message
    - `gpg --decrypt message.txt.asc`
        - To read in terminal
    - `gpg --output message.txt --decrypt message.txt.asc`
        - To read in an external viewer

## File Encryption with Symmetric Cryptography

Symmetric cryptography does not use a public and private key pair. This means the password used to encrypt is also used to decrypt. This method is what most people associate encryption with. The trade off for its simple structure is that each person must find a secure way to obtain the password. This is ideally done in person but could also take place over an encrypted chat/video call through software such as [jitsi](https://jitsi.org/). Digital signatures are also not supported with symmetric encryption.

The recent failure of Truecrypt has made it more of a challenge for cross platform symmetric encryption. In general, we only need a simple, community trusted, program to implement the AES256 algorithm.

### Using symmetric GPG

GPG can also be used for symmetric encryption

1. Encrypt a file with AES 256
    - `gpg --symmetric --compress-algo zlib --cipher-algo AES256 filename.ext`
        - The original file is left intact. You may need to securely delete the original yourself if necessary.
1. Decrypt a file
    - `gpg --output filename.ext --decrypt filename.ext.gpg`

### Using 7zip

1. Download from <http://www.7-zip.org/>. In Linux, use your version of `sudo apt-get install 7zip-full`, it should integrate with your archive manager.
2. In Windows
    - Right click the file -> 7-zip -> Add to archive -> Enter password -> Optionally encrypt file names
    - The original file is left intact. You may need to securely delete the original yourself if necessary.
