---
title: R Style Guide
author: Brett Klamer
description: R coding conventions to create consistent and coherent code.
diversions: Statistical
tags:
date: 2014-01-01
lastmod: 2014-10-11
slug:
---

# R Style Guide

For a quick reference on syntax, refer to

<ul>
<li><a href="https://style.tidyverse.org/">Tidyverse R style guide</a></li>
<li><a href="https://google.github.io/styleguide/Rguide.xml">Google's R style guide</a></li>
</ul>

For additional reading, check out

<ul>
<li><a href="https://docs.google.com/document/d/1esDVxyWvH8AsX-VJa-8oqWaHLs4stGlIbk8kLc5VlII/edit?pli=1">Henrik Bengtsson's R coding conventions</a></li>
<li><a href="http://cran.r-project.org/web/packages/rockchalk/vignettes/Rstyle.pdf">Paul Johnson's R style guide</a></li>
</ul>

There is also an R package, <a href="http://yihui.name/formatR/">formatR</a> by Yihui Xie, which can automatically format code. This may be especially useful if working with someone elses code or modifying old code.
