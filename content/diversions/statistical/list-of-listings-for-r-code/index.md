---
title: List of Listings for R Code in LaTeX
author: Brett Klamer
description: How to create a list of listings for R code in LaTeX.
diversions: Statistical
tags:
date: 2015-08-12
slug:
---

# List of Listings for R Code in LaTeX

knitr [officially supports the listings package](http://yihui.name/knitr/demo/listings/), but I didn't really care for the output it created. Here's what I did to add a list of listings while still keeping things simple.

Add the following to the latex header

~~~ latex
\usepackage{scrhack} % For float package and koma compatibility. Must load before "float".
\usepackage{float}
\floatstyle{plain}
\newfloat{listing}{H}{loc}[chapter] % reference http://tex.stackexchange.com/a/129245
\floatname{listing}{Listing}
~~~
Then add

~~~ latex
\listof{listing}{List of Listings}
~~~
Inside the document where you want the list to show up, usually after `\listoftables` and `\listoffigures`. Now you need to wrap any R code you think is important enough inside the new listing environment (don't forget the caption and label).

~~~ latex
\begin{listing}
<<example>>=
x <- rnorm(10)
mean(x)
@
\caption{This is the listing Caption}
\label{listing-reference}
\end{listing}
~~~
