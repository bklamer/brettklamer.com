---
title: Compile R for Data Science to a PDF
author: Brett Klamer
description: How to compile R for Data Science to a PDF.
diversions: Statistical
tags:
date: 2016-10-31
slug:
---

# Compile R for Data Science to a PDF

<em>R for Data Science</em> by Hadley Wickham and Garrett Grolemund introduces a modern workflow for data science using tidyverse packages from R. The source is available on <a href="https://github.com/hadley/r4ds">GitHub</a> and a version compiled for the web is at <a href="http://r4ds.had.co.nz/">http://r4ds.had.co.nz/</a>. Even though the HTML format is nice, I still like to have a PDF around. Here's how to compile it from the source. (Please consider buying a copy to support their work.)

These instructions assume you are using a Debian based OS and have R, LaTeX, and pandoc installed.

1. Download the repository from <https://github.com/hadley/r4ds>
1. From R, run 

    ~~~ R
    devtools::install_github("hadley/r4ds")
    devtools::install_github("wch/webshot")
    ~~~

1. From the command line, run

    ~~~ bash
    sodu apt install phantomjs
    ~~~

1. From TeX Live (or whatever TeX manager you use) make sure `framed` and `titling` are installed

    ~~~ bash
    sudo env PATH="$PATH" tlmgr --gui
    ~~~

1. Compile the book using

    ~~~ R
    # PDF
    bookdown::render_book("index.Rmd", output_format = "bookdown::pdf_book")
    # Static website
    bookdown::render_book("index.Rmd", output_format = "bookdown::gitbook")
    ~~~

My resulting PDF is 10.9MB and 412 pages as of 2016-10-31.
