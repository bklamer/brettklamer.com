---
title: A Reference List for Statistics
author: Brett Klamer
description: Books for studying statistics.
diversions: Statistical
tags:
date: 2015-10-01
lastmod: 2021-12-27
slug:
---

# A Reference List for Statistics

The following books are useful for learning/reference. Some books overlap content and have different levels of difficulty.

## Mathematical Statistics

- [Introduction to Mathematical Statistics](https://amazon.com/dp/0321795431) by Robert Hogg, et al.
- [In All Likelihood](https://amazon.com/dp/0199671222) by Yudi Pawitan
- [The Matrix Cookbook](http://www2.imm.dtu.dk/pubdb/pubs/3274-full.html) by Kaare Petersen and Michael Pedersen
- [Matrix Algebra: Theory, Computations, and Applications in Statistics](https://amazon.com/dp/0387708723) by James Gentle
- [A First Look at Rigorous Probability Theory](https://amazon.com/dp/9812703713) by Jeffrey Rosenthal

## Bayesian Statistics

- [Doing Bayesian Data Analysis](https://amazon.com/dp/0124058884) by John Kruschke
- [Bayesian Data Analysis](https://amazon.com/dp/1439840954) by Andrew Gelman, et al.
- [Statistical Rethinking](https://amazon.com/dp/1482253445) by Richard McElreath
- [Regression and Other Stories](https://avehtari.github.io/ROS-Examples/) by Andrew Gelman, et al.

## Modeling

- How to create a model
  - [Regression Modeling Strategies](https://amazon.com/dp/3319194240) by Frank Harrell
  - [Data Analysis Using Regression and Multilevel Models](https://amazon.com/dp/0521867061) by Andrew Gelman and Jennifer Hill
  - [The Book of Why](https://amazon.com/dp/046509760X) by Judea Pearl and Dana Mackenzie
  - [Causal Inference](https://www.hsph.harvard.edu/miguel-hernan/causal-inference-book/) by Miguel A. Hernan and James M. Robins
  - [Statistical Issues in Drug Development](https://amazon.com/dp/0470018771) by Stephen Senn
  - [Clinical Prediction Models](https://amazon.com/dp/1441926488) by Ewout Steyerberg
  - [Uncertainty](https://amazon.com/dp/3319819585) by William Briggs
  - [Regression Analysis: A Constructive Critique](https://amazon.com/dp/0761929045) by Richard Berk
- Implementing specific models
    - General
        - [Applied Linear Statistical Models](https://amazon.com/dp/007310874X) by Michael Kutner, et al.
        - [Categorical Data Analysis](https://amazon.com/dp/0470463635) by Alan Agresti
        - [Linear Mixed Models: A Practical Guide Using Statistical Software](https://amazon.com/dp/1466560991) by Brady West, et al.
        - [Extending the Linear Model with R](https://amazon.com/dp/149872096X) by Julian Faraway
        - [Computer Age Statistical Inference](https://amazon.com/dp/1107149894) by Bradley Efron and Trevor Hastie
    - Multivariate
        - [Methods of Multivariate Analysis](https://amazon.com/dp/0470178965) by Alvin Rencher and William Christensen
        - [An Introduction to Applied Multivariate Analysis with R](https://amazon.com/dp/1441996494) by Brian Everitt and Torsten Hothorn
        - [Multivariate Data Analysis](https://amazon.com/dp/0138132631) by Joseph Hair, et al.
    - Statistical Learning
        - [An Introduction to Statistical Learning](https://amazon.com/dp/1461471370) by Trevor Hastie, et al.
        - [Applied Predictive Modeling](https://amazon.com/dp/1461468485) by Max Kuhn and Kjell Johnson
    - Survival Analysis
        - [Survival Analysis: A Self-Learning Text](https://amazon.com/dp/1441966455) by David Kleinbaum and Mitchel Klein
        - [Modeling Survival Data: Extending the Cox Model](https://amazon.com/dp/0387987843) by Terry Therneau and Patricia Grambsch
    - Time Series
        - [Forecasting: Principles and Practice](https://otexts.com/fpp3/) by Rob Hyndman and George Athana­sopou­los
    - Quantile Regression
        - [Handbook of Quantile Regression](https://amazon.com/dp/1498725287) by Roger Koenker, et al.
    - Missing Data
        - [Flexible Imputation of Missing Data](https://amazon.com/dp/1439868247) by Stef van Buuren

## Design of Experiments

- [Statistics for Experimenters: Design, Innovation, and Discovery](https://amazon.com/dp/0471718130) by George Box, et al.
- [Design and Analysis of Experiments](https://amazon.com/dp/1118146921) by Douglas Montgomery
- [The Design of Experiments: Statistical Principles for Practical Applications](https://amazon.com/dp/0521287626) by Roger Mead
- [Design and Analysis of Experiments with R](https://amazon.com/dp/1439868131) by John Lawson

## Programming

- R
  - [Advanced R](https://amazon.com/dp/0815384572) by Hadley Wickham
  - [The Art of R Programming](https://amazon.com/dp/1593273843) by Norman Matloff
  - [R for Data Science](https://amazon.com/dp/1491910399) by Hadley Wickham and Garrett Grolemund
  - [Software for Data Analysis](https://amazon.com/dp/0387759352) by John Chambers
  - [Extending R](https://amazon.com/dp/1498775713) by John Chambers
  - [R Packages](https://amazon.com/dp/1491910593) by Hadley Wickham
- Reproducible Documents
  - [R Markdown](https://bookdown.org/yihui/rmarkdown/) by Yihui Xie, et al.
  - [Dynamic Documents with R and knitr](https://amazon.com/dp/1498716962) by Yihui Xie
  - [bookdown: Authoring Books and Technical Documents with R Markdown ](https://amazon.com/dp/113870010X) by Yihui Xie
  - [Reproducible Research with R and RStudio](https://amazon.com/dp/1498715370) by Christopher Gandrud
  - [LaTeX and Friends](https://amazon.com/dp/3642238157) by Marc van Dongen
  - [More Math Into LaTeX](https://amazon.com/dp/0387322892) by George Gratzer
- Python
  - [Python for Data Analysis](https://amazon.com/dp/1491957662) by Wes McKinney
  - [Python Data Science Handbook](https://amazon.com/dp/1491912057) by Jake VanderPlas
  - [Think Python](https://amazon.com/dp/1491939362) by Allen Downey
  - [Automate the Boring Stuff with Python](https://amazon.com/dp/1593275994) by Al Sweigart
- SQL and Databases
  - [The Language of SQL](https://amazon.com/dp/143545751X) by Larry Rockoff
  - [Data Analysis Using SQL and Excel](https://amazon.com/dp/111902143X) by Gordon Linoff
  - [Data Modeling Essentials](https://amazon.com/dp/0126445516) by Graeme Simsion and Graham Witt
- C++
  - [C++ Primer](https://amazon.com/dp/0321714113) by Stanley Lippman, et al.
  - [Effective Modern C++](https://amazon.com/dp/1491903996) by Scott Meyers
  - [Seamless R and C++ Integration with Rcpp](https://amazon.com/dp/1461468671) by Dirk Eddelbuettel
- C
  - [C Programming Absolute Beginner's Guide](https://amazon.com/dp/0789751984) by Greg Perry and Dean Miller
  - [C Programming: A Modern Approach](https://amazon.com/dp/0393979504) by K. N. King 
  - [Modeling with Data: Tools and Techniques for Scientific Computing](https://amazon.com/dp/069113314X) by Ben Klemens

## Data Visualization

- [Visualizing Data](https://amazon.com/dp/0963488406) by William Cleveland
- [ggplot2](https://amazon.com/dp/331924275X) by Hadley Wickham
- [The Grammar of Graphics](https://amazon.com/dp/0387245448) by Leland Wilkinson, et al.
- [Exploratory Data Analysis](https://amazon.com/dp/0201076160) by John Tukey
- [Data Visualization](https://socviz.co/) by Kieran Healy
- [Fundamentals of Data Visualization](https://clauswilke.com/dataviz/) by Claus O. Wilke

## Sampling and Surveys

- [Sampling](https://amazon.com/dp/0470402318) by Steven Thompson
- [Survey Sampling](https://amazon.com/dp/0471109495) by Leslie Kish
- [Applied Survey Data Analysis](https://amazon.com/dp/1420080660) by Heeringa, et al.
- [The Survey Research Handbook](https://amazon.com/dp/0072945486) by Pamela Alreck and Robert Settle

## Measurement

- [An Introduction to Error Analysis](https://amazon.com/dp/093570275X) by John Taylor

## Mathematical Background

- [Linear Algebra](https://amazon.com/dp/0538735457) by David Poole
- [Calculus](https://amazon.com/dp/0486404536) by Morris Kline
- [Book of Proof](https://amazon.com/dp/0989472108) by Richard Hammack
