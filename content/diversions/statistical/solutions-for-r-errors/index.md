---
title: Solutions for R Errors
author: Brett Klamer
description: A collection of errors and solutions I've encountered while using R.
diversions: Statistical
tags:
date: 2015-07-17
lastmod: 2021-07-30
slug:
---

# Solutions for R Errors

## System Errors

- I commonly experience errors running functions from the `gtsummary` package. These issues arise when running code within a virtualized Ubuntu OS. Functions that include many variables, from either `tbl_uvregression()`, `tbl_regression()`, or `tbl_merge()` will inevitably result in the following error:
   
   ```r
   Error: C stack usage   7971348 is too close to the limit
   ```
   
   `gtsummary` produces a monster of nested code as seen in the `traceback()`. You can inspect the C stack usage in r using `Cstack_info()`. Unfortunately I don't know of any way to modify this value within a running R session. Luckily it can be modified within the terminal using
   
   ```bash
   ulimit -s unlimited
   ```
   
   Then, within the current terminal, start an R session and run the desired code. I'm usually working in an R Markdown document which requires running
   
   ```r
   rmarkdown::render(input = "./input.rmd", output_format = "html_document")
   ```
   
   Then R Markdown caching takes care of the rest and I can continue the normal document flow in other chunks. I don't know of a way to start an rstudio session with the modified `ulimit`. 

## Building Packages

- While running `R CMD check` you may encounter `checking PDF version of manual ... WARNING`
  1. Determine why the latex build is failing. Open a terminal in the package folder and run
        ~~~ r
        R CMD Rd2pdf man/
        ~~~
      Look at the error messages and try to determine where the problem occurs.

  2. If you see the following:
        ~~~
        Converting Rd files to LaTeX 
        Creating pdf output from LaTeX ...
        Error in texi2dvi(file = file, pdf = TRUE, clean = clean, quiet = quiet,  : 
        Running 'texi2dvi' on 'Rd2.tex' failed.
        Messages:
        sh: 1: /usr/bin/texi2dvi: not found
        Output:

        Error in texi2dvi(file = file, pdf = TRUE, clean = clean, quiet = quiet,  : 
        Running 'texi2dvi' on 'Rd2.tex' failed.
        Messages:
        sh: 1: /usr/bin/texi2dvi: not found
        Output:

        Error in running tools::texi2pdf()
        ~~~
      then you need to install `texi2dvi`, which is located in the `texinfo` package. To install `texinfo` on Ubuntu use
        ~~~
        sudo apt-get install texinfo
        ~~~
      Note: do not install `texinfo` using TeX Live Manager. `texi2dvi` would not be placed into `usr/bin/`.

- While running `R CMD check` you may encounter `no visible binding for global variable [object name]`
  - Creating new objects inside your package using other package's functions (data.table, dplyr, ggplot, etc) may produce this NOTE. See <http://stackoverflow.com/a/12429344> for a nice discussion and solution. Use something like...
    ~~~ r
    #-----------------------------------------------------------------------------
    # To mitigate R CMD check producing NOTES
    # reference http://stackoverflow.com/a/12429344
    #-----------------------------------------------------------------------------
    globalVariables(c("object1", "object2"))
    ~~~
    outside of the package function at the bottom.

- The `citation()` function will produce a warning if the `DESCRIPTION` file does not include a date field. This can be ignored since submission to CRAN will automatically add the date field.
