---
title: Statistics Jobs Search
author: Brett Klamer
description: Websites to search for statistics jobs.
diversions: Statistical
tags:
date: 2013-08-01
lastmod: 2022-01-24
slug:
---

# Statistics Jobs Search

## Find Jobs

- <a href="https://www.indeed.com/jobs?q=statistics">Indeed</a> - Almost everything
- <a href="https://forms.stat.ufl.edu/statistics-jobs/">University of Florida</a> - Mostly academic
- <a href="https://datalab.ucdavis.edu/jobs/">UC Davis</a> - Mostly academic
- <a href="https://www.stat.purdue.edu/resources/jobs/postings/index.html">Purdue</a> - Mostly academic
- <a href="https://stat.uw.edu/news-resources/jobs">University of Washington</a> - Mostly academic
- <a href="https://jobs.amstat.org/jobs/">ASA</a> - Mostly academic
- <a href="https://jobs.imstat.org/jobseeker/search/results/">IMS</a> - Mostly academic
- <a href="https://jobs.siam.org/jobseeker/search/results/">SIAM</a> - Mostly academic
- <a href="https://stat.ethz.ch/pipermail/r-sig-jobs/">R SIG for jobs</a> - Use R!
- <a href="https://www.r-users.com/">R-users</a> - Tal Galili/R-bloggers spinoff site
- <a href="https://twitter.com/RStatsJobsBot/with_replies">Twitter #rstats jobs</a> - Use R!
- <a href="https://findajob.agu.org/">AGU</a> - Because &#39;business analyst&#39; one too many times
- <a href="https://www.linkedin.com/">Linkedin</a> - Recruiterville
- <a href="https://hnhiring.com/">Hacker News</a> - Tech and others
- <a href="https://weworkremotely.com/">We Work Remotely</a> - Tech, remote friendly
- <a href="https://www.usajobs.gov/Search?k=statistics">USA Jobs</a> - Government jobs

## Salary

- [levels.fyi](https://www.levels.fyi)
- [Glassdoor](https://www.glassdoor.com)
- [ASA Salary Information](http://www.amstat.org/ASA/Your-Career/Salary-Information.aspx)
- [O'Reilly 2017 Data Science Salary Survey](http://www.oreilly.com/data/free/files/2017-data-science-salary-survey.pdf)
- [O'Reilly 2016 Data Science Salary Survey](http://www.oreilly.com/data/free/files/2016-data-science-salary-survey.pdf) ([html version](https://www.oreilly.com/ideas/2016-data-science-salary-survey-results))
- [O'Reilly 2015 Data Science Salary Survey](http://www.oreilly.com/data/free/files/2015-data-science-salary-survey.pdf) ([html version](https://www.oreilly.com/ideas/2015-data-science-salary-survey/))
- [O'Reilly 2014 Data Science Salary Survey](http://www.oreilly.com/data/free/files/2014-data-science-salary-survey.pdf) ([html version](https://www.oreilly.com/ideas/2014-data-science-salary-survey/))
