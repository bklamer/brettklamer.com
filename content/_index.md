---
title: Home
author: Brett Klamer
description: Personal website of Brett Klamer.
---

# Home

Hello, this is the website of Brett Klamer. I'm an applied statistician and am interested in statistical programming, visualization, and modeling. I enjoy working with [R](https://www.r-project.org) and have developed a few [packages](/work/). In my free time I enjoy 
reading, running, and nature.

You may contact me at the email address given by running this R code:

``` r
# Feel free to interpret visually
username <- "mail"
domain <- "brettklamer.com"
cat(c(username, domain), sep = "@")
```
