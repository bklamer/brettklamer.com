# 1. Delete old site at ./static/work/pkg/
# 2. Build new site using pkgdown::build_site()
# 3. Run pkgdown-post-process.r
pkgs <- c("depower", "rankdifferencetest")

for(i in seq_along(pkgs)) {
  # remove pkgdown.yml
  unlink(paste0("./static/work/", pkgs[i], "/pkgdown.yml"), recursive = TRUE)

  # remove katex-auto.js
  unlink(paste0('./static/work/', pkgs[i], '/katex-auto.js'), recursive = FALSE)
  
  # remove lightswitch
  unlink(paste0('./static/work/', pkgs[i], '/lightswitch.js'), recursive = FALSE)
  
  # remove font awesome assets
  unlink(paste0("./static/work/", pkgs[i], "/deps/font-awesome-6.4.2"), recursive = TRUE)
  
  # remove unused bootstrap
  unlink(paste0('./static/work/', pkgs[i], '/deps/bootstrap-5.3.1/bootstrap.bundle.min.js.map'), recursive = FALSE)
  
  # remove unused jquery
  unlink(paste0('./static/work/', pkgs[i], '/deps/jquery-3.6.0/jquery-3.6.0.min.map'), recursive = FALSE)
  unlink(paste0('./static/work/', pkgs[i], '/deps/jquery-3.6.0/jquery-3.6.0.js'), recursive = FALSE)
  
  # remove unused txt file
  unlink(paste0('./static/work/', pkgs[i], '/deps/data-deps.txt'), recursive = FALSE)

  # file paths
  dirs <- c("/", "/articles/", "/news/", "/reference/")
  path <- paste0("./static/work/", pkgs[i])
  tmp <- lapply(dirs, function(x) {
    list.files(
      path = paste0(path, x),
      pattern = "*.html$"
    )
  })
  names(tmp) <- dirs

  # find and replace across all files
  for(j in seq_along(tmp)) {
    for(k in seq_along(tmp[[j]])) {
      text <- readLines(con = paste0(path, dirs[j], tmp[[j]][[k]]))
      
      # pages
      text <- gsub(
        pattern = '<link href="deps/font-awesome-6.4.2/css/all.min.css" rel="stylesheet">',
        replacement = "",
        x = text,
        fixed = TRUE
      )
      text <- gsub(
        pattern = '<link href="deps/font-awesome-6.4.2/css/v4-shims.min.css" rel="stylesheet">',
        replacement = "",
        x = text,
        fixed = TRUE
      )
      text <- gsub(
        pattern = '<link href="../deps/font-awesome-6.4.2/css/all.min.css" rel="stylesheet">',
        replacement = "",
        x = text,
        fixed = TRUE
      )
      text <- gsub(
        pattern = '<link href="../deps/font-awesome-6.4.2/css/v4-shims.min.css" rel="stylesheet">',
        replacement = "",
        x = text,
        fixed = TRUE
      )
      
      # 404 pages
      text <- gsub(
        pattern = paste0('<link href=\"https://brettklamer.com/work/', pkgs[i], '/deps/font-awesome-6.4.2/css/v4-shims.min.css\" rel=\"stylesheet\">'),
        replacement = "",
        x = text,
        fixed = TRUE
      )
      text <- gsub(
        pattern = paste0('<link href=\"https://brettklamer.com/work/', pkgs[i], '/deps/font-awesome-6.4.2/css/all.min.css\" rel=\"stylesheet\">'),
        replacement = "",
        x = text,
        fixed = TRUE
      )
      
      # do I want an advertisement?
      ad_start <- grep(pattern = '<div class="pkgdown-footer-right"', x = text, fixed = TRUE)
      if(length(ad_start) > 0L) {
        stopifnot(text[ad_start + 2] == '</div>')
        text <- text[-(ad_start:(ad_start + 2))] 
      }
      
      # result
      writeLines(text = text, con = paste0(path, dirs[j], tmp[[j]][[k]]))
    }
  }
}
